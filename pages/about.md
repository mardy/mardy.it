<!--
.. title: About
.. slug: about
.. date: 2019-01-23
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
.. pretty_url: True
-->

<div class="container" style="max-width: 40rem;">
{{% hcard intro="Here's a few links for those interested in tracking me over social media:" %}}
</div>

While studying Russian, I'm keeping a blog in this language [here](https://mardy.livejournal.com/).
