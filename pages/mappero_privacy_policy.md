<!---
.. title: Privacy Policy
.. slug: mappero_privacy_policy
.. type: text
.. pretty_url: False
-->

Mappero Geotagger respects your privacy: all the content imported into Mappero
Geotagger is not sent over any network connection.

Mappero Geotagger will not try to access any files belonging to the user, except for
those files which the user has explicitly imported into Mappero Geotagger.

Such files will be processed like follows:

1. If the image metadata contains location information, the image will be
   displayed on the map. Such information will only be visualized locally; it
   will not be sent over a network.

2. If the user sets a location for the imported image(s), or changes the
   existing location, the image file(s) will be updated to contain the desired
   location information.

Mappero Geotagger does not retain ownership of any imported content.
