<!--
.. title: Projectos
.. slug: projectos
.. date: 2019-01-23
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
.. pretty_url: True
-->

Un parve selection de mi projectos personal:

.. post_list::
   :post_type: pages
   :categories: projectos
   :template: lista_projectos.tmpl
