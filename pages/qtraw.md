<!--
.. title: QtRaw
.. slug: qtraw
.. date: 2012-12-17 17:12:25 UTC+03:00
.. tags: projectos
.. category: projectos
.. link: 
.. description: a Qt plugin for loading raw image files
.. type: text
.. pretty_url: True
-->

If you are working on a Qt-based photo viewer or manipulation program, you
might have noticed that raw image files are not supported by Qt: you can load
JPEGs, PNGs, SVGs and some other file types, but not ARW, NEF, CR2 and other
raw files coming straight from a digital photocamera.

[QtRaw](https://gitlab.com/mardy/qtraw) aims to improve this situation: it's a
Qt *imageformat* plugin based on the excellent [LibRaw
project](https://www.libraw.org/), which allows your application to load raw
image files. The nice thing about it is that it doesn't require any
modification (not even a recompilation) of the application: just build and
install the plugin in your Qt's `imageformats/` directory, and all Qt
applications will automatically make use of it.

### Features

* Uses embedded thumbnail if suitable for requested size
* Automatic image rotation
* Supports CRW, CR2, ARW, NEF, RAF, DNG (you can [ask for more!](https://gitlab.com/mardy/qtraw/issues))
* Supports all recent (since 0.15 at least) LibRaw versions
* Supports Qt 4 and 5 (verified to work with Qt 5.6, 5.9 and 5.12)
