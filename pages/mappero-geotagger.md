<!--
.. title: Mappero Geotagger
.. slug: mappero-geotagger
.. date: 2012-08-19
.. tags: projectos
.. category: projectos
.. link: 
.. description: 
.. type: text
.. pretty_url: True
-->

<h3>Mappero Geotagger</h3>

<div class="alert alert-danger">
  <strong>NOTE</strong>: this page is no longer updated. Mappero Geotagger has now a website of its own, and you are welcome to <a href="http://mappero.mardy.it">visit it here</a>.
</div>

<div style="float: right; text-align: center; margin: 1em;"><iframe width="400" height="315" src="https://www.youtube.com/embed/b1J84dISuNk?rel=0" frameborder="0" allowfullscreen></iframe><small><center>See Mappero Geotagger in action</center></small></div>

<p>Mappero Geotagger is a geotagging application: it allows you to assign a geographic location to your photos. The location information is embedded following a standard format in the picture file itself, so that you won't lose it if you need to move the file around; and if you upload it to <a href="http://flickr.com">Flickr</a> or other services which support the <a href="http://en.wikipedia.org/wiki/Exchangeable_image_file_format#Geolocation">EXIF</a> standard, other people will be able to find your pictures just by browsing a map.<br>
The application is available under the terms of the <a href="http://www.gnu.org/copyleft/gpl.html">GPLv3 open source license</a> and runs on Linux, Mac OS X (64bit) and on Windows 2000 or later.</p>

<h3>Features</h3>
<ul>
  <li>Automatic downloading of maps from <a href="http://www.openstreetmap.org/">OpenStreetMap</a>.</li>
  <li>Visualize existing location information by showing your photos on a map.</li>
  <li>Set or modify geolocation information by dragging pictures on the map.</li>
  <li>Visualization of GPX and KML GPS tracks.</li>
  <li>Automatic assignment of location information by correlating the time of the photos with the times of a GPX track (this process is often called <em>correlation</em>).</li>
  <li>Graphic visualization of the photos on the map during the GPX correlation operation, with controls to specify time zone offset and fine-grained time adjustment.</li>
  <li>Want to know if your favorite feature is supported? <b><a href="mailto:shop@mardy.it">Ask me!</a></b> (mention "Mappero Geotagger" in the subject, to avoid the antispam filter)</li>
</ul>
<p>Mappero Geotagger supports all file formats supported by <a href="http://www.exiv2.org/">libexiv2</a>; a list can be found <a href="http://dev.exiv2.org/wiki/exiv2/Supported_image_formats">here</a>, but it seems to be outdated: more formats than those are actually supported (at least, I found that Mappero Geotagger can successfully geotag Sony ARW images).</p>

<h3>How to get it</h3>

<div style="float: right; text-align: center; margin: 1em;"><a href="/archivos/imagines/mappero-geotagger.png"><img src="/archivos/imagines/mappero-geotagger.png" border="0" width="400px"></a><br/><small><center>The mandatory screenshot: Mappero Geotagger correlating images!</center></small></div>

<p>You can purchase Mappero Geotagger through this website. Unless there are some active <a href="#promotions">promotions</a>, pricing goes as follows:</p>
<ul>
  <li>with a <a href="#donation"><em>donation</em></a> of at least <b>15€</b>, you get the latest version;</li>
  <li>with a <a href="#donation"><em>donation</em></a> of at least <b>40€</b>, you get all versions (even future ones)</li>
</ul>
<p>What you will get is:</p>
<ul>
  <li>access to the purchased release in form of (all of them):<ul>
    <li><b>source code</b> (can be compiled on all Linux distributions)</li>
    <li>pre-compiled packages for <b>Ubuntu</b> (12.04, 32 and 64 bits)</li>
    <li>dmg package for <b>Mac OS X</b> (tested in Snow Leopard and Mountain Lion, but should run on any 64 bit version of OS X)</li>
    <li><b>Microsoft Windows executable</b>, with installer (2000, XP, Vista, 7 and most likely 8)</li>
    </ul></li>
  <li><b>Any bugfix release</b>: from time to time I might release some new versions which fix any found bugs; I will make these available to you.</li>
  <li>Access to the <a href="http://lists.mardy.it/listinfo.cgi/mappero-geotagger-mardy.it">Mappero Geotagger <b>mailing list</b></a>: there you can ask for support on how to use the software (but belive me, it's very easy!), you can report any bug you found, suggest new features and be notified of new releases.</li>
</ul>
<p>The GPLv3 license, which Mappero Geotagger uses, allows you to re-distribute the software, as long as you also redistribute the source code. You are very welcome to share the application with your friends, indeed &mdash; just please remind them that I would appreciate a donation. Remember, the more donations I get, the more I feel committed to improve this software!</p>

<h3><a name="donation">Donations</a></h3>

<p>Why do I say &ldquo;donations&rdquo;? Simple: because you are free to donate any amount you wish; chances are that you already got Mappero Geotagger in your hands, since it can be freely redistributed, therefore any donation is welcome. Just please notice, if you want to download the software from this site, you must donate at least the amount described in the paragraph above.<br />
Any other donation amount will give you access to the mailing list, where new releases will be announced and where you can ask for any help in using the program, report bugs and suggest features.</p>
<p>You can donate in the following ways:</p>
<ul>
  <li>Via paypal: just click this ugly button:
<form action="https://www.paypal.com/cgi-bin/webscr" method="post">
<input type="hidden" name="cmd" value="_s-xclick">
<input type="hidden" name="hosted_button_id" value="NQ6NU5FEB9JLY">
<input type="image" src="https://www.paypalobjects.com/en_GB/i/btn/btn_donate_SM.gif" border="0" name="submit" alt="PayPal — The safer, easier way to pay online.">
<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
</form>
  </li>
  <li>Directly to my Finnish bank account (recommended for citizens of the euro zone):
  <p style="margin-left: 3em">IBAN:   FI53 1820 3500 0221 81<br/>
BIC:    NDEAFIHH<br/>
Alberto Mardegan<br/>
Osmankäämintie 44 C<br/>
01300 Vantaa, Finland<br/>
</p>
In case you pay by bank account, please remember to write a mail to <a href="mailto:shop@mardy.it">shop@mardy.it</a> with the details of your payment (please mention "Mappero Geotagger" in the subject in order to avoid the spam filters).
  </li>
</ul>
<p>It might take me a few days (up to three working days) to get back to you after I've received the payment, so please be patient.</p>
<p>Important notice: please <b>do not share your mardy.it account information with anyone!</b>. While you are free to redistribute the software, please do not do that by using my web server's limited resources (you probably already noticed how slow it is); should I detect that you have shared your mardy.it username and password with other people, <b>your account will be deleted</b>.</p>

<h3><a name="promotions">Promotions</a></h3>

<p>Only till September 30th, <b>mention Mappero Geotagger in <a href="http://twitter.com">twitter</a> or in <a href="http://plus.google.com">Google+</a> and you can get it for just 10€</b> or, even better, <b>blog about it and get it for 5€!</b>. Here are the rules (please read carefully!):</p>
<p>For Google+ and twitter:</p>
<ul>
  <li>The post must be public (visible to all).</li>
  <li>The post must contain a link to this page, <a href="http://www.mardy.it/mappero-geotagger">www.mardy.it/mappero-geotagger</a></li>
  <li>The post must be written originally by you; re-sharing or re-twitting won't be considered valid for this promotion.</li>
  <li>The post must be somehow related to Mappero Geotagger: a twit like "My dog has died" with a link to this site won't be appreciated. Please use your common sense.</li>
  <li>Make a <a href="#donation">donation</a> of at least 10€, and in the message write the link to the post, so that I can have proof of it; if you forget about it, or realize that you wrote a wrong link, write me a message at <a href="mailto:shop@mardy.it">shop@mardy.it</a> (please mention "Mappero Geotagger" in the subject in order to avoid the spam filters).</li>
</ul>
<p>For bloggers:</p>
<ul>
  <li>The post must be public (visible to all).</li>
  <li>The post must contain a link to this page, <a href="http://www.mardy.it/mappero-geotagger">www.mardy.it/mappero-geotagger</a></li>
  <li>The post must be written originally by you.</li>
  <li>The post must be related to Mappero Geotagger.</li>
  <li>The post must tell readers not to ask you to share your copy of Mappero Geotagger (though you are free to share it, in fact).</li>
  <li>The post can be in any language (of course, <a href="http://www.interlingua.com">interlingua</a> es le lingua preferite!).</li>
  <li>Make a <a href="#donation">donation</a> of at least 5€, and in the message write the link to the post, so that I can have proof of it; if you forget about it, or realize that you wrote a wrong link, write me a message at <a href="mailto:shop@mardy.it">shop@mardy.it</a> (please mention "Mappero Geotagger" in the subject in order to avoid the spam filters).</li>
</ul>
