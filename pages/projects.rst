.. title: Projects
.. slug: projects
.. date: 2019-01-23
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
.. pretty_url: True

Projects I've been working on:

.. post_list::
   :post_type: pages
   :categories: projectos
   :template: lista_projectos.tmpl
