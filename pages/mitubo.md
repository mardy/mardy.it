<!--
.. title: MiTubo
.. slug: mitubo
.. date: 2022-05-08 22:35:00
.. tags: projectos
.. category: projectos
.. link: 
.. description: 
.. type: text
.. pretty_url: True
.. data: data/mitubo/releases.json
-->

MiTubo is an application for playback or download of media content from video
streaming websites, such as Vimeo and YouTube.

<figure style="width: 528px" class="mx-auto">
  <img src="/archivos/imagines/mitubo/mainpage-0.9.png" class="shadow" />
  <figcaption>MiTubo main page: search box, playlists and RSS feeds</figcaption>
</figure>

MiTubo is available as a desktop application for Linux and Windows (a macOS
version is in the works, follow the [blog](/) to be alerted when it comes out!)
and as a mobile application for the fantastic [Ubuntu
Touch](https://ubuntu-touch.io/) operating system.

For extracting the video information and for downloading of videos, MiTubo uses
the [yt-dlp](http://github.com/yt-dlp/yt-dlp) or the
[youtube-dl](http://youtube-dl.org) projects.

### Contents

1. [Features](#features)
1. [Screenshots](#screenshots)
1. [Download MiTubo!](#downloads)
1. [Contributing](#contributing)

### Features <a name="features" />

- Playback video from exact URL
- Search video
    - Yandex search
    - [Invidious](https://invidious.io/) search for YouTube videos
- Organize videos into playlists
- Subscribe to RSS video feeds
    - Follow YouTube channels
- Download media
    - Allow restricting to only audio or video
    - Selection of quality
- Drag URLs during playback to enqueue them in the “Watch later” list


### Screenshots <a name="screenshots" />

In no particular order:

<figure style="width: 528px" class="mx-auto">
  <img src="/archivos/imagines/mitubo/infopage-0.9.jpg" class="shadow" />
  <figcaption>The video information page</figcaption>
</figure>

<figure style="width: 528px" class="mx-auto">
  <img src="/archivos/imagines/mitubo/playback-0.9.jpg" class="shadow" />
  <figcaption>Watching cat videos in MiTubo</figcaption>
</figure>

<figure style="width: 528px" class="mx-auto">
  <img src="/archivos/imagines/mitubo/feed-0.9.jpg" class="shadow" />
  <figcaption>Subscribe to YouTube channels and RSS feeds</figcaption>
</figure>

<figure style="width: 528px" class="mx-auto">
  <img src="/archivos/imagines/mitubo/download-0.9.png" class="shadow" />
  <figcaption>Downloading videos has never been easier</figcaption>
</figure>


### Downloads <a name="downloads" />

Here are the latest releases of MiTubo, the most recent ones are at the top:

> **_NOTE_**: For Debian, Ubuntu and derivatives there's a [daily
> PPA](https://launchpad.net/~mardy/+archive/ubuntu/mitubo).

<ul>
{{% template %}}
{% for version_data in global_data['mitubo/releases'] %}
  <li>Version <b>{{version_data.version}}</b> ({{version_data.date}}):
  {% set packages = version_data.packages %}
  {% for platform,file in packages.items() %}
  {% set plat_data = global_data['mitubo/platforms'][platform] %}
  <a href="http://mitubo.mardy.it/releases/MiTubo_{{file}}.{{ plat_data.extension }}">{{plat_data.name}}</a>
  {% if not loop.last %} &mdash; {% endif %}
  {% endfor %}
{% endfor %}
{{% /template %}}
</ul>


### Contributing <a name="contributing" />

There are several ways in which you can help this project, including:

- Testing and [reporting bugs](https://gitlab.com/mardy/mitubo/-/issues) and
  even requesting new features (while new feature requests don't directly help
  developing the project, they act as a strong sign of support and interest).
- [Code contributions](https://gitlab.com/mardy/mitubo): MiTubo is licensed
  under the GPL 3.0 license, and you must be willing to grant me the option to
  relicense your work under any existing or future license published by the
  Free Software Foundation.
- Translations: to be honest, the project is not yet fully ready for
  translations, but it's something I will be addressing soon. ☺
- Donations: I'm developing MiTubo in my free time and for my own needs, but if
  you want to keep me motivated this is certainly an excellent way to do that!
    - Donate via [Paypal](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=NQ6NU5FEB9JLY)
    - Donate via [Yandex Money](https://yoomoney.ru/quickpay/shop-widget?writer=buyer&targets=&targets-hint=MiTubo%20development&default-sum=&button-text=14&payment-type-choice=on&hint=&successURL=&quickpay=shop&account=410015419471966).

