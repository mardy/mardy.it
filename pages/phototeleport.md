<!--
.. title: PhotoTeleport
.. date: 2018-02-01 23:35:00
.. tags: projectos
.. category: projectos
.. link: 
.. description: 
.. type: text
.. pretty_url: True
-->

<div style="float: right; text-align: center; margin: 1em;"><a href="/archivos/imagines/phototeleport/interfaccia.png"><img src="/archivos/imagines/phototeleport/interfaccia.thumbnail.png" border="0" width="270px"></a><br/><small><center>Just a few steps to get the job done</center></small></div>
How many photo shareing websites do you use? Especially if you are a
photographer, it's very likely that samples of your portfolio can be found in
two or more sites like Flickr, Facebook, Google Photos, 500px, etc. The easier
it is to stumble upon your photos, all the better for your popularity (and
business!).

[PhotoTeleport](https://www.phototeleport.com) is a photo uploader supporting
simultaneous uploads to multiple services. But it doesn't limit itself to
upload bare picture files: each photo is accompanied by its caption,
description and tags (according to each site's capabilities) in order to
maximize its discoverability.

### Features

* Support many photo sharing services: DeviantArt, Facebook, Flickr, Google Photos, VK, etc. (see [here](https://www.phototeleport.com/faqs/#services) for an updated list)
* Album selection, or creation of a new album (if the service supports it)
* Fast editor for captions, descriptions and tags
* Possibility to specify different captions, descriptions and tags for each website
* Read metadata embedded in the image files
* On upload completion, provide a link to the newly uploaded files for additional editing or sharing

This is just a beta version, but more features are planned, including:

* Ability to save and restore session
* Watermark support
* Image scaling
* Support for more plugins (500px, Imgur, Smugsmug, Shutterstock, Photobucket, Pinterest and more)
* Support for uploading to a custom (S)FTP server

### Learn more

To learn more about PhotoTeleport, as well as to keep up to date with its
development and releases, please visit [Phototeleport's
website](https://www.phototeleport.com), read its
[blog](https://www.phototeleport.com/blog/) and subscribe to the Atom/RSS feed.
