<!--
.. title: Oculo
.. slug: oculo
.. date: 2011-05-07
.. tags: projectos
.. category: projectos
.. link: 
.. description: 
.. type: text
.. pretty_url: True
-->

<div style="clear: both;">
<script type="text/javascript"><!--
google_ad_client = "ca-pub-6593712523528448";
/* Projecto */
google_ad_slot = "8209989807";
google_ad_width = 728;
google_ad_height = 90;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
</div>
<div style="float: right; text-align: center; margin: 1em;"><a href="/archivos/imagines/oculo.png"><img src="/archivos/imagines/oculo.png" border="0" width="400px"></a></div>

<h3>About Oculo</h3>

<p>Oculo is a homescreen widget for the Nokia N900 which can take any web site and render its contents into your homescreen. Not only it can render a complete web-page, you can also choose which specific parts of a web page you are interested in!<br/>
Latest news, sport results, stock market quotes, currency conversions, web comics, picture of the day... The internet is filled with dynamic content that you might want to bring into your phone homescreen, and this is what Oculo is for.</p>

<h3>Features</h3>
<ul>
  <li>Uses the <a href="http://www.webkit.org/">WebKit</a> engine to render the web pages</li>
  <li>Can render full pages, or specific elements</li>
  <li>Configurable widget size, scaling options</li>
  <li>Configurable interval between updates</li>
</ul>
<p>To see Oculo in action, see the video <a href="http://blog.mardy.it/2011/05/oculo-dynamic-web-content-to-your-n900.html">from this blog post</a>.</p>

<h3>Contributions and donations</h3>

<a href="http://flattr.com/thing/185586/Oculo-dynamic-web-content-widget" target="_blank">
<img src="http://api.flattr.com/button/flattr-badge-large.png" alt="Flattr this" title="Flattr this" border="0" /></a>
<p>I'm working on Oculo in my spare time, because I enjoy doing it. Nevertheless, tokens of appreciation are indeed welcome and will support my decision of keeping the software free.<br/>
Money donations are the most concrete way of supporting my work for Oculo, and stimulate me to spend more time on it. <a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=DLKNAWDRW4WVG">Follow this link to donate me a free amount via Paypal</a> &mdash; if you are donating with some specific feature request in your mind, don't hesitate to mention it in the donation description; I don't promise I'll ever fulfill it, but it might help.</p>

<h3>News and updates</h3>

<p><a href="http://blog.mardy.it">My blog</a> is best way for you to be informed about Oculo development. Note that I also blog about some other stuff, and often in <a href="http://www.interlingua.com">interlingua</a> (which is a nice international language, <i>que totes pote comprender</i>); but when blogging about software, I usually do that in English.</p>
