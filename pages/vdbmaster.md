<!--
.. title: VDBmaster
.. slug: vdbmaster
.. date: 2006-08-12
.. tags: projectos
.. category: projectos
.. link: 
.. description: 
.. type: text
.. pretty_url: True
-->

<h3><a name="introduction"></a>Introduction</h3>



<p>vdbmaster es un simple programma, publicate con <a title="Licentia GPL" href="http://www.gnu.org/licenses/gpl.txt">licentia GPL</a>, que ha le objectivo de facilitar le processo de masterisation de un DVD de filmatos.<br>

Illo utilisa growisofs pro scriber le disco, e le base de datos de <a href="http://www.splitbrain.org/Programming/PHP/VideoDB">VideoDB</a>) como referentia.



<h3><a name="usage"></a>Usage</h3>



<p>Le typic sequentia operative que on exeque con vdbmaster es:

<ol>

  <li>Decider qual filmatos masterisar: vdbmaster monstra le lista del filmatos catalogate in VideoDB que ancora non ha essite scribite, e permitte de selectionar un o plus de illos.

  <li>Creation de un directory con le files a masterisar (symlinks, ligamines symbolic), que le usator pote modificar -- typicamente pro adder altere files non presente in VideoDB.

  <li>Masterisation del DVD: growisofs (o un altere programma) es activate, e tote su output es visibile in vdbmaster.

  <li>VideoDB es actualisate con le information del identificativo del disco que ha essite create: le filmatos nunc resulta colligate al DVD.

  <li>Il es possibile eliminar del disco dur (hard disk) le filmatos registrate, pro ganiar spatio.

Le programma es simple e facilemente modificabile pro adaptar lo a altere situationes.

</ol>



<h3><a name="characteristicas"></a>Characteristicas</h3>

<ul>

   <li>Scripte in <a href="http://www.python.org">Python</a>, con le interfacie graphic <a href="http://www.gtk.org">Gtk+</a></li>

   <li>Supporto unicode (<a href="#imagines">vide le imagines</a>)

   <li>Le commando a usar pro effectuar le masterisation pote esser varie; le predefinite es <a href="http://fy.chalmers.se/~appro/linux/DVD+RW">growisofs</a>

   <li>Utilisa le base de datos de VideoDB pro presentar un selection del filmatos a masterisar; e lo actualisa al termino del masterisation

   <li>Supporto al internationalisation

   <li>Programma simple, breve e facilemente modificabile

</ul>



<h3><a name="imagines"></a>Imagines</h3>



<p>Alicun imagines de vdbmaster.

<table cellpadding="1em" border="0"> <tbody>

  <tr>

    <td class="imagine"><a href="/archivos/imagines/vdbmaster1.png"><img width="500"border="1" src="/archivos/imagines/vdbmaster1.png"></a><p>Pagina initial: selection del filmatos.</p></td>

  <tr>

    <td class="imagine"><a href="/archivos/imagines/vdbmaster2.png"><img width="500" border="1" src="/archivos/imagines/vdbmaster2.png"></a><p>Le output del programma de mesterisation &mdash; interrumpibile!</p></td>

  <tr>

    <td class="imagine"><a href="/archivos/imagines/vdbmaster3.png"><img width="500" border="1" src="/archivos/imagines/vdbmaster3.png"></a><p>Al fin del scriptura, on pote liberar spatio del disco dur, per eliminar le files masterisate.</p></td>

</tbody> </table>



<h3><a name="requisitos"></a>Requisitos pro le installation</h3>



<p>vdbmaster ha essite developpate e probate solmente in Linux, ma illo deberea functionar in un qualcunque computator que ha iste programmas installate:

<ul>

  <li><b>Python</b> 2.3 o successive

  <li><b>PyGtk</b> 2.0 o successive

  <li><b>VideoDB</b>

</ul>



<h3><a name="discarga"></a>Discarga e proba vdbmaster</h3>



<p>Pro discargar vdbmaster, clicca hic:

<center><a href="archivos/vdbmaster/vdbmaster-1.0.tar.gz">vdbmaster-1.0.tar.gz</a></center>

</p>



<h3><a name="installation"></a>Installation</h3>



<p>Le programma es distribuite con le <i>distutils</i> de Python; pro plure informationes super le installation con distutils, vide <a href="http://www.python.org/doc/inst/inst.html">le manual &ldquo;Installing Python modules&rdquo;</a>. Ma le programma es tanto simple, que probabilemente iste notas essera sufficiente in le major parte del casos.<br>

Decomprime le archivo e modifica le file vdbmaster.py (al capite del file, il ha alicun variabiles de configuration) e installa le script vdbmaster.py:

<pre>

tar -xvzf vdbmaster-<i>version</i>.tar.gz

cd vdbmaster-<i>version</i>

<i>...modifica le file vdbmaster.py...</i>

python setup.py install

</pre>

Le ultime linea es si tu vole usar distutils; alteremente, il suffice que tu copia le file vdbmaster.py in un directory listate in le <code>PATH<code> (i.e., <code>/usr/bin</code>).
