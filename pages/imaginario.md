<!--
.. title: Imaginario
.. slug: imaginario
.. date: 2015-05-09 23:35:00
.. tags: projectos
.. category: projectos
.. link: 
.. description: 
.. type: text
.. pretty_url: True
-->

**NOTE**: For the desktop version of Imaginario, [follow this link](http://imaginario.mardy.it)

<div style="float: right; text-align: center; margin: 1em;"><a href="/archivos/imagines/imaginario-main.png"><img src="/archivos/imagines/imaginario-main.png" border="0" width="270px"></a><br/><small><center>The main screen</center></small></div>
Imaginario is a photo manager application primarily developed for mobile
devices running Ubuntu. However, while the current version uses the
[Ubuntu UI toolkit](https://developer.ubuntu.com/api/apps/qml/sdk-14.10/UbuntuUserInterfaceToolkit.overview-ubuntu-sdk/)
for its user interface, the core of the application is portable and I
don't exclude to port it to other systems in the future. As a matter of
fact, I'd love to make a cross platform (Linux, OS X and Windows) desktop
version of it.

### Features

* Adding metadata to images: tags, geolocation (and soon title and description)
* When possible, metadata is embedded into the image
* Filter pictures by tags, date, geolocation
* Hide pictures containing specific user-defined tags
* Share photos via content-hub

That's certainly not an impressive list, but that's because Imaginario is still in its early stages. Some of the planned features include:

* Organizing tags hierarchically
* Duplicate detection
* Image versioning (when re-importing the same picture from an external editor)
* Face detection
* Face recognition and auto-tagging of people

### Screenshots

In no particular order:

<div style="float: left; text-align: center; margin: 1em;"><a href="/archivos/imagines/imaginario-view.png"><img src="/archivos/imagines/imaginario-view.png" border="0" width="480px"></a><br/><small><center>Viewing an image</center></small></div>
<div style="float: left; text-align: center; margin: 1em;"><a href="/archivos/imagines/imaginario-map-filter.png"><img src="/archivos/imagines/imaginario-map-filter.png" border="0" width="270px"></a><br/><small><center>Filtering by location</center></small></div>
<div style="float: left; text-align: center; margin: 1em;"><a href="/archivos/imagines/imaginario-settings.png"><img src="/archivos/imagines/imaginario-settings.png" border="0" width="270px"></a><br/><small><center>Defining hidden tags</center></small></div>
<div style="float: left; text-align: center; margin: 1em;"><a href="/archivos/imagines/imaginario-import.png"><img src="/archivos/imagines/imaginario-import.png" border="0" width="270px"></a><br/><small><center>The import prompt</center></small></div>
<div style="float: left; text-align: center; margin: 1em;"><a href="/archivos/imagines/imaginario-map.png"><img src="/archivos/imagines/imaginario-map.png" border="0" width="270px"></a><br/><small><center>Geotagging a photo</center></small></div>

