<!--
.. title: Mappero
.. slug: mappero
.. date: 2010-05-22
.. tags: projectos
.. category: projectos
.. link: 
.. description: 
.. type: text
.. pretty_url: True
-->

<div style="clear: both;">
<script type="text/javascript"><!--
google_ad_client = "ca-pub-6593712523528448";
/* Projecto */
google_ad_slot = "8209989807";
google_ad_width = 728;
google_ad_height = 90;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
</div>
<div style="float: right; text-align: center; margin: 1em;"><a href="/archivos/imagines/mappero-base.png"><img src="/archivos/imagines/mappero-base.png" border="0" width="400px"></a></div>

<h3>About Mappero</h3>

<p>Mappero is a map application for <a href="http://www.maemo.org">maemo</a>, running on the <a href="http://maemo.nokia.com/N900">N900 mobile computer</a>.</p>
<p>Formerly known as <a href="http://gnuite.com/nokia770/maemo-mapper/"><em>maemo-mapper</em></a>, Mappero has a totally new user interface and rendering engine, which makes use of the 3D accelerator in the N900.</p>

<h3>Features</h3>
<ul>
  <li>Support for different configurable maps sources: OpenStreetMaps, Google, Yahoo, Virtual earth and what not.</li>
  <li>Navigation: turn-by-turn voice navigation (with configurable route providers) in your own language<a name="note_lang" href="#f.note_lang"><sup>*</sup></a>.</li>
  <li>GPS tracking</li>
  <li>File import/export of GPX tracks and routes</li>
  <li>POI handling</li>
</ul>

<h3>Contributions and donations</h3>

<a href="http://flattr.com/thing/160950/Mappero" target="_blank">
<img src="http://api.flattr.com/button/flattr-badge-large.png" alt="Flattr this" title="Flattr this" border="0" /></a>
<p>I'm working on Mappero in my spare time, alongside with other software projects and <a href="http://photo.mardy.it">interesting hobbies</a>. Any help with code, <a href="http://talk.maemo.org/showthread.php?t=51405">graphics</a>, ideas is most welcome. See the <a href="https://garage.maemo.org/projects/maemo-mapper/">Mappero project page</a> to get access to the source code, forums, bug and feature requests tracker.</p>
<p>Money donations are the most concrete way of supporting my work for Mappero, and stimulate me to spend more time on it. <a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=GAVNC2A2UY6H6">Follow this link to donate me a free amount via Paypal</a> &mdash; if you are donating with some specific feature request in your mind, don't hesitate to mention it in the donation description; I don't promise I'll ever fulfill it, but it might help.</p>

<h3>News and updates</h3>

<p>I'm regularly blogging about Mappero in <a href="http://blog.mardy.it">my blog</a>, and that's probably the best way for you to be informed about upcoming features. As I also blog about some other stuff, and often in <a href="http://www.interlingua.com">interlingua</a> (which is a nice international language, <i>que totes pote comprender</i>), you might just want to follow <a href="http://blog.mardy.it/feeds/posts/default/-/english">my English blog feed</a>, in which I talk mostly about maemo and Mappero.

<hr/>

<div class="footnote"><a name="f.note_lang" href="#note_lang"><sup>*</sup></a> If your language is currently not available, you can contribute your own voice!</div>
