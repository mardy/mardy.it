<!--
.. title: Progetti
.. slug: progetti
.. date: 2019-01-23
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
.. pretty_url: True
-->

Una piccola selezione dei miei progetti personali:

.. post_list::
   :post_type: pages
   :categories: projectos
   :template: lista_projectos.tmpl
