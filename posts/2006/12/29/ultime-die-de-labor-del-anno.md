<!--
.. title: Ultime die de labor del anno!
.. slug: ultime-die-de-labor-del-anno
.. date: 2006-12-29 00:00:00
.. tags: Helsinki,labor
.. category: Helsinki
.. link: 
.. description: 
.. type: text
-->

Le retorno in Finlandia ha essite tranquille, a parte alicun problemas minor in le aeroporto: mi spuma de barba, que io portava in mi baggage a man, esseva sequestrate perque illo eccedeva le quantitate maxime admissibile. Isto es a causa del <a href="http://www.enac-italia.it/SecurityInformative/Informativa.htm">nove mesuras de securitate activate in le aeroportos del Union Europee</a>, que previde que le quantitate de liquido presente in cata contenitor non pote superar le 100 centilitros (mi flacon esseva de 200cl). Benque io non comprende totalmente le senso de iste restriction, io es felice de haber contribuite al lucta al terrorismo.
Le altere problema incontrate, es que quando io arrivava in mi appartamento io observava que mi valisa esseva rumpite; fortunatemente non in maniera tal que alicun cosa poteva exir, ma certemente le aqua pote entrar &mdash; e io debera considerar iste eventualitate, le proxime vice que io va utilisar le valisa. Le causa del ruptura non es certe, ma io suspecta que le responsabiles es le terroristas qui labora in le aeroportos.

Le dies sequente io ha retornate al labor in Nokia, cuje officios es inusualmente vacue: le major parte del empleatos es in vacantia, e le passos del presentes face echo in le corridor &mdash; especialmente illos de mi boss. Como consequentia de iste desertification io non recipe ulle signalation de problemas o requestas, lo que, juncte al facto que mi software es in un stadio stabile, implica que io es quasi disoccupate. Non a caso, io ha scribite iste message del labor, quando mi die laborative verte a su termino... De facto, isto es le ultime die laborative del anno 2006 e, cosa ancora plus importante, de hodie io pote esser certe que io es un empleato Nokia a tempore indeterminate: io esseva empleate le 28 de augusto, con un periodo de proba de quatro menses que ha expirate in gaudiose silentio! :-)