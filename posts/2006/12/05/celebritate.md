<!--
.. title: Celebritate
.. slug: celebritate
.. date: 2006-12-05 00:00:00
.. tags: amicos,Helsinki,cinema
.. category: amicos
.. link: 
.. description: 
.. type: text
-->

E ben, si! Finalmente io ha devenite un celebritate in tote le Finlandia, justo 20 minutas retro! <a href="http://www.yle.fi/tv2">YLE &ndash; TV2</a>, un del principal canal televisive finnese, monstrava un servicio integremente dedicate a <a href="http://www.jollydragon.net">JollyDragon</a> e pro alicun secondos io etiam appareva in illo.
Le filmato es disponibile in <a href="http://video.google.com/videoplay?docid=-3269224920489236691">Google video</a>, ma solmente mi amicos qui es presente in le video es authorisate a vider lo. ;-)

E in un magazin de Oulu, Äijä, il ha un articulo super JollyDragon con photos de un Weekly Welcome e un breve intervista a Zeeshan e a me. Infortunatemente io non ha un copia de iste magazin, ma spera que tosto le articulo sera disponibile in le sito internet de Jollydragon.