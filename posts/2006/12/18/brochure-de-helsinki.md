<!--
.. title: Brochure de Helsinki
.. slug: brochure-de-helsinki
.. date: 2006-12-18 00:00:00
.. tags: turismo,Helsinki
.. category: turismo
.. link: 
.. description: 
.. type: text
-->

Le officio touristic de Helsinki ha publicate un brochure que es disponibile in <a href="http://www.hel2.fi/tourism/en/esitteet_Helsinki.asp">plure linguas</a>, includite le italiano. Pro vider le version italian, <a href="http://www.hel2.fi/tourism/matko_esitteet/IT_Helsinki.pdf">clicca hic</a>!
Postea, veni a visitar me! :-)