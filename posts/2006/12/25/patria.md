<!--
.. title: Patria
.. slug: patria
.. date: 2006-12-25 00:00:00
.. tags: Italia
.. category: Italia
.. link: 
.. description: 
.. type: text
-->

<div style="float:right"><a href="http://www.mardy.it/archivos/active/1/315_large.jpg"><img src="http://www.mardy.it/archivos/active/1/315_large.jpg" width="300px"></a></div>
Salute, Italia! Post plus que tres menses e medie io ha retornate a mi terra native, ben que pro solmente pauc dies. Le viage de arrivata non esseva perfecte, perque le avion de Zürich a Venetia partiva con quasi un hora de retardo, sin que ulle motivation esseva explicate al viagiatores. Pro insister con le criticas, io adde que le aeroporto de Zürich es governate per un grande confusion, probabilemente causate per le facto que le Suissa non es un pais del Union Europee, e on debe passar le check-in de novo.
Del viage sur le Alpes io ha salvate alicun photos perque le nubes esseva tanto dense que illos pareva un terreno coperite per nive, e le summitates del montanias plus alte esseva ancora visibile e ben se integrava in le panorama nivate.

Durante mi permanentia in Italia io passa le tempore a incontrar mi amicos e parentes, sin oblidar le ex-collegas de labor. Totes es felice de revider me (o illes finge ben), e io equalmente. In le mesme tempore anque Andrea e Loco (mi fratre e su sponsa) es in Italia, assi que le familia es complete.

In Venetia io ha visitate le exposition de arte "Loco", dedicate al talento de Loco; illo non es multo extense, ma le operas merita un visita anque per le personas qui non es familiar con iste typo de arte (alicun exemplos es in le <a href="http://www.mardy.it/ia/taxonomy/term/36">Galleria del imagines</a>). Va a visitar lo!