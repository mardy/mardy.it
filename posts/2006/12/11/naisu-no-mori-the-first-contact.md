<!--
.. title: Naisu no mori: The First Contact
.. slug: naisu-no-mori-the-first-contact
.. date: 2006-12-11 00:00:00
.. tags: cinema
.. category: cinema
.. link: 
.. description: 
.. type: text
-->

Heri vespere io ha vidite le film plus estranie in mi vita: <a href="http://www.imdb.com/title/tt0451829/">Naisu no mori: The First Contact</a>. Illo es un film japonese, que non pote esser classificate in ulle genere cinematographic; illo es un collection de curte historiettas (non sempre sensate), que con le developpamento del film deveni in alicun modo relate &mdash; ma non bastante pro dar lo un senso.
Alicun del historiettas es normal, realistic; alteres es completemente absurde, e involve creaturas phantastic realisate in <i>computer graphics</i> que pare vermente real. Il es impossibile pro me narrar super iste film, e le unic ration pro le qual io ha scribite iste entrata es pro stimular vostre interesse &mdash; ma obviemente necuno lo reguardara, a minus que ille veni in Finlandia pro vider lo. ;-)