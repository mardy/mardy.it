<!--
.. title: Suomenlinna
.. slug: suomenlinna
.. date: 2006-10-29 00:00:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
-->

<div style="float:right"><a href="http://www.mardy.it/archivos/active/1/273_large.jpg"><img src="http://www.mardy.it/archivos/active/1/273_large.jpg" width="300px"></a></div>
In le <a href="http://www.mardy.it/ia/node/258">galleria del imagines</a> io ha cargate le photos de mi visita al fortalessa de Suomenlinna, que es in un insula proxime a Helsinki.
Le dies esseva incredibilemente splendente &mdash; ma anque extrememente frigide! Isto es le ration pro le qual in tote le photos io compare con guantos e con le bonetto del Italia.

Le altere duo personas que es presente in iste photos es Antonio e Diego, de Brasil. Io los regratia pro lor collaboration in prender le photos!

Un curiositate: nos incontrava altere italianos durante le viage, e nos discurreva un pauco. Illes veniva de Borno. Al fin, nos constatava que le mundo es vermente parve, perque nos ha un amico in commun: Luca Ghitti. Ciao Luca! :-)