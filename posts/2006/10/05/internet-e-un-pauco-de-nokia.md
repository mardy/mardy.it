<!--
.. title: Internet!! E un pauco de Nokia...
.. slug: internet-e-un-pauco-de-nokia
.. date: 2006-10-05 00:00:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
-->

Post un longissime tempore e multe impatientia, io ha finalmente un connection internet in mi appartamento! Per favor, invia me vostre congratulationes!
Isto significa que in le dies veniente io cercara de actualisar mi blog regularmente, perque io sape que il ha millenas de personas qui vole saper lo que io face. ;-)

Le labor in Nokia se revela multo interessante e stimulante. Mi collegas es multo sympathic; le plus proxime es Zeeshan (de Pakistan), perque ille es <em>single</em> como io &mdash; ergo nos ha un certe comunantia de interesses, e de tempore libere. In realitate ille ha un fidantiata, ma illa vive in Sancte Petroburgo e veni in Helsinki solmente de tempore in tempore, assi que ille es un &ldquo;quasi single&rdquo;.

Post que plure personas me ha privatemente demandate alicun informationes circa mi occupation in Nokia, io va reportar hic super lo que io face. Le prime cosa a dicer: <b>mi labor non concerne telephonos</b>!
Io labora in Nokia Multimedia, in le sector appellate OSSO (Open Source Software Operations) que se occupa del developpamento de applicationes Open Source. Ancora plus specificamente, mi equipa se appella RTCom (Real-Time Communications) e developpa solutiones pro le communication in tempore real (pro exemplo, programmas de chat con audio e video). Currentemente nos labora con le prototypo de un computator portabile que essera le successor del <a href="http://www.nokia.com/770">Nokia 770 Internet Tablet</a>, ma iste prototypo es coperite per le secreto industrial e io non pote revelar ulle detalio super illo.

Mi prime impression super mi labor es optime: le ambiente es amical, le horarios de labor es multo flexibile e le labor mesme es multo ingagiante. Pro le momento le rythmo non es stressante, perque il non ha scadentias multo proxime; ma io sape que nos debera completar alicun labores pro le septimana de Natal, e io time que isto significa que io non potera prender multe dies de vacationes in ille periodo (de facto, io non mesmo habera accumulate multe ferias...). Totevia le dies 24, 25 e 26 decembre es dies festive, ergo io succedera a tornar in Italia al minus pro pauc dies. Alora, lectores italian, nos va revider nos in decembre!