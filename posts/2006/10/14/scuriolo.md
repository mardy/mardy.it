<!--
.. title: Scuriolo!
.. slug: scuriolo
.. date: 2006-10-14 00:00:00
.. tags: Helsinki,natura
.. category: Helsinki
.. link: 
.. description: 
.. type: text
-->

<div style="float:right"><a href="http://www.mardy.it/node/207"><img src="http://www.mardy.it/archivos/active/1/207_large.jpg" width="300px"></a></div>
Iste matino, durante que io sedeva a mi computator (estranie, nonne?) io percipeva un movimento foras del fenestra; io ha reguardate e ha vidite un scuriolo (it: "scoiattolo", en: "squirrel") que saliva sur le arbore del jardin, e se arrestava sur un ramo. Io ha immediatemente prendite mi photocamera e le resultato es iste horribile photo que vos vide hic al latere.
Pro facer un photo melior, io ha aperite mi fenestra; ma si tosto que io faceva isto, le scuriolo descendeva del arbore e escappava velocissime sur le prato. Alora, vos debe contentar vos de iste photo.

Ma iste non es le prime scuriolo que io vide; io crede que io habera ancora opportunitates de prender altere photos, possibilemente melior. :-)