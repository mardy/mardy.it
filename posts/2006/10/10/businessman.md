<!--
.. title: Businessman
.. slug: businessman
.. date: 2006-10-10 00:00:00
.. tags: Helsinki
.. category: Helsinki
.. link: 
.. description: 
.. type: text
-->

<div style="float:right"><a href="http://www.mardy.it/node/204"><img src="http://www.mardy.it/archivos/active/0/204_large.jpg" width="300px"></a></div>
E como poteva io viver sin mi carta de presentation? Ora io ha 100 de illos, minus los que io ha jam distribuite a mi amicos e cognoscentias. Io sape que alicuno securmente pensara que io ha initiate a distribuer los a tote le feminas que io incontra in le strata, como si illo esseva un sorta de propaganda; pro iste ration io debe acclarar que isto non es del toto ver &mdash; solmente partialmente ver. ;-)

Le resto del cosas procede in lor curso natural: al labor io continua a reciper signalationes de problemas super le programma que io mantene, e toto lo que io debe facer es "acceptar" le signalation e specificar pro qual data io pensa de resolver lo. Io sempre pone le data del 43-esime septimana, ma solmente perque illo es le data plus distante que il es possibile selectionar. :-)
A illes qui se demanda quando es le septimana 43, io responde que io mesme non lo sape; ma tote le eventos e le scadentias que occurre in Nokia es datate con le numero del septimana &mdash non demanda me perque. Io totevia es felice que le septimanas se conta con un numero e que illos non ha un nomine; alteremente illos esserea 52 parolas in plus a apprender quando on studia un lingua!