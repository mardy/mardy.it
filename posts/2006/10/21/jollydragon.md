<!--
.. title: JollyDragon!
.. slug: jollydragon
.. date: 2006-10-21 00:00:00
.. tags: amicos,Helsinki
.. category: amicos
.. link: 
.. description: 
.. type: text
-->

In un discussion in le forum <a href="http://www.finlandforum.org">www.finlandforum.org</a> a proposito de amicitias in Helsinki, alicuno ha mentionate le sito <a href="http://www.jollydragon.net">jollydragon.net</a>, que io non cognosceva (estranie!). Io ha visitate le sito de JollyDragon, e lo ha trovate interessante: illo es un sorta de club, multo informal, que ha como objectivo le establimento de amicitias inter finlandeses e estranieros. On pote seliger inter le multe eventos proponite cata die, o mesmo crear noves, e vider si on trova altere personas qui condivide le initiativa.
Io ha decidite de partecipar al Weekly Welcome, un serata in un pub del centro, specialmente pensate pro le nove membros de JollyDragon qui vole introducer se e entrar a facer parte del gruppo. Isto esseva mercuridi, in Memphis. Il habeva circa 20 personas; le homines esseva principalmente estranier e le feminas esseva finnese &mdash; e multo attractive, on debe admitter. Le incontro esseva multo calorose: Paul, le fundator e le &ldquo;mente&rdquo; de JollyDragon es un juvene irlandese con un charactere tanto amical que io nunquam ha vidite in alicun altere persona: ille va a prender (in senso litteral) personas totalmente incognite de altere tabulas del local e les porta inter nos assi que illes face nostre cognoscentia. E, incredibilemente, isto functiona.
Io crede que io va participar sovente in lor activitates. Intertanto io ha initiate a facer propaganda pro illes: le poster de JollyDragon jam sta in le sala de café del Nokia.