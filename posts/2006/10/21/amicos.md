<!--
.. title: Amicos
.. slug: amicos
.. date: 2006-10-21 00:00:00
.. tags: amicos
.. category: amicos
.. link: 
.. description: 
.. type: text
-->

Isto es un serie (disordinate) de ligamines a sitos de mi amicos:
<ul>
<li><a href="http://yanello.blogspot.com/">Yanello</a>: le sito de mi fratre Andrea qui vive in Japon &mdash; in italiano, ma sin multe senso.</li>
<li><a href="http://www.daysleeper.jp/~loco/">LOCO</a>: le sito de Loco, mi soror affin (le mulier de Andrea) &mdash; in japonese.</li>
<li><a href="http://www.advogato.org/person/zeenix/">Zeenix</a>: le blog de Zeeshan, in anglese.</li>
<li><a href="http://www.flogao.com/tonikitoo">Tonikitoo</a>: le album photographic de Antonio, un mi amico brasilian que pro alicun menses labora in Helsinki pro Nokia.</li>
<li><a href="http://andreambro.blogspot.com">Andrea</a>: le prime italiano qui ha passate le portas del Nokia!</li>
<li><a href="http://simoniito.blogspot.com">Simone</a>: mi companio de ufficio, mi project-manager, mi co-anticaseista... &mdash; mmm... troppo de cosas in commun!</li>
<li><a href="http://gtrev.it">Andrea Nasato</a>: mi concitatano qui pro plure annos viagiava in le mesme traino, de Treviso al bellissime Padova &mdash attention: sito internet contaminate con Java!</li>
<ul>