<!--
.. title: Italian party
.. slug: italian-party
.. date: 2006-10-28 00:00:00
.. tags: amicos,Italia,Helsinki
.. category: amicos
.. link: 
.. description: 
.. type: text
-->

Illes qui crede que in Finlandia il non ha multe italianos, illes es in error! Le problema es que le italianos de Helsinki ama concentrar se in le mesme locos del citate, ergo il non es facile trovar les in le ambiente natural. ;-)

Simone e Anne (su fidantiata) organisava un "sabbato italian" in lor appartamento. Le data esseva establite ante alicun menses, pro evitar absentias o forsan pro preparar le appetito al degustation del productos del cocina sicilian.
Le matino esseva dedicate al preparation del cibo: <em>ravioli</em> e <em>cavati</em>. Io non pote parlar super ravioli, perque illos contine <em>ricotta</em>, que es un <b>ingrediente mortal</b> pro alicun empleatos italian in Nokia (e io pote critar con summe felicitate que <b>IO NON ES LE UNIC!</b>). Ma io es fer de haber contribuite al preparation del cavati in dar los le forma, con un enorme effortio de energias. Il esserea correcte dicer que le maxime parte del labor de preparation del pasta esseva realisate per le feminas (pro le italianos qui lege: "femina" in interlingua non es un termino negative) &mdash; ma post que io es intrinsecamente incorrecte, io non va dicer lo. Ravioli e cavati esseva servite con un sauce de tomates e carne de porco (finlandese?) cocinate per Simone.

Post le prandio, il esseva le torno del <em>tiramisù</em> de Andrea &mdash; excellente! Il esserea interessante saper si Fabio pote preparar lo equalmente ben! ...hint... :-D

Contrarimente a lo que eveniva con le miraculo del multiplication del panes e del pisces, in nostre caso al fin del prandio il non restava ulle sacco de cibo in plus.
E nos anque habeva discursos profunde e illuminante &mdash; ma io non va reportar los hic, perque io dubita que le lectores poterea comprender los completemente.

Il ha photos del varie phases del die in le <a href="http://www.mardy.it/ia/node/249">Galleria de imagines</a>. Un breve ma intense video super le preparation del ravioli es visibile <a href="http://www.youtube.com/watch?v=aCHVXLBSY0Y">in YouTube</a>.