<!--
.. title: Casino, Romario e altere
.. slug: casino-romario-e-altere
.. date: 2006-10-15 00:00:00
.. tags: amicos,Helsinki
.. category: amicos
.. link: 
.. description: 
.. type: text
-->

Jovedi postmeridie le section de Nokia Osso in le qual io labora habeva un party in le Casino de Helsinki, sin un particular motivation (o forsan il ha un motivation, ma necuno se interessava de illo).
Curiosemente le major parte del tempore passava in compania del guida, qui nos portava a vider le varie jocos del casino como si illo esseva un attraction touristic... Ma necuno prendeva photos.
Postea il habeva un joco organisate: cata uno de nos habeva 5 minutas de tempore pro jocar al slot-machine, e illes de nos qui faceva le maxime numero de punctos haberea vincite un premio (in ver moneta!). Estraniemente le vincitor non esseva io. Le joco esseva extrememente enthusiasmante, perque toto lo que on debeva facer esseva premer continuemente un button &mdash; il non importa quando, tote le joco esseva totalmente casual e le tempismo non importava (!!!). Un ver exercitio pro le intelligentia.

Postea, nos vadeva a mangiar in un del plus luxuose restaurantes de Helsinki; le cena costava 70 euros pro capite (ma illo esseva pagate per Nokia), e al fin necuno de nos esseva satiate. Forsan pro esser satiate il esseva necessari pagar 140 euro...

E ancora postea, nos saliva in le bar, ubi tote drinks esseva pagate per Nokia; io non es secur que illo esseva un bon idea, perque plure de nos esseva completemente ebrie, e alicuno mesmo non se presentava al labor le die sequente. Ma io esseva inter le 5 personas que resisteva plus (a star eveliate, non a biber!), e retornava a mi casa post 2:00 horas del matino.
E obviemente venerdi io esseva regularmente in mi officio a 9:00 horas, quando tote le section esseva quasi completemente deserte. Uh! Ma io ha recuperate le somno in le fin del septimana, non vos preoccupa pro me! :-)


Heri (e le prime horas de hodie :-) ) io esseva in un local con mi collegas Kevin (Chinese), Antonio e Diego (Brasilianos). Inter altere cosas, nos ha parlate de futbol e io non ha potite retener me del demandar al brasilianos an Romario joca ancora. Lor responsa me ha surprendite: si, ille joca ancora, ha 39 annos (quasi 40) e ille esseva le melior marcator in le ultime campionato brasilian!!! Le proxime anno ille jocara in Australia, con le objectivo de attinger le considerabile numero de 1000 marcaturas ("solmente" minus que vinti manca ancora). Io non ha parolas pro commentar!


P.S.: Io ha cargate in le sito alicun photos que io prendeva in le parco de Linnanmäki le 19 de septembre. Le photos es prendite con mi telephono, ergo le qualitate es assatis monstrose.
<a href="http://www.mardy.it/ia/node/209">Vide los hic</a>.