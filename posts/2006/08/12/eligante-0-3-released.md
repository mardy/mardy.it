<!--
.. title: eligante 0.3 released
.. slug: eligante-0-3-released
.. date: 2006-08-12 00:00:00
.. tags: eligante
.. category: eligante
.. link: 
.. description: 
.. type: text
-->

<p>New features in 0.3:</p>
<ul>   <li>Windows support</li>   <li>Support <a href="http://www.sqlite.org">SQLite</a> as DB engine</li>   <li>Lots of bug fixes!</li> </ul> To install this new version, <a href="http://sourceforge.net/project/showfiles.php?group_id=152477">click here</a>!<br />