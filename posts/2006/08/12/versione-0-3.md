<!--
.. title: Versione 0.3
.. slug: versione-0-3
.. date: 2006-08-12 00:00:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
-->

<p>Nuove funzionalità nella versione 0.3:</p>
<ul>   <li>supporto per Windows</li>   <li>Supporto per <a href="http://www.sqlite.org">SQLite</a></li>   <li>Correzioni di parecchi bug</li> </ul> Per installare questa nuova versione, <a href="http://sourceforge.net/project/showfiles.php?group_id=152477">clicca qui</a>!<br />