<!--
.. title: Contact me
.. slug: contact-me
.. date: 2006-08-12 00:00:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
-->

<p>You can contact me by these means:</p>           <ul>                 <li><strong>e-mail</strong>: <a href="mailto:mardy78@yahoo.it">mardy78@yahoo.it</a>, <a href="mailto:info@mardy.it">info@mardy.it</a> <br />        </li>             <li><strong>ICQ</strong>: 4469906</li>             <li><strong>AIM</strong>: Mardy che tardi</li>             <li><strong>Yahoo! Messenger</strong>: mardy78</li>             <li><strong>MSN Messenger</strong>: mardytardi@hotmail.com</li>             <li><strong>Jabber</strong>: mardy@jabber.linux.it</li>             <li><strong>IRC</strong>:</li>             <ul>          <li>Azzurra: Mardy (#treviso, #rusteghi)</li>               <li>FreeNode: Mardy (#linux-it, varie)</li>        </ul>             <li><strong>Skype</strong>: mardy78</li>      </ul>      I speak <strong>Italian</strong>, <strong>Interlingua</strong>, English and <strong>Bulgarian</strong>; I understand, up to a certain point, other romance languages.<br />