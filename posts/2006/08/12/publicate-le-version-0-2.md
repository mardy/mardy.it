<!--
.. title: Publicate le version 0.2
.. slug: publicate-le-version-0-2
.. date: 2006-08-12 00:00:00
.. tags: eligante
.. category: eligante
.. link: 
.. description: 
.. type: text
-->

<p>Post que le cambios que io ha operate in le ramo del CVS es multe e importante, io ha decidite de publicar le version 0.2 de eligante.<br /> Le cambios principal, respecto al version 0.1, es le sequentes:</p> <ul>   <li>Movite le configuration del modulos del file eligante.cfg al base de datos SQL.&nbsp; Isto significa que in le archivo eligante.cfg on debe solmente scriber le parametros de connection al base de datos SQL; tote le resto del configuration es facite via le interfacie web.</li>   <li>Le modulo mbox supporta le cassas postal in formato MH, maildir e MMDF (in ultra al formato mbox, que jam esseva supportate)</li>   <li>Correctiones</li> </ul> <p>Pro discargar le version 0.2 de eligante, <a href="http://sourceforge.net/project/showfiles.php?group_id=152477">pressa hic</a>.<br /> </p>