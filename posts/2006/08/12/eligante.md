<!--
.. title: Eligante
.. slug: eligante
.. date: 2006-08-12 00:00:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
-->

<h3><a name="novas"></a>Latest news</h3>



<?php $taxo_id = 6; ?>

<P>Here is a list of the latest news written in English; to see a more complete selection, please see <a href="taxonomy/term/<?php print $taxo_id ?>">all the news about eligante</a>.

<?php

/**

* This php snippet displays content of a specified type, with teasers,

* from certain taxonomy term.

* Sorted by date of creation, most recent first.

* Works with nodes of the flexinode type too.

* To change the length of the list, change $listlength.

* To change the taxomony term, change $taxo_id.

* To change the type of content listed, change the $content_type.

* Tested with Drupal 4.6.3

*/

$listlength="3";

$content_type = 'story';

$result1 = pager_query(db_rewrite_sql("SELECT n.nid, n.created FROM {node} n INNER JOIN term_node ON n.nid = term_node.nid WHERE n.type = '$content_type' AND term_node.tid = $taxo_id AND n.status = 1 ORDER BY n.created DESC"), $listlength);

$output = "<ul>";

while ($node = db_fetch_object($result1)) {

$n = node_load(array('nid' => $node->nid));

$output .= "<li>" . format_date($n->created, "small") . " - <a href=\"node/$n->nid\">$n->title</a>";

}

$output .= "</ul>";

print $output;

?> 





<h3><a name="introduction"></a>Introduction</h3>



<p>Eligante (or <em>e-ligante</em>) is a message collector and browser, released under the <a title="GPL Licence" href="http://www.gnu.org/licenses/gpl.txt">GPL licence</a> and written in <a title="Python" target="_blank" href="http://www.python.org">Python</a>.</p>

<p>The main activities of eligante are:</p>

<ol>

  <li>Getting access to a message source (e-mails, IM, IRC chat logs, web boards, etc.), retrieving the messages and storing them in a SQL server.</li>

  <li>If messages over different sources share the same sender, it's possible to create a unique identity for this user.</li>

  <li>Browsing the messages present in the SQL database.</li>

</ol>

<p>The first goal is achived by a series of modules, or engines (<i>motores</i> in interlingua) which fetch the messages from a specific source (ICQ logs, mailboxes, websites...) and store them in the database (currently <a title="MySQL" target="_blank" href="http://www.mysql.org">MySQL</a>).</p>

<p>The second point allows creating a virtual identity which ties together all the various identities a friend of yours might have in the various sources; for instance, if you had conversations with Marco both by e-mail and by an IRC chat, you can create an identity from him to be the merge of the two.</p>

<p>The third point will allow you to browse into the message database through a web interface; you'll be able to see all messages you had with Marco in an unified view, without having to navigate through the various modules. The interface provides some search functions, and also allows you to execute steps 1 and 2. Hence, all eligante can be controlled from your web browser.</p>



<h3><a name="characteristicas"></a>Features</h3>

<ul>

   <li>Several modules for fetching messages:</li>

   <ul><li><strong>E-mails</strong> in <code>mbox</code>, <code>MH</code>, <code>maildir</code> and <code>MMDF</code> formats</li>

       <li>IRC chat conversations (as logged by <strong>irssi</strong>)</li>

       <li><strong>gAIM</strong> conversation (therefore <strong>ICQ</strong>, <strong>MSN Messenger</strong>, <strong>Yahoo! Messenger</strong>, <strong>Jabber</strong>,...)</li>

       <li>The meeting website <strong>hi5.com</strong></li>

       <li>Several bulgarian websites: <strong>elmaz.com</strong>, <strong>flirt4e.com</strong>, <strong>sreshta.bg </strong> and others</li>

       <li>Other modules can be easily built (next ones will probably be <strong>orkut</strong>, <strong>skype</strong> and any others I'll hear of)</li>

   </ul>

   <li>Unified interface for browsing all messages</li>

   <li>Advanced search functions: in people names, in the subjects and in messages body. It is possible, for instance, to find all the messages in which you mentioned &quot;Linux&quot;.</li>

   <li>Unicode support</li>

   <li>Internationalization support: all the text in the web interface can be translated into other languages</li>     

   <li>Easy configuration and setup: the main settings (for connecting to the DB) are stored in a plain text file, while module configuration can be done via the web interface</li>

   <li>Integration with <a title="The address book" target="_blank" href="http://www.corvalis.net/address">The address book</a>, an electronic address book developed in PHP.</li>

</ul>



<h3><a name="historia"></a>History</h3>



<p>Eligante is a project started in the spring of 2005, to solve a real

problem: I had frequent conversations with many people over different channels

(especially websites) and in such a situation the risk of forgetting to answer

to some message is very big. Besides, I didn't remember what I talked about

with one or another people; for this reason, I needed a quick way for checking

my previous correspondence with someone, independently of the medium where the

actual conversation occurred.</p>

<p>Initially, eligante was just a mess of python scripts; but then I realized

it could be useful for other people too, so I decided to reorganize it in

something easy to develop and mantain. I added internationalization and a

simple web interface; while it's already in a fine usable state, there's still

much work to do, and I hope to find someone interested in it.</p>



<h3><a name="interlingua"></a>What's this interlingua?</h3>



<p>To tell it shortly, <a href="http://www.interlingua.com">Interlingua is the language I like most</a>; it's a natural language, no less than all national languages, it's simple, easy and sounds beautiful. It is the common denominator of the languages of the western world (English, French, Spanish, Portuguese, Italian), and millions of people can understand it at first sight.<p>

<p>You might wonder <em>why</em> I decided to use it in eligante (for the

comments and the names of variables and functions); well, while I don't have

nothing against the English language (apart from its pronunciation), I did want

to test how a community, born and grown with English as the only working

language, would react to a different point of view. I'm referring to the Open

Source community, which came to use English not by an unanimous decision, but

just by convenience; and nowadays <b>people not speaking English</b> have two

choices: either learn it, or <b>stay away from the Open Source community</b>.

<p>

Interlingua is just another language; it could be Spanish, Chinese or

Bulgarian. Though, I like Interlingua most because of it's simplicity, and

because many people can understand it. In fact, the reason why I'm writing this

text in English and not in Interlingua is not because less people would

understand Interlingua (<em>which is not true</em>), but because most people

<em>wouldn't even try to read it</em>. I want to demonstrate that the statement

&ldquo;I don't know this language, hence I don't understand it&rdquo; is plain wrong: from the premise that you don't know a language, you can only derive that you'll won't be able to express yourself correctly in such a language. But you might be able to read it, just make the effort!<br>

Think about this: you know Python and PHP, but you don't know Java and C. Would you conclude that you cannot read a program written in Java or C? Of course you can! Your reading will just be slower, but you'll make it. And the same can happen with natural languages &mdash; as long as they are close enough to the ones you know, of course.

<p>There are other reasons why I've chosen to use an extraneous language in my

project. I'm not going to write about <em>globalization</em>, because that

would take us too far, but I'm sure you got the idea. But there's some collision between the concepts of Open Source and the (implicitly forced) adopting of a single language which I feel almost as a contradiction.

As a nice conclusion of the subject, I'll leave you a question: why know 10

programming languages, but just one human language?<p>





<h3><a name="imagines"></a>Screenshots</h3>



<p>How could I not show some screenshots! The graphics of eligante is extremely essential, just what render it usable. This is a point for which I definitely need help.</p>

<table cellpadding="1em" border="0"> <tbody>

  <tr>

    <td class="imagine"><a href="/archivos/eligante/personas.png"><img width="200" border="1" src="/archivos/eligante/personas.png"></a><p>The list of identities eligante has bound</p></td>    

    <td class="imagine"><a href="/archivos/eligante/cerca.png"><img width="200" border="1" src="/archivos/eligante/cerca.png"></a><p>The search function</p></td>

    <td class="imagine"><a href="/archivos/eligante/liga.png"><img width="200" border="1" src="/archivos/eligante/liga.png"></a><p>Creation of a new identity: from each module, one can choose zero, one or more identities. They'll be all tied together</p></td>

  </tr>

  <tr>

    <td class="imagine"><a href="/archivos/eligante/prende.png"><img width="200" border="1" src="/archivos/eligante/prende.png"></a><p>The fetching of new messages</p></td>

    <td class="imagine"><a href="/archivos/eligante/address.png"><img width="200" border="1" src="/archivos/eligante/address.png"></a><p>eligante integrated into The Address Book</p></td>

  </tr>

</tbody> </table>



<h3><a name="requisitos"></a>Prerequisites for installing</h3>



<p>eligante has been developed and tested just on Linux, but it should work in any machine which runs this software:

<ul>

  <li><b>Python</b> 2.3 o later

  <li><b>Mysql</b> (better if version 4.0 o later)

  <li>(optional) The <b>Apache</b> web server wich CGI support can be used, but it's not required as eligante now comes with its own webserver

  <li>(optional) The Address Book

</ul>



<h3><a name="discarga"></a>Download and try Eligante</h3>



<p>I've tried my best to simplify the process of installing eligante and having it running, and I think I've reached a satisfying level. But should you encounter any problems, please post to the <a href="http://lists.sourceforge.net/mailman/listinfo/eligante-user">eligante-user mailing list</a>.<br>

<em>Your feedback is needed for the improvement of eligante!</em>

<p>To download eligante, click here:

<center><a href="http://sourceforge.net/project/showfiles.php?group_id=152477">Choose the latest version from the sourceforge.net website</a></center>

<p><br>

If you like emotions, you can try out the <a

  href="http://cvs.sourceforge.net/cvstarballs/eligante-cvsroot.tar.bz2">CVS

  version</a>. But please note that this version is always under development,

so it's extremely probable that something won't work as expected.</p>



<h4><a name="installation"></a>Installation</h4>



Uncompress the tar archive and follow the instructions in the <code>INSTALL.txt</code> file:

<pre>

tar -xvzf eligante-<I>&lt;version&gt;</I>.tar.gz

cd eligante-<I>&lt;version&gt;</I>

more INSTALL.txt

</pre>



<h3><a name="participa"></a>Get involved!</h3>



Eligante is registered as a project in <a

  href="http://sourceforge.net/projects/eligante">sourceforge.net</a>. If you are interested in contributing code for eligante, or you just want to follow the development, or ask for help, there are two <a

  href="http://sourceforge.net/mail/?group_id=152477">mailing-lists

waiting for you</a>.<br>

<a href="http://sourceforge.net"><img src="http://sourceforge.net/sflogo.php?group_id=152477&amp;type=2" width="125" height="37" border="0" alt="Logo SourceForge.net" /></a>
