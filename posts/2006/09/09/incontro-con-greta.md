<!--
.. title: Incontro con Greta 
.. slug: incontro-con-greta
.. date: 2006-09-09 00:00:00
.. tags: amicos,Helsinki
.. category: amicos
.. link: 
.. description: 
.. type: text
-->

Circa un septimana retro, io trovava in <a href="http://www.skype.com">Skype</a> un italiana qui vive in Finalndia, e ha initiate a <i>chattar</i> con illa. Illa se appella Greta e vive in Helsinki desde un mense, ubi illa labora pro su doctorato in biologia.
Nos ha excambiate nostre numero de telephono, e hodie nos nos ha incontrate presso le monumento a Sibelius &mdash; le loco que tote touristas qui veni in Finlandia veni a vider; io non mesmo lo ha photographate. :-)
Nos ha promenate in le centro, ubi nos ha mesmo incontrate mi collega Simone con su fidantiata (io non memora le nomine), Davide (un altere italiano qui labora in le sede del Nokia in Espoo) e un altere amica, qui forsan se appella Anna, ma forsan non. Le feminas esseva finlandese, ma le fidantiata de Simone parla italiano perfectemente.
Postea io e Greta ha cenate in un <em>fast food</em> (non McDonald!), e ha retornate cata uno per su strata. Ma, infortunatemente, mi strata esseva bloccate per le policia qui controlava un folla de manifestantes (e curiosos) circa alicun incontro politic europee que se teneva ille vespere in Helsinki (non demanda me qual incontro &mdash; io non lege quotidianos e non reguarda le television, supertoto si illos es in finnese!).
Al fin, post attender un medie hora con le sperantia que le policia nos permitteva de passar, io ha decidite de retornar in Pasila per le traino; isto ha functionate, e post un quarto de hora io esseva de novo in mi appartamento.
