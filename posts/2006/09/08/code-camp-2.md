<!--
.. title: Code camp, 2
.. slug: code-camp-2
.. date: 2006-09-08 00:00:00
.. tags: informatica,espoo
.. category: informatica
.. link: 
.. description: 
.. type: text
-->

Iste ha essite mi tertie e ultime die in le code camp; deman io non potera participar perque io essera ingagiate con le relocation al appartamento: io va signar le contracto, e sperabilemente io va mover me ibi in le mesme die. Ma super isto, io vos informara in le dies sequente.
Como io vos preannunciava, heri e hodie io ha essite le <i>phantasma</i> de Zeeshan: io sedeva proxime a ille, reguardante lo que ille faceva e, aliquando, io le demandava explicationes. A vices io mesmo le corrigeva; alora ille me ha dicite "Si, il es un bon idea que tu sede hic!" &mdash; io me senti honorate. :-)
Pro me, le code camp ha essite le occasion pro vider in action le prototypo del successor del <a href="http://www.nokia.com/770">Nokia 770</a>, que es &mdash; como io al fin ha discoperite &mdash; le producto con le qual io laborara in le menses veniente. Iste nove <i>Internet Tablet</i> es totalmente coperite del secretos industrial, ergo io non pote revelar vos ulle information; ma io crede que io pote dicer vos que io esseva favorabilemente impressionate.

Al fin del die laborative, nos ha consummate un <b>sauna</b>; illo esseva le prime sauna pro me, e io crede que io ha superate le <em>proba</em> con successo. De facto, io debe admitter que io me expectava alique ancora plus extreme, plus fatigante e choccante; ma si io lo compara con un cursa (o un altere activitate physic demandante) de medie hora, io trova que le sauna es multo "<em>soft</em>", in le senso que illo non stressa le physico, e que on poterea repeter lo durante tote un die sin fatigar se. O, al minus, iste ha essite mi prime impression. Un altere cosa a dicer, es que le respiration non es sempre facile, a causa del elevatissime temperatura del aere.
Io es gaudente de haber probate le sauna in compania; e post que nunc io sape que illo non es un cosa tanto terribile como io timeva, io potera etiam entrar in sauna sol &mdash; si con le appartamento io habera accesso a un.

Infortunatemente io non ha altere photos de iste dies a monstrar vos; ma Zeeshan e Naba ha capturate alicun photos e in le septimana sequente io va demandar les an illes vole condivider los con me. :-)