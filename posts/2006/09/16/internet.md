<!--
.. title: Internet
.. slug: internet
.. date: 2006-09-16 00:00:00
.. tags: Helsinki
.. category: Helsinki
.. link: 
.. description: 
.. type: text
-->

Un fortuna del empleatos Nokia es le possibilitate de obtener gratuitemente alicun cosas utile pro le labor: computatores e accessorios electronic, telephonos, bursas, CD e DVD, cursos linguistic (pro exemplo illo de finnese) e un rapide connection internet domestic.
Io ha inviate le requesta del connection internet pro mi appartamento un septimana retro, si tosto que io me ha establite in illo; ma infortunatemente &ldquo; rapide connection internet&rdquo; significa solmente que le datos passara rapidemente, e <em>non que illo essera <u>activate</u> rapidemente!</em>
Le tempore necessari pro le activation es estimate in 3-4 septimanas; io non pote comprender perque il es necessari tanto tempore ma anque si io eventualmente lo comprende isto non va cambiar le facto...
Le sol cosa que io pote facer es communicar vos iste information, assi que vos comprende perque io non actualisa mi blog regularmente &mdash; e, sperabilemente, me excusa.

P.S. Pro tote illes qui, in leger le paragrapho initial, ha pensate que io pote ordinar material gratuitemente a lor beneficio: <em>cancella le paragrapho e oblida lo!</em> Tote le cosas que on ordina debe esser approbate per le capo-officio e mesmo si ille es le sympaticissime Alexander (alias Sasha), isto non significa que toto es permittite.