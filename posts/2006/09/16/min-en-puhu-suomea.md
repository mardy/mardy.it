<!--
.. title: Minä en puhu suomea
.. slug: min-en-puhu-suomea
.. date: 2006-09-16 00:00:00
.. tags: Helsinki
.. category: Helsinki
.. link: 
.. description: 
.. type: text
-->

Le curso de finnese ha initiate! Iste septimana io ha habite mi duo prime lectiones, insimul con altere collegas del Nokia. De facto le curso initiava al initio de septembre, ma io e Zeeshan nos ha inscribite solmente ora e ha perdite le prime sex lectiones. Nulle problema! Nos es confidente que nos pote recuperar le tempore perdite, con un poco de effortio.
Pro isto, hodie io ha comprate duo dictionarios, uno portabile (io nunquam me movera del appartamento sin illo!) e uno plus grande pro mi studio.
Io nunc debe facer le exercitios que le maestra nos assignava pro le septimana veniente &mdash; illa es multo sever!