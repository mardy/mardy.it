<!--
.. title: Al labor!
.. slug: al-labor
.. date: 2006-09-04 00:00:00
.. tags: informatica
.. category: informatica
.. link: 
.. description: 
.. type: text
-->

Hodie il esseva mi secunde die de labor, ma de facto le prime die de activitate intelligente. :-)
De facto, venerdi io solmente visitava le palacio del Nokia, quasi como un tourista, e legeva con interesse le informationes presente in le intranet (informationes reservate al empleatos, ergo io non pote dicer nihil re los, non mesmo sub tortura).

Ma hodie io esseva productive e installava <b>Debian etch</b>, ancora in version beta, sur un del computatores portabile a mi disposition (duo Thinkpad del IBM &mdash; in le altere infortunatemente il ha <span style="font-size: 50%">Windows</span>). Le installation non esseva multo facile, perque un del requisitos de Nokia es que tote le discos debe esser cryptate; e le supporto al cryptation in <code>debian-installer</code> es ancora in phase beta. E, prevedibilemente, il habeva problemas: le procedura de installation non carga le modulo <code>dm_crypt</code> que es necessari pro operar con partitiones cryptate, ergo io debeva cargar lo manualmente. Un altere problema esseva que le partition de swap non esseva recognoscite correctemente, ma un recerca sur google me portava al <a href="http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=385317">solution</a> (io es secur que il ha millenas de personas interessate in iste detalios technic).

Deman e le die sequente io va probabilemente esser absente de internet: io va a facer compania a mi collegas que iste septimana ha un "code camp" (session intensive e collaborative de programmation) in Espoo, un citate proxime a Sofia. Io spera de poter retornar mercuridi, pro haber tempore de completar le questiones burocratic de mi permanentia in Finlandia.