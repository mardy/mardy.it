<!--
.. title: Code camp
.. slug: code-camp
.. date: 2006-09-05 00:00:00
.. tags: informatica
.. category: informatica
.. link: 
.. description: 
.. type: text
-->

<div style="float:right"><a href="http://www.mardy.it/node/175"><img src="http://www.mardy.it/archivos/active/0/175_large.jpg" width="300px"></a></div>
Le <i>code camp</i> es in plen activitate. Le sol exception es io, qui ancora es un <em>pisce for del aqua</em>. Io ha passate le die in leger tote le documentation que io trovava super <a href="http://dbus.freedesktop.org">D-Bus</a>, <a href="http://farsight.sourceforge.net">FarSight</a>, <a href="http://telepathy.freedesktop.org">Telepathy</a> e a sequer como un umbra mi collegas Simone e Naba (io ha discoperite que ille es le autor de <a href="http://www.anjuta.org">anjuta</a>, mirabile!).
Deman le victima va probabilemente esser Zeeshan, ma ille ancora non lo sape.

Io ha capturate un photographia del <i>code camp</i>, ubi on pote vider como le ver programmatores labora.