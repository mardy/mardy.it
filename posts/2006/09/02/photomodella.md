<!--
.. title: Photomodella
.. slug: photomodella
.. date: 2006-09-02 00:00:00
.. tags: Helsinki
.. category: Helsinki
.. link: 
.. description: 
.. type: text
-->

Heri, quando io ha retornate in le hotel post le mangiar, il habeva alicun personas sedite sur sedias in fronte al elevator, e un altere sedite sur le pavimento con un photocamera in mano. Il habeva etiam un umbrella blanc pro illuminar le ambiente, ma io non esseva in un bon position pro vider le subjecto del photographia.
In omne caso io debeva attinger le elevator pro vader a mi camera, alora io passava retro le personas e in facer isto io reguardava le subjecto: il esseva un femina e un homine, intra le elevator, vestite elegantemente. Infortunatemente le bellessa del femina me colpava tanto que io oblidava de facer la un photo (de facto, io crede que le altere photographo non lo haberea appreciate).
Pro iste ration, vos pote solmente creder a mi parolas, e <em>imaginar</em> quanto illa esseva belle!