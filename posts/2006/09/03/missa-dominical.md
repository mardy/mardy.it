<!--
.. title: Missa dominical
.. slug: missa-dominical
.. date: 2006-09-03 00:00:00
.. tags: Helsinki
.. category: Helsinki
.. link: 
.. description: 
.. type: text
-->

Iste matino io me eveliava post 9:00 (usualmente le lumines del aurora que penetra in le camera per le <i>non-troppo-spisse</i> cortinas me evelia <b>multo antea</b>), ergo un pauco troppo tarde pro participar al missa del 9:45 como io planava. (Pro illes qui pensa que 45 minutas deberea sufficier: istos include un abundante breakfast, le consultation del mappa pro trovar le ecclesia, e le transporto via tram)

Al fin, io arrivava al Cathedral de Henrikin pauco post 10 e un quarto. Io non portava mi photocamera, perque le die non esseva tanto solar; ma io habera securmente altere occasiones pro photographar le placia. Lo que il es opportun dicer, es que iste "cathedral" es multo plus parve que le ecclesia de Paese (mi village); multo belle, ma micre.
Totevia, illo es plus que sufficiente: al ceremonia del 11 horas il non habeva multe personas, e ancora il remaneva alicun spatios vacue in le bancos. Benque le missa esseva celebrate in finlandese, le maxime parte del participantes non esseva (o non semblava) finlandese: il habeva supertoto asiaticos, con pelle un pauco obscur (philippinos, probabilemente). De facto, io non saperea quante personas ha comprendite lo que le prestre ha dicite durante tote le hora del missa, ma io suppone que illes es minus que vinti.

Le proxime septimana, io cercara de eveliar me plus tosto: le missa del 9:45 es in latino, ergo il es probabile que io va comprender al minus le "Pater noster". :-)