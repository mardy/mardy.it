<!--
.. title: Efficentia
.. slug: efficentia
.. date: 2006-09-01 00:00:00
.. tags: Helsinki
.. category: Helsinki
.. link: 
.. description: 
.. type: text
-->

Le intersection del stratas e del ferrovias ha sempre essite un problema pro omne urbanistas. In Italia le problema ha essite solvite con super/sub passages o, in caso de insufficientia monetari, con un efficiente systema de barras que blocca le traffico stratal 15 minutas ante le passage del traino.
<div style="float:right">
<a href="http://www.mardy.it/archivos/active/0/159_large.jpg"><img style="width: 300px" src="http://www.mardy.it/archivos/active/0/159_large.jpg"></a>
</div>
Mi fratre me narra que in Japon on pote trovar mesmo trainos que curre sur ferrovias supra-elevate in respecto al stratas; o, in caso de passages a nivello, le barras blocca le traffico exactemente pauc secundas ante le passage del traino.
In Finlandia, le solution es simplemente genial: le traino passa a nivello per le strata, sin ulle barra a bloccar le traffico. Un minuta in avantia que le traino va passar, un sono de campana initia a esser emittite, assi que pedones, automobiles e tote le altere medios stratal es advertite; le libertate individual es sacre, ergo cata uno pote decider si arrestar se o passar.
Iste solution es de facto le plus economic. Io non comprende perque illo non ha essite establite in Italia...
