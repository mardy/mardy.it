<!--
.. title: La guerra di Mario
.. slug: la-guerra-di-mario
.. date: 2006-11-14 00:00:00
.. tags: reflexiones,Helsinki,cinema
.. category: reflexiones
.. link: 
.. description: 
.. type: text
-->

<a href="http://www.cinemavvenire.it/articoli.asp?IDartic=3975">La guerra di Mario</a> es un film italian ambientate in Napoli, que narra le historia de un infante "del strata" (Mario) qui es adoptate per un familia de bon stato social. Iste vespere io ha vidite iste film in compania de altere italianos e, contrarimente a mi expectationes, le film me ha placite. Forsan le motivation es justo le facto que io expectava nihil, e isto esseva plus que nihil. :-)
Le film non esseva multo realistic, in mi opinion alicun aspectos esseva excessivemente emphatisate, supertoto le permissivitate del matre adoptive qui de facto concedeva toto lo que Mario demandava e perdonava tote su trasgressiones (e a vices los laudava!).

Il es natural que, al termino del film, on se demandava qual effecto iste projection poterea haber super le publico finnese: le mundo representate es le summa del basse criminalitate. Io non sape quanto isto corresponde al realitate de Napoli &mdash; io lassa a alteres iste comparation &mdash; ma in omne caso io non es minimemente preoccupate per lo que pensa le finneses: illes sape certamente que un film non es necessarimente fidel al realitate. Si io debeva judicar le Japon o le Corea del Sud basante mi opinion super le filmes que io ha vidite, io dicerea que ille locos es invivibile: violentia e follia sin limites! (io obviemente non me refere a filmes como Godzilla o Gamera, que es clarmente imaginari)

Pro exemplo, io invita totes a reguardar un de mi filmes preferite, <a href="http://www.cinemavvenire.it/articoli.asp?IDartic=2641">Old Boy</a>. :-)