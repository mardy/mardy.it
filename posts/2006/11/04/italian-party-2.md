<!--
.. title: Italian party 2
.. slug: italian-party-2
.. date: 2006-11-04 00:00:00
.. tags: amicos,espoo
.. category: amicos
.. link: 
.. description: 
.. type: text
-->

<div style="float:right"><a href="http://www.mardy.it/archivos/active/1/302_large.jpg"><img src="http://www.mardy.it/archivos/active/1/302_large.jpg" width="200px"></a></div>
Le italianos se reuni ancora, in casa de <a href="http://andreambro.blogspot.com">Andrea</a> pro celebrar in avantia le anniversarios de nascentia de Lucia e <a href="http://simoniito.blogspot.com">Simone</a>. Le personas presente non esseva exactemente le mesme del septimana precedente (<a href="http://www.mardy.it/ia/node/248">Italian Party 1</a>), ma pro un singular <em>principio de conservation del italianos</em> le numero del italianos esseva constante (isto non es ver, ma le principio es comocunque digne de studio).

Iste vice le maxime parte del labor de cocina ha essite facite per Lucia, qui vince le premio "Miss Sacher 2006", e Anne, qui ha preparate le dulcissime "pulla"-s (io admitte que io non ha ben comprendite an isto es un producto finnese o sicilian &mdash; ma lo que importa es le resultato ;-) ).

Il ha duo videos del festa: <a href="http://www.youtube.com/watch?v=pk_3aJPmYEI">le sufflo del candelas del torta</a> e <a href="http://www.youtube.com/watch?v=HP4mvyYgHeU">le cartas de augurios (con surprisa!)</a>. Non perde los!

E il ha alicun photos in le galleria del imagines, ma infortunatemente io non ha succedite a prender ulle elemento compromittente.