<!--
.. title: Nove amico
.. slug: nove-amico
.. date: 2006-11-22 00:00:00
.. tags: amicos,Helsinki,labor
.. category: amicos
.. link: 
.. description: 
.. type: text
-->

<div style="float:right"><a href="http://www.mardy.it/archivos/active/1/308_large.jpg"><img src="http://www.mardy.it/archivos/active/1/308_large.jpg" width="250px"></a></div>
Finalmente io ha trovate un companion de appartamento! Heri io recipeva un e-mail que informava super le possibilitate de prender in proba alicun apparatos electronic, a scopo de studio. Si, <em>studio</em>!
Io ha examinate le varie possibilitates, e ha decidite que le objecto de studio plus interessante pro me es le <a href="http://wikipedia.org/wiki/Nintendo_DS_Lite">Nintendo DS Lite</a>. Io non sape pro quante tempore io pote usar lo &mdash; e non mesmo sape <em>que</em> io deberea facer con illo. Infortunatemente il non ha jocos, le unic cassetta disponibile es illo del navigator Opera (le DS ha un systema de connection Wi-Fi) ma illo es in lingua japonese e io non ha succedite a configurar le accesso al rete. Deman io probara in officio, ma in omne caso le cosa plus importante es que Andreea (99% del lectores pensara que isto esseva un error de dactylographia) sembla disonibile a prestar me alicun joco. Ah, le importantia del amicos... ;-)