<!--
.. title: Auronzo - Bivacco Tiziano - Bivacco Musatti
.. slug: auronzo-bivacco-tiziano-bivacco-musatti
.. date: 2022-07-25 15:34:46 UTC+03:00
.. tags: italiano,Italia,natura,turismo
.. category: 
.. link: 
.. description: 
.. type: text
-->

La giornata di ieri ha visto me e Yulia protagonisti di un paio di insuccessi
escursionistici, ma ci tengo comunque a buttar giù due note a riguardo, perché
mi riprometto di ritentare almeno una delle due imprese in un futuro prossimo.

L'obiettivo iniziale era quello di visitare il rifugio-bivacco Tiziano, sul gruppo montuoso delle Marmarole, a quota 2246 metri, partendo dalla Casa Alpina D. Savio (sulla strada Auronzo - Misurina), a circa 1000 metri di altitudine.

Il sentiero da seguire è il numero 260, che ci è parso da subito poco segnato:
non era ovvio il punto dove andava imboccato e, una volta trovatolo, dopo
alcune centinaia di metri l'abbiamo perso e ci siamo ritrovati in mezzo a una
radura che, se non fosse stato per il nostro desiderio di ricongiungerci al
sentiero, sarebbe stata paradisiaca: una distesa di piante di lamponi e
fragoline di bosco, dei cui frutti abbiamo goduto mentre ci facevamo strada
verso il sentiero principale.

<figure style="margin: 0 auto; width: 725px">
  <img src="/archivos/imagines/blog/2022-07-24 Tiziano.png" class="shadow-sm" />
  <figcaption>La nostra passeggiata:
  <ol>
    <li>La “radura dei lamponi”</li>
    <li>Il punto più alto che abbiamo raggiunto del sentiero verso il bivacco Tiziano</li>
    <li>La pista ciclo-pedonale lungo l'Ansiei</li>
    <li>Il punto dove inizia la fune ferrata nel sentiero verso il Musatti</li>
  </ol>
  </figcaption>
</figure>

Ritrovata la via maestra, abbiamo proceduto la salita, prima nel bosco e poi su
un ghiaione che, pur non presentando particolari difficoltà, ci faceva
preoccupare per il ritorno. Una volta completato, abbiamo incontrato un altro
escursionista che stava scendendo, avendo riunciato alla salita. Alla nostra
offerta di proseguire la passeggiata insieme ha riconsiderato la sua
risoluzione e si è rimesso in cammino dietro di noi. Il sentiero era parecchio
ripido, proseguendo a serpentina stretta, senza presentare spiazzi per soste o
viste panoramiche. Dopo un po', il nostro accompagnatore si è fermato ancora
una volta ed ha deciso di tornare indietro. Noi abbiamo proseguito fino ad una
quota di poco superiore ai 1500 metri, quando abbiamo incontrato un lastrone di
pietra poco invitante: il fatto che mancassero ancora circa 700 metri di
dislivello da coprire, e la paura di incontrare ancora parecchi di questi
lastroni, che ci facevano paura soprattutto in vista del cammino di ritorno, ci
hanno persuasi ad abbandonare la missione e a tornare a valle. Lungo il
ritorno, abbiamo incontrato un paio di gruppi di escursionisti che stavano
salendo al bivacco, ed abbiamo iniziato a dubitare della bontà della nostra
decisione. Poi, incontrato il ghiaione e avendolo sceso senza difficoltà
alcuna, ci siamo decisamente pentiti di essere tornati indietro e ci siamo
riproposti di riaffrontare la salita un altro giorno.

<figure style="margin: 0 auto; width: 640px">
  <video id="video" controls preload="metadata">
     <source src="/archivos/videos/2022-07-24 Teschio.mp4" type="video/mp4">
  </video>
  <figcaption>Un agghiacciante ritrovamento durante la discesa!</figcaption>
</figure>

Ritornati all'inizio del sentiero, abbiamo seguito la pista ciclabile lungo il
torrente Ansiei in direzione Misurina, con l'intenzione di consolarci
passeggiando un po' lungo il fiume, ma una volta incontrate le tabelle che
segnalavano l'inizio dei sentieri 226 e 279 abbiamo deciso di seguire
quest'ultimo, prefissandoci un tempo limite di 1 ora (ed essendo ben coscienti
che in un tale lasso di tempo mai avremmo potuto raggiungere il bivacco
Musatti).

La bellezza del sentiero, tuttavia, ci ha indotto a seguirlo più a lungo: la
parte iniziale si snoda interamente nel bosco, su terreno morbido coperto di
aghi di pini e foglie secche, abbastanza ripido e a tratti esposto (non alla
roccia, ma una caduta laterale di diversi metri può far male comunque, anche se
il terreno è superficialmente soffice!). Nel complesso, decisamente gradevole.

A quota 1300 circa si arriva a un piccolo promontorio sul quale si può riposare
e che offre una discreta veduta panoramica.

Proseguendo, il sentiero continua ad essere ripido, e diventa impervio dopo
quota 1600, dove abbiamo incontrato la fune metallica che aiuta a salire i
punti più difficili. A quel punto abbiamo rinunciato alla salita, ma questa
volta senza troppi rammarici, in quanto già lo scendere quei pochi metri di
roccia che avevamo scalato ci è sembrata un'impresa non facile. Sicuramente i
bastoni che avevamo non erano di aiuto, in quanto è opportuno avere entrambe le
mani libere per proseguire su questi passaggi; magari, se ci daremo un po' di
coraggio, anque questo percorso potremo riaffrontarlo in futuro.
