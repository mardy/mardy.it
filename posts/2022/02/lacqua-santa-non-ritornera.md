<!--
.. title: L'acqua santa non ritornerà
.. slug: lacqua-santa-non-ritornera
.. date: 2022-02-03 18:30:13 UTC+03:00
.. tags: Italia, actualitate, italiano, philosophia, reflexiones, religion, scientia, societate
.. category: 
.. link: 
.. description: 
.. type: text
-->

Forse ciò che mi ha colpito di più la scorsa estate, quando sono ritornato in
Italia in tempo di pandemenza, è stato trovare le acquesantiere vuote. La
sensazione provata in quel gesto abituale, stavolta culminato col tocco della
pietra asciutta con le mie dita, mi ritorna periodicamente alla memoria e mi
infonde un certo senso di incompletezza, decadenza, se non proprio di
sgradevolezza.

Sono tornato a rifletterci più di una volta, e — forse per il fatto di vivere
all'estero e di non aver ascoltato o letto le motivazioni ufficiali della sua
rimozione — la domanda più martellante che il ricordo dell'acqua santa mi
suggerisce è questa: abbiamo tutti perso la fede?

Già, perché se da un punto di vista scientifico sappiamo benissimo che la
benedizione del sacerdote non cambia la composizione chimica dell'acqua e non
le conferisce alcuna proprietà antivirale, sappiamo anche, da fedeli, che
l'acqua in cui intingiamo le dita è acqua *santa*. E ciò che è santo, per
definizione, ci avvicina a Dio, e non va temuto. L'acqua benedetta non si beve
a tavola, non si getta nel lavandino; al contrario, ci eleva: viene usata nei
battesimi, e negli esorcismi per scacciare i demoni. Nella cerimonia
dell'aspersione si prega con queste parole:

> Signore Dio onnipotente, fonte e origine della vita, benedici quest’acqua con
> la quale saremo aspersi, fiduciosi di ottenere il perdono dei peccati, **la
> difesa da ogni malattia** e dalle insidie del maligno, e la grazia della tua
> protezione. Nella tua misericordia donaci, o Signore, una sorgente di acqua
> viva che zampilli per la vita eterna, perché, liberi da ogni pericolo
> dell’anima e del corpo, possiamo venire a te con cuore puro. Per Cristo
> nostro Signore.[^1]

Altrettanto esplicito è il testo della benedizione in latino, laddove recita
*“ut creatura tua, mysteriis tuis serviens, ad abigendos dæmones morbosque
pellendos divinæ gratiæ sumat effectum”* (“affinché la tua creatura [l'acqua],
diventi un agente di grazia divina al servizio dei tuoi misteri, per scacciare
gli spiriti maligni e allontanare le malattie”). Fermo restando che va
condannato l'uso superstizioso dell'acqua benedetta così come di tutti gli
altri oggetti santificati e dei rituali, la fede ci impone di riconoscere che
il fedele autenticamente credente nel Signore non potrà corrompersi venendo a
contatto con l'acqua santa, indipendentemente da quanti virus e batteri ne
siano stati versati dentro.

Chi ha un minimo di fede sa benissimo che il Signore non permetterà che chi lo
cerca venga attaccato da una malattia trasmessa proprio da un oggetto
santificato; sa pure che, se una propria futura malattia fosse contemplata nel
disegno divino, questa si abbatterebbe su di lui nonostante vaccinazioni,
mascherine, distanziamenti sociali e aquesantiere vuote. Il che, si badi bene,
non significa non essere artefici della propria vita: significa riconoscere il
ruolo del sacro e il potere del Signore di operare nel nostro quotidiano.
Quindi non rassegnazione, ma ricerca attiva del divino.

È per questo che provo un certo rammarico e un profondo senso di delusione,
soprattutto rivolto alle autorità ecclesiastiche che, obbedendo ciecamente e
convintamente ai diktat del potere politico, vuoi con chiusure dei luoghi di
culto nei periodi del *lockdown*, vuoi con distanziamenti, mascherine e
rimozione dell'acqua benedetta, hanno dimostrato, nella mia percezione, di
riporre maggior fede nell'Organizzazione Mondiale della Sanità che nella
Provvidenza.

Temo che non sia stata colta appieno la pericolosità di questi cambiamenti per
quanto riguarda gli effetti a lungo termine sulla fede delle comunità. Comunità
che un tempo si meravigliavano per l'avventatezza dei religiosi che prestavano
le loro cure ai lebbrosi e ai malati di peste, incuranti della propria
incolumità e desiderosi soltanto di servire Dio e le sue creature. Non solo
oggi mancano esempi simili, ma anche la sola menzione del concetto di
"Provvidenza" non viene accettata, se non nei contesti più astratti.

La rimozione dell'acqua benedetta o, forse peggio ancora, il suo ritorno in
forma di un pratico dispenser, relega Dio nei piani più alti dell'iperuranio,
dove vivono le belle idee che mai, tuttavia, potranno influenzare la vita
terrena.

Io, senza vergognarmene, continuerò a credere che l'acqua santa non possa
contagiare il fedele.

[^1]: Dal Messale Romano, edizione 2020, pagina 990.
