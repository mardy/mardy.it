<!--
.. title: Looking for Qt support? Get in touch!
.. slug: looking-for-qt-support-get-in-touch
.. date: 2022-04-13 22:11:43 UTC+03:00
.. tags: Linux,Qt,english,informatica,kdeplanet,programmation
.. category: 
.. link: 
.. description: 
.. type: text
-->

Following The Qt Company's
[decision](https://forum.qt.io/topic/134724/unlock-qt-in-russia) to withdraw
support for the Russian market, KDAB's geolocation block of its website and
Upwork stopping its operations in Russia, there's a likely need of Qt support
in the country.

As a developer living in Russia and loving Qt (I spend a considerable amount of
my free time on Qt-based projects), I would find it very unfortunate if some
companies decided to switch to other technologies just for the lack of support.

**That's why, with this post, I want to advertise my willingness to help
companies deliver their Qt-based projects.**

I need to be honest and admit that I do have a [fantastic full-time
job](https://github.com/snapcore/snapd) and that's unlikely that I will be able
to dedicate more than 10-15 hours per week on this effort, but even this small
amount of time has been so far enough to drive several projects
([here's](https://github.com/ubports/media-hub/pull/28)
 [a](https://gitlab.com/ubports/core/fm-radio-service)
 [few](https://gitlab.com/mardy/photokinesis)
 [examples](https://gitlab.com/mardy/mitubo)); and what's more important, I
might be able to get more coding help.

I do have a [history of contributions to the Qt
project](https://codereview.qt-project.org/q/owner:mardy%2540users.sourceforge.net)
starting from 2012, developed for the biggest part in my spare time.


### Get in touch!

Whether you are a company or a Qt expert with some free time to spare and you'd
like to work with me, you are very welcome to [drop me a
line](mailto:info@mardyit). We can try to find a solution for those companies
who need Qt support, and create a network of professionals who can commit to do
some Qt work.

(While I mentioned Russia above, this idea is not limited to Russian companies
or developers in any way! Everybody is welcome, and I promise we'll stay
politics-free.)
