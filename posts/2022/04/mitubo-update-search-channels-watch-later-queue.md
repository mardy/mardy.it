<!--
.. title: MiTubo 0.8: search, channels, watch later queue
.. slug: mitubo-update-search-channels-watch-later-queue
.. date: 2022-04-10 19:06:30 UTC+03:00
.. tags: Linux,Qt,Ubuntu,english,informatica,kdeplanet,programmation,AppImage,MiTubo
.. category: 
.. link: 
.. description: 
.. type: text
-->

It has been a while since I last posted about
[MiTubo](https://gitlab.com/mardy/mitubo), despite releasing a few new versions
in the last months. But now I think that there is enough new stuff that's worth
a mention here.

<center>
<figure>
  <a href="/archivos/imagines/blog/Mitubo-search.png"><img src="/archivos/imagines/blog/Mitubo-search.png" width="80%" /></a>
  <figcaption>Search on YouTube</figcaption>
</figure>
</center>

Initially MiTubo only came with a search feature that was using Yandex video as
a backend; while that worked generally well, most of the returned results were
not playable due to youtube-dl being unable to extract the video information
from the returned web pages. So, now we have the option to search on YouTube,
via the [Invidious API](https://invidious.io/); furthermore, it's also possible
to search for channels, whose RSS feed can then be added as a subscription.

One other thing that has always bothered me is not being able to enqueue a new
video while watching another one, without having to pause the current one, go
back to the main page, add the new video to a playlist, go back to the current
video, and finally, once that is over, open the playlist and start the new
video.

<center>
<figure>
  <a href="/archivos/imagines/blog/Mitubo-dnd.png"><img src="/archivos/imagines/blog/Mitubo-dnd.png" width="80%" /></a>
  <figcaption>Drag and drop a new URL while watching a video</figcaption>
</figure>
</center>

So now we have that. One can drag and drop an URL (or even a longer text
containing several URLs) while watching another video, and a popup will appear
with a few choices (see the screenshot above). I've also added a “Next” button
next to the “Play” one, which is enabled if the “Watch later” playlist is not
empty, and does the obvious thing when pressed. Once started, the new video is
automatically removed from the “Watch later” playlist and moved into the
“Continue watching” list, where it will remain until it's being watched till
the end, which will cause it to part that list and be added to the watch
history.

To be fair, I haven't being testing this extensively, and this last one is a
feature I've developed just in a few hours during this weekend, so I wouldn't
be surprised if there are serious bugs in it. But hey, that's part of the
adrenaline which comes with hobby projects, I guess.
