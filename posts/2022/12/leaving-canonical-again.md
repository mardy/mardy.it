<!--
.. title: Leaving Canonical, again
.. slug: leaving-canonical-again
.. date: 2022-12-03 10:03:58 UTC+03:00
.. tags: Ubuntu,english,etica,informatica,kdeplanet,labor,philosophia,reflexiones,societate
.. category: 
.. link: 
.. description: 
.. type: text
-->

For the [second time](link://slug/looking-for-new-adventures), I'm being shown
the door at Canonical. Well, technically, this time it was me who handed over my
resignation, but that was only after I was told in very clear terms that I
would eventually be fired. No timeframe was given, but since I don't
particularly enjoy the feeling of checking my e-mail every morning to find out
whether this is the day when I'm being fired, I decided to take the initiative
and leave myself.

The reason? Those who know me well might suspect that it's related to some
complications with that fact that I'm living in Russia, or maybe with some
remarks I might have made about the war in Ukraine or about other current
events, since [I tent to be quite outspoken and
provocative](link://slug/principles-and-privileges). Nothing of all that: it's about my
**refusal to get vaccinated** against COVID-19; unfortunately, it has now
become apparent that I'm not the only one leaving, and other employees who have
refused either to get vaccinated or to disclose their vaccination status are
also being shown the door (including people who have been in the company for
more than 10 years). This has sparked some internal discussions in the company, and
several different point of views have been voiced: from those who welcome this
policy and would like to see it extended to flu vaccinations (which makes a lot
of sense, since once you've accepted to renounce your freedom in order to
protect the weak, you should accept it for all transmissible diseases), to
those who voiced concerns about the legality of this move, or would have found
this reasonable one year ago but not in the current situation as restrictions
are getting lifted and the current variants are less scary than the previous
ones; those who pointed out that being vaccinated has little impact on
transmissibility of the virus; that we are mostly a remote company and we could
instead have exceptions to allow unvaccinated people (or people with a weak
immune system) to remotely attend the few in-person meetings we have;
that as long as there are no vaccination mandates for plane flights and other
guests attending the same hotel premises where we meet, mandating employees to
get vaccinated might not help a lot; and whether this is a decision that a
company should make, or shouldn't it rather lobby the politics to have it
mandated at state level. I think there's merit to all these arguments, but I'm
personally not particularly interested in discussing any of them, since my
point is another.

Before talking about that, though, let me clearly set one thing straight: I
hate lies, and **Canonical's management is lying** about this matter. The
vaccination mandate measure is being justified on the grounds that it allows
employees to travel (something that I've been able to do as unvaccinated
throughout the last two years, even when restrictions were at their peak) and,
most importantly, to protect our weaker colleagues. This is what I find most
disgusting: using genuine feelings like love and compassion to justify
repressive measures. No, dear Canonical, this has nothing to do with protecting
the weak; not only because a vaccinated person can still spread the virus (and
our employees know this from first-hand experience), but also because, if this
was the real reason, then you'd accept people who have recently recovered from
COVID-19, since [immunisation after recovery is not worse than that of
vaccination](https://academic.oup.com/cid/article/75/1/e545/6563799); but you
don't, as I was explicitly told by HR that any previous infection is irrelevant.
It's also significant that you didn't establish clear rules about how often
one needs to get vaccinated, since all recent scientific literature on vaccine
efficacy shows that this is not a minor detail. Why not just be honest with
ourselves, and admit it's [just for
business](https://www.enr.com/articles/52481-us-announces-revised-vaccine-mandate-rules-for-federal-contractors)?
Being open about the fact that having a fully vaccinated workforce can grant us
access to more business deals would not change a lot in the practical life of
the (ex-)employees, but at least we won't feel that the company is treating us
as fools while embellishing its image with fake care and compassion. Or, if
there are other reasons, state them, because these ones don't stand up to logic
scrutiny.

Another thing that doesn't match (though maybe this is a timing issue, so I
cannot for sure call it out as a lie) is the fact that HR claims to have an
exemption process through which one could opt-out of the vaccination for
religious beliefs. Well, I was explicitly told in very clear terms by HR that
no exceptions would be made on either moral or religious grounds.  But maybe
this has changed since the time I was told this (mid October) and now?

Here, finally, let me state why I believe that such a mandate is wrong. The
first thing I want to put on the table is that even though I see very little
reason for this mandate (given all what we know about the virus mutability and
infectiousness, the shortcomings of the vaccines, etc. — by the way, if you are
into science I suggest reading [this
article](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC9062939/) which raises
some questions you won't hear in mainstream media and has a comprehensive
bibliography for further study), I recognize that in principle there are very
solid reasons for vaccination mandates, for example in the case where a virus
is extremely lethal, its symptoms otherwise uncurable and the vaccine is 100%
safe and highly effective. But even in that case, while getting vaccinated
myself, I would still oppose a mandate. Why?  Because of freedom, which trumps
everything. The choice is never between a healthy life and freedom: if there's
no freedom, there's no life worth living.  Even if some decision has very solid
reasons behind it, this doesn't automatically make it a good decision.

Let me make a few examples: if a company (I'm talking about companies here, but
the reasoning could be extended to states as well) decided that smokers will be
fired, or that those who drink alcoholics will be fired, or that you cannot eat
meat, or that you must take a pill whenever your head aches, or that
transgender people must undergo gender reassignment surgery, or that
everyone should wear a black band on their arm whenever a relative of a
colleague dies, or that employees' households must use the product made by the
employer, or that they have to excercise sports for at least two hours per
week, etc.; I would be categorically opposed to every single of these
impositions, despite recognising that there are reasons behind each of them,
and that I even dream of a world in which some of their goals are attained
(could we just all be fit and healthy?!). Because I think that personal freedom
is more important. You can always find good reasons to justify this or that
action; surely, if we think back at the fascist and totalitarian regimes of the
first half of last century, we must acknowledge that they were supported by the
(overwhelming?) majority of the population. An effective propaganda machine
could convince the population on this and that matter, but ultimately it's the
population who _reasoned_ and accepted that storytelling. Nowadays the
situation is different, but the mechanisms are the same, except that propaganda
has become way more effective ([or have we become
dumber?](link://slug/the-idiotism-of-software-developers)) and aligned over the
same direction, thanks to the globalisation process.

I'm well aware that societies are made of rules and therefore inevitably
restrict personal freedom: Western societies, for example, forbid nudity in
public places, and that's something I accept because it's part of my culture;
it's a rule deeply entrenched in our history, and I don't feel it as a burden.
I'm convinced, however, that the evolution of human society should be that, as
we become more conscious, we should be moving towards more free societies, with
fewer rules and more tolerant for diversity.
