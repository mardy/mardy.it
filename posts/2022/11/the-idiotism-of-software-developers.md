<!--
.. title: The “idiotism” of software developers
.. slug: the-idiotism-of-software-developers
.. date: 2022-11-08 20:11:55 UTC+03:00
.. tags: english,kdeplanet,informatica,matematica,philosophia,planetmaemo,reflexiones,religion,scientia,societate
.. category: 
.. link: 
.. description: 
.. type: text
-->

Before you get angry at me for this title, please let me state that I count
myself in the number of the “idiots” and, secondly, that what I mean by
“idiotism” here is not to be intended as an offence, but as some traits of
mindset which are typical of very logical brains.

Some months ago I finished reading Dostoevskiy's “The Idiot”, a book about an
exceedingly good-hearted man, prince Lev Mishkin, whose behaviour was puzzling
the people around him so much that they thought of him as an idiot. Sure, the
fact that he was suffering from epilepsy didn't help, but it was far from being
the primary reason for their thinking, since his epileptic seizures were very
rare (if I remember correctly, only two occurred during the time of the story)
and everybody's opinion had already formed well ahead of witnessing him in such
a state.

He was an idiot because he was open, trustful, and especially because he could
not “read between the lines” of what was been said to him: his social conduct
was straight, and although he was following at his best the customs that he had
been taught, he was supposedly awkward and unable to perceive and parse all the
messages that are implicitly conveyed by social behaviours and human
interactions. I added the word “supposedly” because, as a matter of fact, his
behaviours were all perfectly normal for me: I only noticed their awkwardness
when it was pointed out by the other characters, at which point I couldn't help
smiling and acknowledging that, indeed, that thing he did was weird.

However, he was a good and caring person, and not without talents: he had an
interest in calligraphy, and everybody liked to listen to him, as his speech
was insightful and his thoughts were original. I wonder how many of my readers
can identify themselves in such a character?

I definitely can. I won't get into the details, but I've felt many times on me
the amused or puzzled glance of people (like that time in high school when I
could not open a door in front of dozens of people, and I heard them say “So,
that guy is the genius of mathematics?” — I'll never forget that!), often
without understanding the reason for their reactions. Still, generally people
seem to like my company and be genuinely interested in talking to me.

So, what's wrong with prince Lev Mishkin, me, and maybe with you too? Well, a few
things, I would say. I'm not going to claim any scientific truth on what I'm
going to say, these are just my own impressions and deductions, which seem
to be shared by other people in the interwebs too, judging from a quick search
I did; take them for what they are.

The first thing I notice is some common traits between us and autistic people:
we tend to work better with things, rather than with people; we can focus on
a certain thing (work, a mathematical problem, a game) and forget about the
world around us; we have our unique hobbies, like solving puzzles, arguing
about a specific and very narrow topic, learning artificial (both human and
programming) languages; it's as if we needed to build a small, better world
where we would feel safe and at ease.

The other thing, which I actually consider harmful and which I put efforts to
change in my own life, is the fact that it's extremely easy to get us
interested into a specific aspect of a problem, and make us forget (or just not
notice) the big picture. That small part that we are looking at is stimulating
and challenging, and we are led to think that it's core of the issue, and maybe
of all the issues that affect our world. What is often missing is the ability
to take one step back and try to look at the issue from a different angle, and
especially the ability to listen for counter arguments; I mean, we do listen to
them, but since we have, in a way, “gamified” the issue, even when we think
that we are open to listen to the other side, we are in reality trying to win
the counter-arguments, rather than genuinely trying to understand them.

Another thing which we have, is faith. Yes, you read it right: even though the
IT world is probably the one with the highest percentage of atheists, men
always need something to believe in. We just don't realize it, but we do hold a
blind trust in certain persons and authorities. This does not mean that this
trust lives forever and cannot be broken, but this generally does not occur
because of a conscious realization of ours. Much more often than we'd like to
admit, the reason why we lose faith in a certain person or authority is because
*the rest of the persons and authorities that we trust has told us so*. In
other words, even if there's undoubtedly a reasoning of our own, the full
realisation and conviction occurs after having collected and compared the
opinions (or statements) of those we trust. The net result is that the IT
population is the one most trustful of the mainstream media, because it's the
mainstream media who has more “voice”: that's where the most _reputable_
journalists, scientists, activists are (and “reputable” is the key word here,
since this reputation is recursively created by the mainstream media
themselves or by their sponsors).

I might be biased by my own experience here, but it seems to me that there
isn't a group of people more homogenous in their political (and generally,
world) views than that of IT workers. When, in 2018, I saw the leaked video of
Google's co-founder Sergei Brin and other executives' reaction at Trump's
presidential victory, what I found most surprising was not the contents of the
speech, as they were mostly mainstream opinions, but rather the fact that all
this could be said in a company meeting. Something like this, I though, could
never happen in an European company, as political matters are a conventional
tabu in the work environment. But the point is that Brin and others could say
those words only because _they knew_ that the overwhelming majority of the
audience shared the same opinion. I don't think you could find the same
homogeneity of thought among shop assistants or philosophy professors.

Assuming that you have followed me this far into my rambling, and that you
recognize that there might be some truth in what I wrote, you might now be
wondering if there's a way to counterbalance our “idiotic” traits.
Unfortunately I don't have a full answer, as myself am only halfway there (but
maybe I'm too optimistic here? and does this road even ever end?), but there
are a few things that I think are absolutely worth trying:

- Talk with people. Better if face to face, or at least in a video call; just
  1-on-1, avoid groups, or you'll get on the defensive and try to defend your
  position for the sake of not losing the argument in front of an audience. But
  it's not a fight. Your goal when talking should not be that of convincing or
  getting convinced, but rather just to _understand_ the other points of view.

- Read both sides of the narrative. Try to see the other party's argument as
  they themselves present it, and not how it is presented in the media you
  usually read. Media often use this trick, to either invite “clown
  representatives” of the other point of view just to ridicule it, or they give them
  too little time, or extrapolate their answer out of context, just to make
  them appear unsensible.

- Always assume that other people are smart, and that no one is bad.

- Whatever the argument, try to answer the key question: “Cui bono?” (who
  profits?) to be at least aware of all the hidden interests behind this and
  that. They don't necessarily invalidate a position, but they must be
  considered.

- Lose faith. The only faith you are allowed to keep is the faith in God (or
  Gods), if you have it: but men, theories, institutions, authorities
  (including religious ones!), these must always be assumed to be imperfect and
  not blindly trusted.  People serve their interests or can be manipulated. Try
  always to start from a clean table and an empty mind, and see if they have
  enough arguments to convince you. 

- Do never assume “They can not _all_ be wrong” or “If this were wrong, at
  least some media would report it”. It just doesn't work this way, this is
  again a matter of having faith in the majority. Think of how many times in
  (recent) history you were presented an unambiguous truth, which later came
  out to be a scam (Iraq war being a famous one).

- Defocus. You might be spending a lot of energy into something that's not
  worth it. I mean, feel free to pursue whatever hobbies you like, as long as
  they make you feel better. But if you think you have a _mission_, think twice
  about it. Think about the world you'd like to live in, and whether/how this
  mission contributes to it.[^saudi]

- Ask questions. Be curious. Be challenging. For any topic, there are questions
  that have not been answered in mainstream media[^refugees]. Find the answer,
  then find explanations, never stopping at the first satisfactory one, but
  always get at least two competing answers. From here, ask more questions,
  rinse and repeat. And at every step ask yourself this: why didn't I know
  about this? Is someone trying to hide the truth from me?

- Aim at improving. Whenever you read something or talk to people, keep a
  humble attitude and try to be challenged. Your goal should be that every
  reading and every dialog should make you wiser, even if what you initially
  read and heard sounded like garbage. There are always reasons for all these
  thoughts you disagree with.

- Reach out to the people nearby. Try not just to be sympathetic to the needs of
  some population living far away from you, which the media has singled out as
  being those needing your compassion, and try instead (or in addition to that)
  to be sympathetic and helpful to the people around you. To your neighbours,
  to those you see in the public transport and, first and foremost, to your
  relatives.

Summing up, what I want you to realize is that we IT workers are easily
exploitable. All those thought manipulation techniques represent a problem to
everyone, but it's particularly with us that they tend to be especially
effective; as a matter of fact, I've found that awareness of how the power
controls us is higher among uneducated people, because they are more
distrustful of the media and just tend to consume less of it. We, on the other
hand, are not only well educated to respect the authority (see [Noam Chomsky on
education](https://www.youtube.com/watch?v=wv6TyJ1AbRM)), but our logical,
detail-focused mind can be easily externally controlled by continuously
stimulating it to focus on specific things and not others.

How would Dostoevskiy call us?

[^saudi]: I was recently surprised when I read people in a forum who were
discussing avoiding doing business with Saudi Arabia because of their human
rights record. Seriously? We are talking about a government who has indirectly
caused the death of more than 300 thousands people in Yemen, and your main
reason to criticize them is human rights? It's like asking the police to arrest
a killer because before the assassination he misgendered his victim! Yet the
elephant in the room continues to go unseen.

[^refugees]: My favourite one is: which country hosts more refugees from Ukraine?
