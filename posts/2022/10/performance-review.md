<!--
.. title: Performance reviews
.. slug: performance-review
.. date: 2022-10-27 08:47:14 UTC+03:00
.. tags: english,informatica,kdeplanet,programmation
.. category: 
.. link: 
.. description: 
.. type: text
-->

It happened a few times during my career, that I found myself in a team with a
colleague whose productivity was close to zero. In most of these cases it was
simply a matter of people who hadn't the skills and happened to choose the wrong
career path, and in one case it was actually an excellent developer, but just
slacking off. Regardless of the case, in many of these occasions it looked like
the team manager hadn't noticed the poor performance of the individual in
question, whereas this was rather obvious to the rest of the team.  I'm not
sure why the managers didn't notice the black sheep, but the point is that none
of the other developers did raise the issue either: why would I report a fellow
colleague, who might risk losing his job because of my evil tongue?

So, Scrum to the rescue? Not quite. As a matter of fact, while it is true that an
underperformer could be easily spotted by seeing how often he fails to complete
his stories in the timeframe suggested by the story points, this information is
generally accessible to the product owner, whereas the line manager might not
attend the Scrum meetings at all (as was the case in a previous project of
mine, where the line manager was completely detached from the project); and
even if the line manager had this information, it's not a given that he'd make
use of it — as a matter of fact, I cannot say with certainty that the line
managers did not notice those underperforming colleagues of mine; maybe they
noticed, but failed to intervene for some reason?

It sounds like this might be
[360](https://en.wikipedia.org/wiki/360-degree_feedback) material.
Unfortunately, in my experience the 360 reviews are a waste of time for the
most part, but that might be because they had been badly implemented in the
companies where I worked in.  In these reviews I get to rate my colleagues
using a set of predefined statements which generally look positive, such as
“Often delivers more than expected” or “Always meets the expectations”, but
each of which imply a very different rating.  I can see that the reason why the
system uses this kind of sentences is because no one wants to explicitly assign
a bad rating to a colleague, so all the possible answers have a “positive”
feeling.  The problem with this is that the shades of meaning in these
sentences is not obvious at all, so one risks ending up picking sentences
almost at random.

A system that would allow a company to get a honest feedback, without requiring
employees to say bad things about their colleagues, could be based on the idea
that your colleagues have usually a good sense of how well you are doing.  To
me, it would make more sense if the performance review consisted of a question
like this:

> Please make a list of those colleagues that in your opinion are more valuable
> to your team or to the company in general.

Then the company should sum up these lists and have a look at who is _not_
there, or whose name appears way too few times in relation to the number of
people who have worked with him or her. Then this information would not only be
available to the direct line manager, but also to upper line managers, who
might be willing to judge the situation with more objectivity and be able to
decide to move the person to another team.

The presentation of this question could indeed be very different from what I've
suggested here, for example it could be something like “Make a list of
colleagues you'd be most happy to work (or continue working) with”, or it could
include some personal feedback: in that way, if the only good thing that people
have to say about a developer is “He's a very nice guy”, well, you could
imagine that we are not dealing with a strong developer after all.

On the opposite side of the spectrum, the people whose names appear more often
in the _star colleagues_ lists are probably the employees that the company
should cherish and try hard not to lose. Salary increases, bonuses and all
other gratifications that can help in retaining them should be primarily
connected to the colleagues' direct feedback, rather than to semi-obscure
metrics which might not capture their real value.
