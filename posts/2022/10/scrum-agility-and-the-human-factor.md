<!--
.. title: Scrum, agility and the human factor
.. slug: scrum-agility-and-the-human-factor
.. date: 2022-10-29 10:17:14 UTC+03:00
.. tags: english,informatica,kdeplanet,programmation
.. category: 
.. link: 
.. description: 
.. type: text
-->

I've been working in Scrum teams for 15 years now, give or take. Different
companies, different approaches, from loosely following the agile principles to
a stricter implementation of the Scrum methodology. The only invariant being
that in practice Scrum is never followed by the book, but every company and
team makes its own adaptations, which makes it hard for everyone to voice
statements and critiques that could be considered universally true. That's why I
will refrain from taking this road, and instead I'll try to point out a few
aspects and behaviours that I've personally noticed during my career, good and
bad ones (but of course, since I'm old and bitter, more bad than good).

Scrum's focus on communication is, in my opinion, where most of its value lies
on: it's indeed important that other team members know what you are doing, and
that management has an idea of the progress being made. Hence daily standups
and the scrum board, and demos and retrospective at the end of each sprint.

It makes sense, on paper. And in practice as well, if you find yourself in a
team which is not really a team but a group of individuals with communication
problems. But that should not be the rule, and that's my main criticism of
Scrum: it's **a very good system for managing poorly skilled developers**.

In a highly skilled team, all of the benefits that Scrum is supposed to bring
are already a given: back in 2006-2008, in Nokia, most of our communication was
occurring in IRC and mailing lists. The fact that some of our co-workers were
[remote](https://www.collabora.com/) indeed acted as a push for this choice.
The same occurred in my early years at Canonical, where (at least in my team)
we had a video conference only once per week: if the communication is already
happening in an open (to the team members) place, there's no need for further
synchronisation points. And if the managers and product owners are also
monitoring these discussions, they know perfectly well how the situation is and
can report it further up.


### Demos

What about demos? Having always worked on middleware and system programming,
where there's very little to _show_, I've never been fond of demos. Our team
demos have always been boring and of little significance for most of their
spectators. And while it can take a lot of time to prepare a demo, if we
already communicated what he have done in this sprint, what's the point of
spending more time for the demo? Is it a lack of trust? Or is it a way to make
the team proud of its accomplishments?
In any case, the same could be achieved in a more efficient way: have a
specialized person (maybe from the QA team) prepare and run all the demos. In
this way you'd not only achieve those goals, but you'd also make sure that
there's no cheating (ehmm…) and that bugs are noticed right away.


### Story estimations

Luckily so far I've been involved in only two projects where it was required to
estimate the complexity of the backlog stories. But it was enough to convince
me that there's something fundamentally wrong with it. On one hand, we are
being told that we should estimate _complexity_, and not _time_, but then the
story points are being used to decide how many stories can fit in a
sprint. It can make sense to estimate the complexity, indeed, because it's an
intrinsic property of the story, which typically does not depend on the actual
person who sits down to work on it; but then what can we use this value for?
And how much time are we willing to spend in order to figure out this nearly
useless magic number? We keep the whole team sitting down together while
throwing estimates, and then question them; and indeed, in order to let other
people express a reasonable estimate we should also explain the story with a
certain level of detail. And while this sounds like a worthwhile thing to do
(explaining the story, I mean), it's probably better to do that in written
form, and ask the team members to read all the stories _before_ the grooming
session. This is what we have been doing at some point, except that most of the
time there would be at least one colleague who hadn't done his homework, so you
still had to either retell the story during the meeting, or have the complexity
discussion anyway since not all estimates were in agreement.

Having a clear description on all the stories (where a “clear description”
could even be just a link to a bug report, if that contains enough information)
does not cost a lot of time, as long as the description is written by a single
developer and not during a meeting with the whole team. It brings the benefit
that while writing the story one might realize that there's more to it and this
can lead to the creation of additional stories _before_ one actually starts the
sprint.

If, for whatever reason, a point score is absolutely required (just because
upper management wants so), then this should probably happen as the description
is being written, and can be set by the author alone; no need to gather the
whole team for this (since, in any case, the score is inevitably linked to how
the story's creator presents it).

### Sprint planning

It is my firm believe that a team member with no stories assigned should always
be allowed to drag the topmost story from the backlog (who should be kept in
the desired order by the product owner) into the sprint, without needing any
kind of approval from above.

Unfortunately, I've been working on teams where this was actively discouraged,
if not prohibited. The rationale being that all stories that we bring into the
sprint are a commitment, and that once the _TODO_ column is empty, everybody who
is free of other work should help their colleagues to complete their stories
which are still in progress.  This sounds good in theory, but in practice it
makes little sense, unless the stories still in progress can be reasonably
split into smaller parts (and most of the times they can't).

So, a lot of effort is made in order to plan a sprint so that all of its
stories can be completed, and at the end of the sprint one can look at the
burn-down chart and feel proud of that diagonal line having gotten so close to
the horizontal axis.  Except that everybody knows that we are just fooling
ourselves, because that beautiful graph is only the result of us undercommitting
on the sprint planning and then avoiding to bring in more stories into the
sprint once we had the resources to do so.


### Every developer is worth 1

It seems to me that Scrum works under the assumption that all members of a team
are interchangeable, and that it does not matter which developer takes up which
story. It has been like this at least in one of the projects I worked in: as
soon as you'd finish working on your story, it was mandatory to pick the
topmost story from the _TODO_ column, whatever it might be. Not the second or
the third one: you were allowed to pick the first one only. The reason for this
is that we had some stories that no one wanted to work on, and they'd risk
getting delayed forever.  But this “solution” caused another issue, because if
the topmost item in the _TODO_ column was an uninspiring story, this made so
that developers would be slower and hesitant in finishing their current
stories; no one would openly talk about that, but it was something you could
easily sense, as people started to find issues in their code, adding more unit
tests than usual, or “inadvertently” picking up the _second_ topmost story from
the _TODO_ column.
So, a problem that could have been easily solved by authority (just let the PO
or the lead developer decide who should do what) would become another source of
inefficiency.

A slightly related issue is the fact that if a team is very inhomogeneous, and
we estimate the stories just by their complexity, we can't really predict
whether our sprint will be a successful one, because it all depends on which
developer works on which story. Believe it or not, I happened to complete
in a handful of hours a story that a teammate had been working on for almost
one month (I assume he was also slacking off, but the point remains): at the
standups he was always reporting “good progress”, until I took the chance of
him having a day off and I stole the story from him.  One might argue that even if
a complexity estimation gives no forecast on when the story will be completed,
it can still be used in cases like this, to detect the underperformers.  This
is totally true, but that's the subject of [another blog
post](link://slug/performance-review).


### Rearranging developers in homogeneous teams

Besides, it should not come as a surprise that developers can have very
different skills, up to a factor of hundreds, if we take the extremes, and this
can be due to very different reasons: a low performance could be due to one
being a junior developer, to personality issues, or just to a congenic lack of
an analytic mindset.

If on one side it makes sense to mix junior and experienced developers in the
same team in order to boost the productivity and skills of the junior
developers, the same does not hold true for good and bad developers: if one
hasn't acquired the needed coding skills after a couple of years of work
experience, it's unlikely that continuing to have this developer work side by
side with more experienced ones will bring much benefit to him. On the opposite
side, it can stress out the more productive developer and demotivate him. Now
that a long time has passed, I can say that this is exactly what happened to me
in a couple of occasions during my career (and the fact that these colleagues
continuously argued and did not recognize their shortcomings did not help).

You will never get a perfectly homogenous team, since people always have
different experience and skills; anyways, a little of gradation is healthy,
because it stimulates all team members to compete in improving, by seeing each
other as a model to surpass. But this only works if the target is felt as
reachable (otherwise the lowest skilled developer won't have enough motivation)
and worth reaching (otherwise the highest skilled developer will feel he has
nothing to learn from the others).

That's why, in my opinion, it's worth rearranging teams along skill levels: the
teams with lower skill levels can benefit from being Scrum managed, because
Scrum sets up a frame inside which a low-skilled developer can still be
productive (albeit at a slower pace) thanks to the continuous monitoring and
feedback.  Highly skilled teams — which, incidentally, does not only mean good
coders, but also good teammates with prioritization and communication skills,
and understanding the project goals and nearly capable of managing themselves —
could do very well even without Scrum, and just need a project owner who sets
the goals and defines the high-level stories (generally called “epics”), which
they can then break into smaller tasks by themselves. Adding more Scrum to
such teams might not lead to any improvement.


### What about managers?

I tend to think on a similar vein when it comes to managers (be it project
manager or product owner): my impression is that Scrum is most beneficial when
the team has a poor manager, because it sets some rules that make the figure of
the manager less relevant and reduce it to a role that can be easily performed
by almost anyone. Here I might be totally wrong, of course, since I've never
worked as a manager and there are aspects of their job that are less visible
and that I might be overlooking; but again, I think that a good manager makes
Scrum redundant. Being crystal clear about the team goals, setting up a work
environment that stimulates every team member to give his best, being able to
notice early when things are not going as planned or when someone is
underperforming or not feeling at ease, making sure that the current
development stage is well understood by the rest of the project, etc.: all
these things are also Scrum's goals, so if they are already happening, the
company should cherish this manager as a role model and question whether
Scrum would be really beneficial here.


### Conclusion

In other words, summing up, one company/department should consider introducing
Scrum once a problem has been detected; “as long as the boat goes, let it go”,
as a [popular Italian song](https://www.youtube.com/watch?v=pVGGCf_i1y4) says.
