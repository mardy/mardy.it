<!--
.. title: MiTubo 1.4 adds feed folders
.. slug: mitubo-14-adds-feed-folders
.. date: 2022-10-10 17:40:09 UTC+03:00
.. tags: Linux,Qt,Ubuntu,english,informatica,kdeplanet,programmation,MiTubo,QML
.. category: 
.. link: 
.. description: 
.. type: text
-->

Exactly one month has passed since the previous release, just the right time
needed to complete the feafure I've been working on since several weeks and to
fix a few bugfixes introduced with the previous release. So it's time a new
release of [MiTubo](https://gitlab.com/mardy/mitubo):

<center>
<video id="video" controls preload="metadata" width="100%">
   <source src="/archivos/videos/mitubo-1.4.webm" type="video/webm">
</video>
</center>

I realized that I'm not that good at making release videos, but the point of
the video above is to show that you can organize your feeds into folders. When
clicking on a folder, a page opens with the folder's contents; but you can also
directly click on a feed, as long as its preview is visible in the folder's
delegate, and then the feeds open directly. This means that if you organize the
feeds inside your folders so that the favourite ones are at the top, they'll
also be visible in the folder preview and you'll be able to jump to them in
just one click.

Maybe I'm not that good with textual explanations either, so why don't you
check it out for yourself? ☺  Get it at
[mardy.it/mitubo](http://www.mardy.it/mitubo) (builds for Linux, Ubuntu Touch,
Windows and macOS are available)!
