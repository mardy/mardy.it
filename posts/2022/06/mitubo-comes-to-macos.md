<!--
.. title: MiTubo comes to macOS
.. slug: mitubo-comes-to-macos
.. date: 2022-06-22 22:44:47 UTC+03:00
.. tags: Linux,Qt,Ubuntu,english,informatica,kdeplanet,programmation,AppImage,MiTubo
.. category: 
.. link: 
.. description: 
.. type: text
-->

I just released [MiTubo 1.2](/mitubo/#downloads). New in this version:

- As suggested by `alphas12` in the comments, I added the author name in the
  YouTube search results.
- In the same results list, there's now a clickable link to the channel, which
  makes it easier to subscribe to it.
- Improve layout of some pages on narrow displays (though there's still much to
  be done!).
- Skip invoking youtube-dl if the video information is already encoded in the
  page `HEAD` meta properties.
- Remember the preferred playback resolution; this can be helpful on low
  bandwith connections.
- First macOS release!

While bringing in the macOS version, I updated the [QScreenSaver
library](https://gitlab.com/qt-goodies/qscreensaver) to support inhibiting the
screensaver on macOS too.

I also tested the AppImage on openSUSE, and it seems to work fine there too.
So, fewer and fewer people have valid excuses not to try out MiTubo!
