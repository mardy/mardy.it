<!--
.. title: MiTubo 1.1: screensaver inhibitor
.. slug: mitubo-11-screensaver-inhibitor
.. date: 2022-06-04 13:46:54 UTC+03:00
.. tags: Linux,Qt,Ubuntu,english,informatica,kdeplanet,programmation,AppImage,MiTubo
.. category: 
.. link: 
.. description: 
.. type: text
-->

Looks like I'm posting a bit too often about [MiTubo](/mitubo), but don't
worry, I'll soon find something else to write about.

Version 1.1 is now released, bringing you:

- A screensaver inhibitor, at last!
- AppImage now works in Arch and Manjaro

More in detail, this means that your computer won't go to sleep or start the
screen saver while you are watching a video. To achieve this, I wrote a
portable (well, for the time being it only supports Linux and Windows, but
macOS support will eventually arrive too) library for inhibiting the
screensaver: [QScreenSaver](https://gitlab.com/qt-goodies/qscreensaver). It
lives in its own repository, and it's written in a way that should be easy to
integrate with your own project. You are welcome to try it out (and add support
for cmake/qmake/meson/…).

The AppImage support has been improved after a user filed a bug about MiTubo
not working in Arch; I myself could not verify the issue as I've grown too lazy
to install a distribution like Arch, but I did it on Manjaro (which is also
based on Arch), and indeed the MiTubo AppImage contained some unnecessary
libraries (while missing some others) that rendered it non-functional in those
distributions. Now it's all fixed, so there's a good chance that the AppImage
will work on your distribution, too.
