<!--
.. title: Mitubo 0.9: multiple concurrent video downloads
.. slug: mitubo-09-multiple-concurrent-video-downloads
.. date: 2022-05-05 22:16:40 UTC+03:00
.. tags: Linux,Qt,Ubuntu,english,informatica,kdeplanet,programmation,AppImage,MiTubo
.. category: 
.. link: 
.. description: 
.. type: text
-->

It will never stop surprising me how easy it is to implement big new features
in a QML application! The assumption here is that the C++ part of the
application should be well-written: objects should not be overloaded with
unrelated functionalities just because it seems faster to code them that way,
but one should rather design classes so that each exposes *one* functionality,
and then QML and javascript act as the glue which binds all the parts together.

In a way, **QML stands to C++ classes like the POSIX shell stands to
command-line tools**: a simple language which allows concatenating small units
of functionality together to build a powerful program.

Anyway, that was not what I wanted to talk you about today. ☺  Today's post is
about [MiTubo](https://gitlab.com/mardy/mitubo), whose version 0.9 has been
released today:

<center>
<video id="video" controls preload="metadata" width="100%">
   <source src="/archivos/videos/mitubo-0.9.webm" type="video/webm">
</video>
</center>

The big feature in this release is download of audio/video files: I thought,
since I'm using [yt-dlp](https://github.com/yt-dlp/yt-dlp) (or
[youtube-dl](https://youtube-dl.org/) on Ubuntu Touch) anyway for
extracting video streams, why not add an option to let users download the media
content? This turned out to be easier than expected, so if you were looking for
a graphical frontend to the YouTube downloader, well, now MiTubo is an option
you could try.
