<!--
.. title: MiTubo 1.0: playlist support, new “website”
.. slug: mitubo-10-playlist-support-new-website
.. date: 2022-05-21 15:53:56 UTC+03:00
.. tags: Linux,Qt,Ubuntu,english,informatica,kdeplanet,programmation,AppImage,MiTubo
.. category: 
.. link: 
.. description: 
.. type: text
-->

Some news from the MiTubo world:

- Version 1.0 has been released!
- It's also available for Windows (boo!)
- Some basic support for remote playlists
- New “Check for updates” dialog
- Added support for translations
- Added Italian translation, of course
- Minor cosmetic changes (like using a different unicode symbol for the “Back” button)
- New web page for MiTubo

Expanding a bit on the points above, the first thing worth saying is that the
choice of releasing this version as “1.0” does not mean that it's more stable
than the previous ones; it just means that I'm rather satisfied with the
feature set, and that I believe that the program is ready for more widespread
use.

This is also the reason why I decided to prepare a web page for it:
[mardy.it/mitubo](/mitubo). I didn't go for a completely separate website,
unlike what I previously did for [Mappero Geotagger](https://mappero.mardy.it),
[PhotoTeleport](https://phototeleport.com) and
[Imaginario](https://imaginario.mardy.it) (which reminds me that I haven't been
working on the latter for a long time! I should try to correct this soon!),
both because this way it's simpler to publish news about it (I'll continue
doing that here, instead of cross-posting in two sites), and because having it
in the same domain might be mutually beneficial for the SEO ranking of the blog
and of MiTubo.

As for the Windows version, I want to thank [once
again](link://slug/new-website-for-mappero-geotagger)
the [MXE project](http://mxe.cc) for their fantastic cross-compiling suite. I
find it very cumbersome working in Windows, and being able to build my programs
from Linux makes my life a lot easier (if you want to have more information
about how this works with QBS, have a look at the previous MXE post). I wish
there was something similar for macOS; and that's why the macOs version is
going to take more time to arrive — on the other hand, I haven't received any
requests for it, so I'm not in a hurry to work on that.

Last but not least, translation support means that if you want to help with
translations, now you can. I've myself tried
[QtLinguist](https://doc.qt.io/qt-5/linguist-translators.html) for the first
time to write the Italian translation, and I found it to be an extremely
effective tool, once you learn the key bindings by heart.
