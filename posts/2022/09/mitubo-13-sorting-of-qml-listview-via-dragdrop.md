<!--
.. title: MiTubo 1.3: sorting of QML ListView via Drag&Drop
.. slug: mitubo-13-sorting-of-qml-listview-via-dragdrop
.. date: 2022-09-10 13:38:51 UTC+03:00
.. tags: Linux,Qt,Ubuntu,english,informatica,kdeplanet,programmation,MiTubo,QML
.. category: 
.. link: 
.. description: 
.. type: text
-->

One feature that I've been asked to add to
[MiTubo](https://gitlab.com/mardy/mitubo), and that indeed becomes more and
more important as the number of subscriptions increases, is the ability to
group subscriptions into folders. I've spent a good amount of time implementing
the needed support in the C++ backend, which is now able to handle nested
folders too, but given that building the UI parts was not a quick task and
seeing how much time has passed since the last release, I thought of releasing
a partial implementation of the whole feature, consisting only of the ability
to manually sort the subscriptions via drag&drop (that, is no folder support).
It turns out this is already not a trivial work!

I found a [nice tutorial on ListView DnD
sorting](https://agateau.com/2016/reordering-a-listview-via-dragndrop-3/) by
the great Aurélien Gâteau which I found very inspiring, and while I didn't
actually reuse the same code (mostly because I was already halfway through with
my implementation, which I started before finding his tutorial), it was helpful
to have it as a reference. I added a few animations to make it look more
pleasant, and I'm rather satisfied with the result:

<center>
<video id="video" controls preload="metadata" width="100%">
   <source src="/archivos/videos/mitubo-1.3.webm" type="video/webm">
</video>
</center>

I'm not showing you the code yet (though, indeed, you can find it in the
`DraggableListView` and `DraggableDelegate` items [in the source
repository](https://gitlab.com/mardy/mitubo/-/tree/master/src/desktop/qml))
because it's not yet in a shape where it's generally reusable in other
projects, but if I happen to need the same feature elsewhere I'll eventually
try to turn it into a couple fully reusable components.

Anyway, here's what's new in this latest MiTubo release:

- Subscriptions can be sorted by means of Drag&drop
- For systems with python 3.5 or older (such as Ubuntu Touch), use the daily
  builds of [youtube-dl](https://youtube-dl.org/) instead of the official
  releases
- Implement **PeerTube search** (this should reserve a post of its own!)

You can get it [at the usual place](http://www.mardy.it/mitubo). This time
there are only Linux and Windows builds as I'm a bit lazy to make a macOS
version, but should you need it, don't hesitate to ask!
