<!--
.. title: The Media
.. slug: the-media
.. date: 2022-09-15 23:29:28 UTC+03:00
.. tags: Russia,Ucraina,Union Europee,actualitate,information,politica,reflexiones,societate,english
.. category: 
.. link: 
.. description: 
.. type: text
-->

Ah, the media. No words from me are necessary here: just read these two
articles, then read their titles again, then wonder.

- [news.mc/2022/09/12/ukrainian-refugees-attacked-by-russian/](https://news.mc/2022/09/12/ukrainian-refugees-attacked-by-russian/)
- [news.mc/2022/09/15/assailant-appears-in-court-charged-with-assault-on-ukrainian-refugees/](https://news.mc/2022/09/15/assailant-appears-in-court-charged-with-assault-on-ukrainian-refugees/)

Just wow.
