<!--
.. title: libSDL2 and VVVVVV for the Wii
.. slug: libsdl2-and-vvvvvv-for-the-wii
.. date: 2024-02-02 20:50:44 UTC+03:00
.. tags: Linux, Wii, curiositates, english, informatica, planetmaemo, programmation
.. category: 
.. link: 
.. description: 
.. type: text
-->

Just a quick update on something that I've been working on in my free time.

I recently refurbished my old Nintendo Wii, and for some reason I cannot yet
explain (not even to myself) I got into homebrew programming and decided to
port libSDL (the 2.x version -- a 1.x port already existed) to it. The result
of this work is already available via the [devkitPro](https://devkitpro.org/)
distribution, and although I'm sure there are still many bugs, it's overall
quite usable.

In order to prove it, I ported the game [VVVVVV](https://thelettervsixtim.es/)
to the Wii:

<iframe src="https://vk.com/video_ext.php?oid=7200355&id=456239302&hd=1"
width="640" height="360" allow="autoplay; encrypted-media; fullscreen;
picture-in-picture;" frameborder="0" allowfullscreen></iframe>

During the process I had to fix quite a few bugs in my libSDL port and in a
couple of other libraries used by VVVVVV, which will hopefully will make it
easier to port more games. There's still an issue that bothers me, where the
screen resolution seems to be not totally supported by my TV (or is it the HDMI
adaptor's fault?), resulting in a few pixels being cut at the top and at the
bottom of the screen. But unless you are a perfectionist, it's a minor issue.

In case you have a Wii to spare, or wouldn't mind playing on the Dolphin
emulator, [here's the link to the VVVVVV
release](https://github.com/mardy/VVVVVV/releases/tag/v2.4.1_wii1). Have fun! :-)
