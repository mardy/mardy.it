<!--
.. title: Bon anno!
.. slug: bon-anno
.. date: 2007-01-01 00:00:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
-->

Un augurio de bon anno nove a tote mi amicos e mi inimicos!
Il es un pena que io non pote portar mi augurios personalmente a totes; ma del altere latere, isto pote esser considerate un fortuna, perque io ha un certe antipathia pro le festivitate del primo del anno -- non demanda me de explicar lo, io non sape: io crede que il es un sentimento innate. :-)


P.S. Le anno initia con +4 grados, lo que pare esser un bon signo.