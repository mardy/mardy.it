<!--
.. title: Glaciation
.. slug: glaciation
.. date: 2007-01-28 00:00:00
.. tags: turismo,Helsinki,natura
.. category: turismo
.. link: 
.. description: 
.. type: text
-->

<div style="float:right"><a href="http://www.mardy.it/ia/node/340"><img src="http://www.mardy.it/archivos/active/1/340_small.jpg" width=300px"></a></div>
Post alicun dies de temperaturas human (~0 grados), il ha habite un altere abassamento e nos ha retornate a -10. Altere nive ha cadite sur le citate e in mi opinion isto es un bon cosa, perque io ama camminar sur le nive, si illo non es troppo sordide.
Io continua mi activitate de patinage sur glacie, hodie io lo ha practicate pro le sexte vice e io illo me place sempre plus, benque io admitte mi total incapacitate. In le photo on vide le pista de patinage ubi io va usualmente (illo es in le centro del citate, justo a latere del station ferroviari), e in fronte a illo un povre tigre que esseva surprendite per le glaciation durante que illo vadeva a chassar salmones. Infortunatemente le photo esseva realisate per mi telephono, ergo le qualitate es assatis scadente (si, io lo sape, que mesmo photos prendite con le photocamera es assatis scadente, si le photographo es io!).

Un actualisation super <a href="http://www.nseries.com/n800">mi filio N800</a>: il ha plure sitos internet que parla de illo, e le commentos (etiam per parte del usatores) es generalmente enthusiastic. Uao! :-)