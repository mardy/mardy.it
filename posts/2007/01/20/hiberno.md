<!--
.. title: Hiberno?
.. slug: hiberno
.. date: 2007-01-20 00:00:00
.. tags: Finlandia,natura
.. category: Finlandia
.. link: 
.. description: 
.. type: text
-->

<div style="float:right"><img src="http://www.mardy.it/archivos/imagines/hav_suomi_2007012017_1801.gif"></div>
Forsan le hiberno ha finalmente arrivate! Post circa duo menses, le temperatura ha retornate a -7°, assi como illo esseva in novembre, pro alicun dies. Le nive del dies passate ha completemente disapparite, e con iste temperatura io non sape si nos va haber lo &mdash; in omne caso, hodie il es un die de sol (al minus del 9:03 al 16:01; quasi septe horas!).
Le aspecto positive es que le frigido es un bon stimulo pro practicar <i>ice-skating</i>, i.e. patinage sur glacie: io ha recentemente comprate un copula de patinos e intende continuar a ridiculisar me in le pista. Usque nunc io non ha ancora cadite un sol vice, ma isto es probabilemente causate per le facto que io es multo prudente (lege: lente).

Fabio e Mirco es decise a venir a visitar me in Helsinki pro alicun dies in le medio de februario &mdash; ex mi amicos finlandese, illo es le periodo le plus frigide del hiberno: esque illes supervivera? O io habera duo homines de nive plus, in mi jardin...


Anteprima pro informaticos: le software al qual io labora essera probabilemente publicate como Open Source &mdash; stay tuned!