<!--
.. title: Mi filio ha nascite!
.. slug: mi-filio-ha-nascite
.. date: 2007-01-09 00:00:00
.. tags: informatica,labor
.. category: informatica
.. link: 
.. description: 
.. type: text
-->

Finalmente! Heri, le 8 januario 2007, mi filio ha nascite! Le doctor <a href="http://www.nokia.com/A4126339">Olli-Pekka Kallasvuo</a> annunciava heri durante le CES in Las Vegas le nascita del <a href="http://www.nseries.com/n800">N800</a>, le nove <i>internet tablet</i> que porta in se un parte de me &mdash; quatro menses de programmation!
Pro celebrar le evento, heri on ha habite un party in le officio, e totes ha recepite in dono un exemplar del N800: finalmente etiam io ha un filio a crescer!
Le distribution del N800 ha jam initiate, al minus in le Statos Unite, e forsan etiam in Europa. Ma qui se cura? &mdash; io ham lo ha! :-)

Jam desde alicun dies ante le annunciation official, alicun blogs e sitos de informatica reportava alicun rumores circa le N800, obtenite per fontes non official. Le cosa le plus amusante pro me es leger le commentos del visitatores de iste sitos: illes non cognosce le <i>device</i>, ma illes pote scriber paginas e paginas de suppositiones e recensiones "a distantia". :-)

Alicun ligamines:
<ul>
<li>Sito official: <a href="http://www.nseries.com/n800">http://www.nseries.com/n800</a>
<li>Le annuncio in <a href="http://punto-informatico.it/p.aspx?id=1839207&r=Telefonia">Punto Informatico</a>
<li>Un <a href="http://hardwaregadget.blogosfere.it/2007/01/ces-2007-presentato-il-nokia-n800-linternet-tablet-successore-del-770.html">recension in un blog</a>
</ul>