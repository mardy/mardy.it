<!--
.. title: Happy new year!
.. slug: happy-new-year
.. date: 2007-01-04 00:00:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
-->

I wish a happy new year to all my friends and my enemies!
It's a pity, not being able to personally bring my wishes to everybody; but on the other hand, this can be considered a luck, since I've always had a certain dislike for the new year celebrations -- don't ask me to explain this, I don't know: I think it's an innate feeling. :-)

P.S. This year starts with +4 degrees, which appears to be a good sign.