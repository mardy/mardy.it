<!--
.. title: Bicycletta
.. slug: bicycletta
.. date: 2007-09-04 00:00:00
.. tags: Helsinki
.. category: Helsinki
.. link: 
.. description: 
.. type: text
-->

Profitante del discontos de fin estate, io ha comprate un bicycletta. Io non sape si io habera le occasion de usar lo ante le primavera del proxime anno, ma isto es un aspecto secundari. Intertanto, io lo ha usate solmente un vice, le sabbato passate, quando in le matino il habeva sol e un temperatura relativemente calide. Io non saperea dicer qual percurso io ha seguite, io curreva per Lauttasaari sin un destination, e de facto per plure tempore io non habeva ulle idea de ubi io esseva. Ma ha retornate a casa con successo.
<a href="http://www.mardy.it/archivos/active/1/1062_large.jpg"><img src="http://www.mardy.it/archivos/active/1/1062_small.jpg" width="400px" style="margin-left:auto;margin-right:auto;display:block;padding:0.5em"></a>