<!--
.. title: Un information sportive
.. slug: un-information-sportive
.. date: 2007-11-19 00:00:00
.. tags: amicos,Helsinki,curiositates
.. category: amicos
.. link: 
.. description: 
.. type: text
-->

Un bon parte del italianos qui io cognosce a Helsinki es amicos incontrate in partitas de <i>calcetto</i> (io non cognosce le vocabulo anglese, ma io intende le partitas de football que on joca con 5-6 jocatores pro equipa &mdash; mini football?); quasi omne sabbato postmeridie nos va jocar un partita. Purmente amical, sin agonismo e tension. Io es un pexime jocator, ma nonobstante isto io ama iste sport e io cerca de non mancar ulle incontro.

Durante le hiberno nos joca in un sala coperite, pro evitar le risco de congelamento. ;-) Le sala es optime pro nos, e offere duchas e sauna. Toto ha sempre essite perfecte.

Le ultime sabbato nos debeva interrumper le incontro pro alicun minutos, durante le quales tote nos teneva lor oculos a reguardar le tecto del sala:
<img src="http://www.mardy.it/archivos/active/0/1073_small.jpg" style="padding: 0.5em; margin-left: auto; margin-right: auto; display: block;" width="300">
Le palla esseva bloccate inter le cavos que rege le lampas!
<center><div style="margin-left: auto; margin-right: auto;display:block;"><img src="http://www.mardy.it/archivos/active/0/1072_small.jpg" style="padding: 0.5em; vertical-align:top" width="300"><img src="http://www.mardy.it/archivos/imagines/detalio_palla.jpg" style="padding: 0.5em; vertical-align:top"></div></center>
<p>Pro le cronaca, nos continuava le partita con un altere palla, perque nos non succedeva in recuperar le nostre.