<!--
.. title: Bandieras a medie palo
.. slug: bandieras-a-medie-palo
.. date: 2007-11-08 00:00:00
.. tags: reflexiones,Finlandia,actualitate
.. category: reflexiones
.. link: 
.. description: 
.. type: text
-->

<div style="float:right"><a href="http://www.mardy.it/archivos/imagines/bandiera.jpg"><img src="http://www.mardy.it/archivos/imagines/bandiera.jpg" width="300px"></a></div>
Io suppone que multe de vos sape le motivation, perque le television italian ha certemente date ample resonantia a <a href="http://www.google.com/news?ncl=1103673274&hl=it&topic=w">iste nova</a> (probabilemente pro evitar de parlar de embarassante themas politic); ma pro le non-italianos, o pro quicunque non ha audite super le tragic facto de chronica que accideva heri in Finlandia, io da un synthese extreme. Un juvene de 18 annos ha entrate in <a href="http://www.mardy.it/archivos/imagines/Jokelan%20Lukio.png">le schola superior de Jokela</a> e ha sparate a plure persona con un pistola: le numero del victimas es 9, includente ille mesme que se suicidava per sparar se al capite.
Le evenimento, que es stupefacente per se, es rendite ancora plus <i>interessante</i> per le facto que le author del excidio habeva in alicun maniera <i>annunciate</i> in internet lo que ille vadeva a facer. Ille publicava un video in YouTube e explicava in un sito su &ldquo;rationes&rdquo;; io non crede que illos merita de esser scribite o debattite, tanto illos es insensate. E io non crede que il ha responsabilitates per parte de YouTube o del altere sito: il es practicamente impossibile, specialmente pro un gestor de sitos web de tal grandessa, monitorar tote le contento que le utentes publica; e in omne caso, qui haberea considerate seriemente le cosa?

Le motivation pro le qual io scribe de iste facto non es pro <em>informar</em>, ni pro <em>commemorar</em> le victimas (benque io es dolente pro illes e lor familiares); il ha jam troppo de isto in internet, sia como informationes detaliate super le facto, sia como activitates de commemoration, tanto natural quanto plus o minus inutile (pardona mi franchessa, e non vole confunder lo pro cynismo). Lo que me interessa es le <em>reactiones</em>  practic, propositive, active. Le Finlandia jam desde plure annos es un distributor de empleo pro psychologos del estraniero; un <a href="www.helsinki.fi/psykologia/esittely/Annual_Report_2001.pdf">un reporto del departimento de psychologia del Universitate de Helsinki</a> del 2001 dice que quasi le totalitate del doctores in psychologia es empleate, e psychologos es demandate in plure sectores. <a href="http://www.psyli.fi/cgi-bin/linnea.pl?document=english">Un statistica plus recente</a>, del 2007, per le Association psychologic finnese, dice que solmente 3-4% del psychologos es sin empleo, typicamente illes qui ha justo graduate e que ancora non ha un labor permanente; le resto del empleatos es assi distribuite:
<ul>
<li>Municipalitates 55%
<li>Stato 18%
<li>Empleo private 18%
<li>Practicantes private e interprenditores 9%
</ul>
E le statistica continua con le salario: le psychologos empleate per le stato recipe mediamente 2800 euro mensil, e les qui labora privatemente 3600 euro (grosse, on debe subtraher le taxas). <small>Io non osa cercar statisticas simile pro le psychologos italian.</small>
Evidentemente alicun cosa non ha functionate, e continua a non functionar, si le Finlandia es ancora le pais europee con le plus alte percentual de suicidios. Io non sape qual es le presentia del psychologos in le scholas finnese; certemente illo pote adjutar enormemente in evitar tragedias simile o altere situationes de difficultate. Ma post le schola, il es plus difficile diagnosticar tal situationes, spacialmente perque le character generalmente pauc social del finneses non adjuta multo (e particularmente in le areas foras del grande citates).
Forsan si le psychologos italian apprendeva le finnese como parte de lor cursos universitari, on poterea solver duo problemas contemporaneemente...