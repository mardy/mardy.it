<!--
.. title: Patre, pro le secunde vice
.. slug: patre-pro-le-secunde-vice
.. date: 2007-10-18 00:00:00
.. tags: informatica,labor
.. category: informatica
.. link: 
.. description: 
.. type: text
-->

Le N800 habera tosto un fratre! Le N810 ha essite <a href="http://press.nokia.com/PR/200710/1160660_5.html">officialmente annunciate</a> heri, e immediatemente photos e characteristicas technic ha apparite in plure sitos; pro le italianos, io signala <a href="http://www.technews.it/nokia-annuncia-il-nuovo-n810">technews.it</a>. Vide lo pro photos e recension!

De facto, mi secunde filio non ha ancora nascite: illo non es ancora disponibile in le botecas, ma on non debera attender multo. Optimisticamente, illo essera commercialisate ante Natal, assi que illo potera esser regalate al amantes del technologias. Le precio non es definite; il pare que le version american costara 479 dollaros, equivalente a circa 340 euro, ma il ha nulle information official circa le precio in Europa.

Le novitates plus importante de iste modello es, a mi aviso, le presentia de un claviero (it: <i>tastiera</i>, en: <i>keyboard</i>) e le navigator GPS.