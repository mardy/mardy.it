<!--
.. title: Incertessas atmospheric
.. slug: incertessas-atmospheric
.. date: 2007-10-14 00:00:00
.. tags: Helsinki,natura
.. category: Helsinki
.. link: 
.. description: 
.. type: text
-->

Compara iste duo photos:
<div style="margin-left: auto; margin-right: auto; display: block;"><a href="http://www.mardy.it/archivos/imagines/neve.jpg"><img src="http://www.mardy.it/archivos/imagines/neve.jpg" style="padding: 0.5em;" width="400"></a><a href="http://www.mardy.it/archivos/imagines/sol.jpg"><img src="http://www.mardy.it/archivos/imagines/sol.jpg" style="padding: 0.5em;" width="300"></a></div>
Eh, si: le prime nive jam ha arrivate! De facto, illo esseva miscite con pluvia, assi que illo non lassava alicun tracia, ma subito se disfaceva. Le altere photo es del die sequente, que esseva totalmente sin nubes. Le temperatura nunc es de -1 grados, ma le previsiones pro le septimana veniente annuncia un altiamento del temperatura, con un media de 10 grados. Pro le ver nive on debera attender ancora!