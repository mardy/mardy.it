<!--
.. title: Ancora ciambella
.. slug: ancora-ciambella
.. date: 2007-10-13 00:00:00
.. tags: cocina
.. category: cocina
.. link: 
.. description: 
.. type: text
-->

Dominica passate, in un momento de follia, io ha cocinate le quarte ciambella. Quando mi parentes veniva a visitar me in augusto, illes me portava un stampo pro ciambellas, dono de mi amita; totevia io ancora non ha osate usar lo, perque io non esseva secur que illo es intendite pro le uso in alte temperaturas (illo es de un material plastic, apparentemente). Le resultato es que le ciambella ha sufferite un grande trauma quando io lo ha removite del olla <i>(italiano: pentola)</i>, e se ha rumpite in alicun punctos.
Ma ante que isto accideva, io prendeva un photo:
<a href="http://www.mardy.it/archivos/imagines/ciambella4.jpg"><img src="http://www.mardy.it/archivos/imagines/ciambella4.jpg" style="padding: 0.5em; margin-left: auto; margin-right: auto; display: block;" width="500"></a>
Io debe absolutemente consumer le tres ovos que me resta, ante que illos se guasta; assi il non es improbabile que deman io prepara un altere ciambella &mdash; iste vices io probara a cocinar lo in le stampo plastic, sperante que le plastica non deveni parte integrante del dulce. :-)