<!--
.. title: Orientamento cerebral
.. slug: orientamento-cerebral
.. date: 2007-10-10 00:00:00
.. tags: curiositates
.. category: curiositates
.. link: 
.. description: 
.. type: text
-->

Zeeshan me ha inviate <a href="http://www.news.com.au/perthnow/story/0,21598,22492511-5005375,00.html">iste ligamine</a> a un articulo que presenta un test pro verificar si tu usa majormente le parte sinistre o dextere de tu cerebro. Vide iste animation:
<img src="http://www.mardy.it/archivos/imagines/dansatrice.gif" style="padding: 0.5em; margin-left: auto; margin-right: auto; display: block;">
Esque le dansatrice rotea in senso orari o antiorari? Le articulo sustene que si vos vide le dansatrice rotear in senso orari, vos usa majormente le parte dextere del cerebro, e vos usa le parte sinistre si vos lo vide dansar in senso antiorari. Il es possibile vider le dansatrice rotear in ambe sensos, como le test ipse affirma.

Personas qui usa majormente le parte dextere de lor cerebro ha un personalitate tendentialmente sentimental, durante que le sinistres es rational.

Io non crede que iste test ha ulle validitate, perque le maxime parte de mi collegas (includente io) videva le dansatrice rotear in senso orari (lo que significarea que nos es sentimental, e non rational!). De facto, io crede que isto es solmente un joco optic, totevia il es bastante amusante vider como personas vide le mesme cosa e lo interpreta differentemente. Le ambiguitate es debite al facto que le animation es bidimensional, e quando nostre cerebro lo elabora pro construer un imagine tridimensional, illo adde un dimension &mdash; ma le direction de illo es absolutemente libere, e lassate al interpretation.