<!--
.. title: Porvoo
.. slug: porvoo
.. date: 2007-04-29 00:00:00
.. tags: turismo,Finlandia
.. category: turismo
.. link: 
.. description: 
.. type: text
-->

Le viages continua... <a href="http://www.porvoo.fi">Porvoo</a> es un placente citate maritime locate a 50 kilometros a est de Helsinki. Si, forsan io debeva omitter de scriber le distantia, assi que on continuava a creder que isto esseva un ver viage, e non un simple promenada. Ma io non utilisa le kilometros como unitate de mensura de mi dies.
Le occasion pro visitar Porvoo esseva le visita de Francesco e Michela, que ha essite in Helsinki del 24 al 29 april, in le melior hotel del citate (mi casa). Le idea initial esseva prender un automobile e currer verso Savonlinna o Kuopio, ma con nostre grande surprisa pro rentar le auto il esseva requirite pagar per carta de credito, que nos non ha. On non mesmo acceptava le pagamento in moneta o carta bancari, solmente carta de credito. Nulle problema: le plano B ha functionate multo ben! Nos ha prendite un autobus, multo plus economic (e secur, he he!) e in pauc tempore ha arrivate in iste belle citate.
Io esseva pigre e non portava mi photocamera; tote le photos ha essite prendite per le camera de mi amicos, e Francesco es le autor de multe de illes (gratias!) &mdash; nunc io considera seriemente le acquisition de un photocamera simile (un modello de Canon IXUS), sia pro le parve dimensiones, sia pro le bon qualitate.
Io quasi oblidava dicer que <a href="http://www.mardy.it/ia/node/428">le photos es hic</a>!
(le ecclesia de Porvoo es absente del photos, perque illo es in phase de restauro, post que un incendio ha destruite le copertura)