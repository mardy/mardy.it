<!--
.. title: Stockholm
.. slug: stockholm
.. date: 2007-04-15 00:00:00
.. tags: turismo
.. category: turismo
.. link: 
.. description: 
.. type: text
-->

<div style="float:right"><a href="http://www.mardy.it/archivos/active/1/393_large.jpg"><img src="http://www.mardy.it/archivos/active/1/393_small.jpg" width="300px"></a></div>
Io ha retornate del cruciera que <a href="http://www.jollydragon.net/eventscalendar">JollyDragon</a> ha organisate a Stockholm iste fin de septimana. Le cruciera esseva amusante e folle, como il esseva prevedibile, e me ha lassate con plure horas de reposo a recuperar &mdash; cosa que io va facer nunc (e possibilemente lunedi, al labor ;-) ).
Un gratias special a Hanna qui ha prendite le initiativa de organisar le evento!

Photos es <a href="http://www.mardy.it/en/node/378">hic</a> &mdash; horribile como sempre, ma isto face parte de mi stilo.