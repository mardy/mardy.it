<!--
.. title: Un saluto del sol levante
.. slug: un-saluto-del-sol-levante
.. date: 2007-07-26 00:00:00
.. tags: turismo,Japon
.. category: turismo
.. link: 
.. description: 
.. type: text
-->

<div style="float:right"><a href="http://www.mardy.it/ia/node/520"><img src="http://www.mardy.it/archivos/active/0/797_small.jpg" width="300px"></a></div>
In le ultime menses io ha essite terribilemente occupate con le renovation del appartamento; dicer que io ha removite le carta del muros e los ha repicturate non rende le idea del labor que io, con le adjuta de plure amicos, ha complite. Ma io va narrar super isto un altere vice.
Nunc io es in Japon, hospite in le casa de mi fratre e Loco, in le proximitate de Tokyo. Io es impressionate per le modernitate del casa e per le cura e le detalios con que illo ha essite construite; le comparation con mi appartamento esserea tanto impare e humiliante que io prefere non pensar lo!
Le photos que vos pote vider <a href="http://www.mardy.it/ia/node/520">in le galleria</a> ha essite prendite in le village (difficile definir si illo es un village, un citate, o simplemente un continuo de casas) ubi nos habita, e in Asakusa, un citate con un area turistic plen de templos. Forsan in le dies veniente io habera tempore pro describer brevemente tote le photos; pro le momento, illos es sin description (ma io es secur que vos sapera distinguer un templo de un casa).