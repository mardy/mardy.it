<!--
.. title: Pitagora... que??
.. slug: pitagora-que
.. date: 2007-03-31 00:00:00
.. tags: curiositates
.. category: curiositates
.. link: 
.. description: 
.. type: text
-->

<div style="float:right;clear:both"><embed style="width:400px; height:326px;" id="VideoPlayback" type="application/x-shockwave-flash" src="http://video.google.com/googleplayer.swf?docId=3163263343187879320&hl=it" flashvars=""> </embed></div>
<a href="http://www.youtube.com">YouTube</a> e <a href="http://video.google.com">Google Video</a> es un ver miniera de "guastatempore": quando on ha alicun minutos a perder, on pote navigar in le enorme archivo de videos e trovar un modo pro passar le tempore que non require quasi ulle activitate cerebral. Un sorta de television, con le importante differentia que con iste systemas on ha un major controlo super lo que on vole vider.
Ma non sempre. In un del frequente momentos de <i>stand-by</i> de mi meninges, io ha cadite sur iste video japonese, que vole condivider con vos, in le sperantia que mi car fratre Andrea pote dar nos alicun explicationes super le motivationes que porta le Japoneses a complir iste cosas folle.