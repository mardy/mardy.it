<!--
.. title: Vamos OpenSource!
.. slug: vamos-opensource
.. date: 2007-03-18 00:00:00
.. tags: Helsinki,informatica,labor
.. category: Helsinki
.. link: 
.. description: 
.. type: text
-->

Le 9 de martio le management de Nokia ha approbate le publication de <a href="http://mission-control.sourceforge.net">mission-control</a> como <a href="http://ia.wikipedia.org/wiki/Software_libere">software libere</a> sub licentia <a href="http://www.gnu.org/licenses/lgpl.html">LGPL</a>, e <a href="http://naba.co.in">Naba</a> ha immediatemente initiate le operationes pro le distribution de mission-control in le sito de <a href="http://sourceforge.net">SourceForge</a>. Le passo successive ha essite <a href="http://lists.freedesktop.org/archives/telepathy/2007-March/000471.html">annunciar le nova</a> al personas qui pote esser interessate.

Io scribe iste articulo super <a href="http://mission-control.sourceforge.net">MissionControl</a> perque io etiam ha contribuite a su realisation, e lo mantene activemente. Il es difficile explicar al non-informaticos (e a vices etiam al informaticos!) qual es le functionalitate de mission-control, perque illo non es un programma autonome, ma un componente del architectura <a href="http://telepathy.freedesktop.org">Telepathy</a>. Pro dar un information partial, incorrecte ma suggestive, io va dicer que illo es le "coordinator" del canales que le programmas de communication (chat textual, o con audio e video) necessita pro le transferimento del datos. Mmmm... no, vos non debe comprender; io lo ha scribite solmente pro dar le impression que le paragrapho es complete. :-)

Le programmatores qui labora con Telepathy ha immediatamente initiate a utilisar mission-control, lo que me rende felice (e occupate a solver le errores que illes me signala!).
<hr class="septhema">
<div style="float:right;padding:1px">
<a href="http://www.mardy.it/archivos/active/1/370_large.jpg"><img src="http://www.mardy.it/archivos/active/1/370_small.jpg" width="150px"></a></div>
<div style="float:right;padding:1px 10px"><a href="http://www.mardy.it/archivos/active/1/371_large.jpg"><img src="http://www.mardy.it/archivos/active/1/371_small.jpg" width="150px"></a>
</div>
Cambio de thema: ante alicun dies io scribeva que le hiberno ha finite... ma heri e hodie le temperatura ha retornate proxime al 0 grados e durante que io scribe iste lineas le neve cade intensemente a uniformar le colores del jardin. Le photos ha essite prendite a distantia de 40 minutas &mdash; si, io sape que pro un nordico isto es perfectemente normal, ma pro me le nive es ancora le joco del infante que io porta intra me (e etiam foras, a vices ;-) ).