<!--
.. title: Retorno a Italia
.. slug: retorno-a-italia
.. date: 2007-03-10 00:00:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
-->

Mi sequente visita a Italia es fixate: del 5 al 10 de april. De facto le 10 non es un die a considerar, perque io partira multo tosto in le matino (forsan "nocte" es plus appropriate): le avion parti al horas 6:40 de Venetia, lo que significa que io debera esser in le aeroporto a 5:00 horas, e probabilemente eveliar me ante 4:00 horas. Le mission me pare impossibile, ma io es fiduciose que in alicun maniera io essera expellite del Italia con successo.

Mi collega e amico <a href="http://andreambro.blogspot.com">Andrea</a> me ha informate que il es conveniente demandar le carta Miles & More de <a href="http://www.lufthansa.com">Lufthansa</a>: iste carta permitte de ganiar viages gratuite quando on supera un certe quantitate de kilometros &mdash; io time que io nunquam attingera le numero necessari, ma post que le carta es gratuite on ha apparentemente nihil a perder in requestar lo. Vamos probar...