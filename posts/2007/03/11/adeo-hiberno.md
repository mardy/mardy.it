<!--
.. title: Adeo, hiberno!
.. slug: adeo-hiberno
.. date: 2007-03-11 00:00:00
.. tags: Helsinki,natura
.. category: Helsinki
.. link: 
.. description: 
.. type: text
-->

<div style="float: right"><img src="http://www.mardy.it/archivos/imagines/Helsinki20070310.gif"></div>Iste imagine, prendite del sito del Instituto Metereologic de Helsinki, testimonia que le hiberno es concluse. Iste matino il cadeva un pauco de nive, ma subito dispareva sin lassar tracias; il resta ancora multe nive del menses passate, que se ha transformate in glacie e que nunc se funde lentamente in aqua sordide.
Le dies ha devenite multo plus luminose, e finalmente le 21 de martio on habera un democratia solar, que assignara le mesme quantitate de sol a tote le partes del Terra.
Mi prime hiberno in Finlandia ha passate sin multe difficultates; le obscuritate de decembre non me ha deprimite usque al suicidio, e le -24 grados non me ha glaciate comletemente (ma forsan illos ha rendite mi cerebro un pauco plus lente?). Io ha experientiate le patinage sur glacio, le immersion in le aqua frigide post un sauna, un pauco de descensas con slittas; il manca le ski, que on pote re-schedular pro le anno veniente, ma io non es specialmente interessate in illo. Io haberea volite facer plus practica de patinage sur glacie, que io ama multo ben que io es totalmente inhabile in illo. Ma vamos parlar de isto in le sequente hiberno!