<!--
.. title: Augurios de bon Natal
.. slug: augurios-de-bon-natal
.. date: 2007-12-14 00:00:00
.. tags: Helsinki,curiositates
.. category: Helsinki
.. link: 
.. description: 
.. type: text
-->

Iste matino io ha recepite un carta postal de augurios de bon Natal, totalmente inexpectate.
<img src="http://www.mardy.it/archivos/imagines/cartolina1.jpg" style="padding: 0.5em; margin-left: auto; margin-right: auto; display: block;" width="400">
Gratias! Ma... qui es le author?
<img src="http://www.mardy.it/archivos/imagines/cartolina2.jpg" style="padding: 0.5em; margin-left: auto; margin-right: auto; display: block;" width="600">
Oh! Illes es mi amicos de <a href="http://www.k-rauta.com">K-RAUTA</a>, un negotio de equipamentos pro le casa, que io ha sovente visitate durante iste estate, quando io renovava mi appartamento. Io lo visitava tanto frequentemente, que mi amicos burlava in dicer que tote le personal de K-rauta me cognosceva personalmente. Mmmm... que dicera illes post saper de iste carta postal?