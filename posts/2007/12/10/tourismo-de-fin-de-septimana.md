<!--
.. title: Tourismo de fin de septimana
.. slug: tourismo-de-fin-de-septimana
.. date: 2007-12-10 00:00:00
.. tags: turismo,varsavia
.. category: turismo
.. link: 
.. description: 
.. type: text
-->

<div style="float:right"><a href="http://www.mardy.it/en/node/1083"><img src="http://www.mardy.it/archivos/active/0/1114_small.jpg" width="300px"></a></div>
Io ha retornate hodie de un breve viage in Polonia, plus precisemente a Varsavia. Le amicos polonese qui io cognosce in Helsinki me habeva alertate que Varsavia es un del citates minus actractive del Polonia, ma io ha essite multo satisfacite de lo que ha vidite. Le tempore non esseva perfecte, il habeva sol solmente sabbato, durante que in le altere dies le ciel esseva coperite per nubes non troppo aggressive.

Io ha versate un mar de photos in le <a href="http://www.mardy.it/en/node/1083">galleria</a>, pro vostre uso e consumo.