<!--
.. title: Stock plugin, version 0.2
.. slug: stock-plugin-version-0-2
.. date: 2007-12-20 00:00:00
.. tags: maemo
.. category: maemo
.. link: 
.. description: 
.. type: text
-->

Heri vespere io ha publicate le version 0.2 de un plugin que io ha developpate pro le <a href="http://tableteer.nokia.com">OS2008</a> (que es le systema operative del <a href="http://www.nokia.com/n800">N800</a>/<a href="http://www.nokia.com/n810">N810</a>). Le plugin es un <i>home desktop applet</i> que monstra le datos financiari del titulos que le usator ha seligite:
<img src="http://www.mardy.it/archivos/imagines/stock-plugin.png" style="padding: 0.5em; margin-left: auto; margin-right: auto; display: block;">
Le datos es prendite de Yahoo!Finance. Le <i>applet</i> es ancora in un stato non optimal, specialmente perque:
<ul>
<li>Le graphica es horribile (il manca le effecto de transparentia)
<li>Si il ha multe titulos a visualisar, il ha un belle effecto de rolamento; ma isto require un certe labor per le CPU, ergo pro non exhaurir le batteria on deberea stoppar lo quando le <i>applet</i> non es visibile (per exemplo quando le schermo es obscur)
<li>io es secur que il ha multe cosas a corriger
</ul>
Nonobstante isto, le <i>applet</i> es multo stabile e io ha ponite multe effortios in verificar que illo non perde memoria (in gergo informatic, que illo non produce <i>memory leaks</i>); io es satisfacite de isto, perque ora le <i>applet</i> pote functionar per plure dies sin exhibir problemas.

Le projecto es registrate in le <a href="http://garage.maemo.org"><i>garage</i> de Maemo</a>, e plus precisemente al pagina <a href="https://garage.maemo.org/projects/stock-plugin/">https://garage.maemo.org/projects/stock-plugin/</a>.