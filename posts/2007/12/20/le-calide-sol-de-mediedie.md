<!--
.. title: Le calide sol de mediedie
.. slug: le-calide-sol-de-mediedie
.. date: 2007-12-20 00:00:00
.. tags: Helsinki,curiositates
.. category: Helsinki
.. link: 
.. description: 
.. type: text
-->

Dominica io me trovava in le placia del mercato, <i>kauppatori</i> (del qual on pote vider <a href="http://www.virtualhelsinki.net/helsinkipanoraama/kauppatori.html">un animation panoramica a 360 grados</a>), e post que il esseva un bellissime die de sol, io prendeva un photo pro capturar le sensation.
<a href="http://www.mardy.it/archivos/active/0/1154_large.jpg"><img src="http://www.mardy.it/archivos/active/0/1154_small.jpg" style="padding: 0.5em; margin-left: auto; margin-right: auto; display: block;"></a>
Lo que forsan non es evidente, es que le sol esseva quasi perfectemente a sud: le hora esseva 13:46.