<!--
.. title: Mardy United gania le Tropheo Ambrosioni
.. slug: mardy-united-gania-le-tropheo-ambrosioni
.. date: 2007-12-02 00:00:00
.. tags: amicos,Helsinki
.. category: amicos
.. link: 
.. description: 
.. type: text
-->

<div style="float:right"><a href="http://www.mardy.it/archivos/imagines/trofeo_ambrosioni.jpg"><img src="http://www.mardy.it/archivos/active/0/1079_small.jpg" width="300px"></a></div>
Le 2007 essera recordate, inter le altere cosas, pro le establimento de un importante competition sportive, le Tropheo Ambrosioni. Organisate pro honorar le homonyme amico (e collega, e companio de football) qui va abandonar le frigide terras de Finlandia pro retornar al calide colles roman, iste torneo de football essera disputate cata anno inter le <em>melior</em> jocatores de football qui cata septimana se incontra pro un match amicabile.

Iste anno on habeva 20 jocatores dividite in 4 agguerritissime equipas cuje membros esseva extracte a sorte, ma de un maniera que le fortia del equipas esseva multo balanciate. Le equipa in le qual io ha jocate (consistende de duo italianos, duo finlandeses e un brasiliano) esseva nominate &ldquo;Mardy United&rdquo;, obviemente non per me. Le torneo consisteva de duo semifinales, final e final pro le tertie e quarte posto. Sin surprisa, mi equipa vinceva le torneo e portava a casa le grandiose &ldquo;1° Trofeo Ambrosioni&rdquo;, e cata uno de nos esseva premiate con un medalia representante duo jocatores de football (<a href="http://www.mardy.it/en/node/1075">vide le photos in le galleria</a>). Le altere equipas esseva premiate con del ballas de gumma e con del barras de chocolate.

In le photo de gruppo on pote observar que io vestiva un T-shirt con le logo de <a href="http://www.maemo.org">maemo</a>.