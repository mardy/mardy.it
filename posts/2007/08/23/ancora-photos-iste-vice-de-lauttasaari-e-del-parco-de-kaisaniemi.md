<!--
.. title: Ancora photos, iste vice de Lauttasaari e del parco de Kaisaniemi
.. slug: ancora-photos-iste-vice-de-lauttasaari-e-del-parco-de-kaisaniemi
.. date: 2007-08-23 00:00:00
.. tags: turismo,Helsinki,natura
.. category: turismo
.. link: 
.. description: 
.. type: text
-->

<div style="float:right"><a href="http://www.mardy.it/archivos/active/1/1054_large.jpg"><img src="http://www.mardy.it/archivos/active/1/1054_small.jpg" width="300px"></a></div>
Io profita del belle tempore estive pro prender quante plus photos possibile; io mesme non volerea poner troppo de photos in le sito, ma post que io los ha... perque non?
Io promitte solemnemente que in futuro io va probar a corriger iste situation. Io promitteva que io haberea titulate tote le photos del Japon, e io ancora non lo ha facite; eh... io attende, in le sperantia que vos oblida. ;-)

In le galleria io ha ponite <a href="http://www.mardy.it/ia/node/1007">photos de Lauttasaari</a>, que es le parve insula in le qual io vive, e del <a href="http://www.mardy.it/ia/node/1057">parco de Kaisaniemi</a>, qui se trova retro al station ferroviari de Helsinki. Io promenava per illo con mi parentes, e post que illes exclamava "bellissime!" pro qualcunque cosa que illes videva, io me sentiva in obligation de photographar lo. Assi, le culpa non es solmente mie.