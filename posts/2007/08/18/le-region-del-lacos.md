<!--
.. title: Le region del lacos
.. slug: le-region-del-lacos
.. date: 2007-08-18 00:00:00
.. tags: turismo,Finlandia,natura
.. category: turismo
.. link: 
.. description: 
.. type: text
-->

<div style="float:right"><a href="http://www.mardy.it/ia/node/932"><img src="http://www.mardy.it/archivos/active/1/995_small.jpg" width="300px"></a></div>
Le photocameras digital ha multissime avantages super illos mechanic, ma anque un grandissime contro: il es troppo facile premer le button. 174 (centoseptantaquattro) photos in un die non es admissibile! Il non importa qual es le subjecto, ma quando on preme le button del camera on debe esser consciente del facto que al minus un persona (victima) debera reguardar le photo, e probabilemente esser enoiate per illo. E quando le victima coincide con le assassin, le unic cosa que on pote accusar es le arma del delicto. Dicte, facte.
Al fin, io ha delite 49 photos, ma publicar 125 photos de lacos e campania finlandese haberea essite un tortura pro mi appassionate lectores (multo plus que leger mi nonsensos in interlingua). Io ha facite un effortio ulterior, ma non ha succedite a descender sub 70 photos; io non poteva reducer le numero ancora, perque il es essential que vos comprende que in Finlandia il ha <b>multe</b> lacos.

Le occasion ha essite un viage que io ha facite con mi patre e mi matre: nos ha prendite un automobile (in rent) e ha viagiate a nord-est de Helsinki, passante per Lahti, Jämsä, Jyväskylä, Mikkeli e retro al base (vide le region in <a href="http://maps.google.com/maps/mm?ie=UTF8&hl=en&ll=61.259669,27.432861&spn=2.192437,10.283203&z=7&om=1">Google maps</a>).

<a href="http://www.mardy.it/ia/node/932">Clicca hic pro vader al album photographic!</a>