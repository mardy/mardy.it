<!--
.. title: Iride
.. slug: iride
.. date: 2007-08-26 00:00:00
.. tags: Helsinki,natura
.. category: Helsinki
.. link: 
.. description: 
.. type: text
-->

<div style="float:right"><a href="http://www.mardy.it/archivos/active/1/1059_large.jpg"><img src="http://www.mardy.it/archivos/active/1/1059_small.jpg" width="300px"></a></div>
Post un breve pluvia, con tonitros, le sol ha retornate a splender e a fugar le nubes. Il habeva un meraviliose iride, complete, que vadeva da un extremo al altere del horizonte; infortunatemente illo esseva tanto grande que io non ha succedite a photographar lo integremente.
Io ha probate a componer un imagine panoramic, a partir del photos que io ha prendite, con le auxilio del programma  <a href="http://hugin.sourceforge.net/">hugin</a>, un programma pro Linux que servi a iste scopo. Le programma es optime e non difficilissime a utilisar, ma infortunatemente io non ha prendite le photos exactemente del mesme puncto de vista, e in ultra le nubes se moveva multo velocemente, assi que il es probabilemente impossibile obtener un imagine componite coherente.
Alora, contenta vos de un semi-iride... :-)