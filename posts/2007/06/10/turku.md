<!--
.. title: Turku
.. slug: turku
.. date: 2007-06-10 00:00:00
.. tags: amicos,turismo,turku
.. category: amicos
.. link: 
.. description: 
.. type: text
-->

<div style="float:right"><a href="http://www.mardy.it/ia/node/485"><img width="300px" src="http://www.mardy.it/archivos/active/1/516_small.jpg"></a></div>
In occasion del <a href="http://www.maailmakylassa.fi/english"><i>World Village Festival</i></a> que occurreva in Helsinki le 26 e le 27 de majo, io hospitava in mi appartamento alicun voluntarios del <a href="http://ec.europa.eu/youth/program/index_en.html">EVS</a> (<i>European Voluntary Service</i>) qui arrivava de altere citates del Finlandia pro visionar le evento.
Post un septimana, Marion e Ute me invitava pro un barbecue in Lieto, un village proxime a Turku, e io succedeva a convincer Lassi a venir con me (il non ha essite facile). Nos arrivava in Turku sabbato postmeridie, e nos promenava in le proximitate del centro del citate. Mi fotos meraviliose es <a href="http://www.mardy.it/ia/node/485">in le galleria</a>, como usual.