<!--
.. title: Vappu
.. slug: vappu
.. date: 2007-06-02 00:00:00
.. tags: Helsinki,cultura
.. category: Helsinki
.. link: 
.. description: 
.. type: text
-->

<div style="float:right"><a href="http://www.mardy.it/archivos/active/1/481_large.jpg"><img src="http://www.mardy.it/archivos/active/1/481_small.jpg" width="300px"/></a></div>
Con un mense e un die de retardo, io publica alicun photos que io habeva oblidate in mi photocamera &mdash; un cosa aliquanto inusual pro me.
Le photos es prendite le prime de majo, le festa international del laboratores; in Finlandia iste festa es appellate <i>Vappu</i>, parola que probabilemente es un <em>finnisation</em> del nomine de <a href="http://www.newadvent.org/cathen/15526b.htm">Sancte Walburga</a> e es cognoscite in le paises nordic per le 
nomine <a href="http://en.wikipedia.org/wiki/Walpurgis_Night">&ldquo;nocte de Walburga&rdquo;</a>. Iste festa es particularmente significative pro le studentes finlandese, perque illo coincide approximativemente con le termino del anno scholastic (e universitari), e gania un grande participation gratias al facto que illo es le prime grande festivitate del primavera.
Como usual, le festa es un importante occasion pro biber multe alcohol, ma le characteristica que vos probabilemente observara in le photos es le facto que multissime personas porta un bonetto blanc: illo es parte del costume official que le studentes porta quando illes ceremonia lor graduation al schola secundari. Un altere characteristica del festa es que in le multitudine del personas il ha plure gruppos que porta un <em>supertoto</em> (italiano: <i>tuta</i>, anglese: <i>overall</i>) que es distinctive pro le gruppo; in le caso del studentes universitari, il pare que illes se collige in gruppos a secunda del facultate universitari que illes studia.

Reguarda le photos hic: <a href="http://www.mardy.it/ia/node/455">Galleria del Vappu</a> &mdash; illes qui ha vidite Helsinki in hiberno essera surprendite de vider como in primavera il ha si multe personas!