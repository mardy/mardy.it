<!--
.. title: Summer Time Machine Blues - Viages temporal
.. slug: summer-time-machine-blues-viages-temporal
.. date: 2007-05-20 00:00:00
.. tags: cinema
.. category: cinema
.. link: 
.. description: 
.. type: text
-->

Pro le amantes del cinema, io ha un titulo a consiliar: <a href="http://imdb.com/title/tt0498567">Summer Time Machine Blues</a>. Benque le nomine del film pare anglese, de facto il se tracta de un film <i>made in Japan</i>, que difficilemente essera publicate in Europa o foras del Japon: il non ha <i>samurai</i>s, <i>yakuza</i>s o <i>geisha</i>s que pote capturar le interesse del publico occidental, ma solmente un gruppo de juvene amicos que viagia in le tempore.
Le idea non es nove, illo es jam utilisate in multe filmes con differente intentiones: a vices le viage in le tempore es un pretexto pro introducer un aventura fantastic, a vices illo es le thema de episodios de scientia fictive, <i>sci-fi</i>, plus o minus plausibile logicamente. Le imagination non ha limites, e viages temporal pote esser un bon argumento pro fantasticar; imaginar le futuro es un cosa, ma il es facile e inevitabile discoperir que le viage in le passato causa un absurdo logic que rende tote le scena irrealisabile.
<em>Summer Time Machine Blues</em> supera, con inventiva e originalitate, le paradoxos que se crea quando on viagia in le passato, e se explica in un sympathic mixtura de humorismo, absurditate e logica infallibile, que al fin non mancara de portar le spectator a reflecter super le conceptos de tempore e necessitate.

Io ha trovate un <a href="http://www.youtube.com/watch?v=H5XftzMJHr4">trailer in YouTube</a>, ma illo es sin subtitulos...