<!--
.. title: Failed experiment
.. slug: failed-experiment
.. date: 2007-02-24 00:00:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
-->

<div style="float:right"><a href="http://www.mardy.it/archivos/active/1/347_large.jpg"><img src="http://www.mardy.it/archivos/active/1/347_large.jpg" width="200px"></a></div>
<i>Non tutte le ciambelle escono col buco</i>: this popular italian saying means that not all the "Ciambellas" (i.e. the cake in the photo on the side) are created with their hole, to mean that there's always the possibility of a failure.
In my case, the <i>ciambella</i> has been born with its hole, but, as expected by the usual laws of statistics, the error is always present, under some other form. I'm not a good photographer but if you think that the colours of the photos are wrong, well, let me tell you that they are not: the <i>ciambella</i> is really dark, and he smell reminds of some combustion. Anyway, the light stains (where the exterior of the cake was left on the pan) show that the interior is not totally dark, and maybe it's also edible &mdash; with some good will.
But why do I publish this photo? Well, the reason is that this is the first "cake" I try to make, and I wanted to document the happening.
If you want, you can send me an e-mail to book my next culinary product, but I give no warranty about its edibility (and inexplosivity).