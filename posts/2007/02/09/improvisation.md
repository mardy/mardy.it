<!--
.. title: Improvisation
.. slug: improvisation
.. date: 2007-02-09 00:00:00
.. tags: Helsinki
.. category: Helsinki
.. link: 
.. description: 
.. type: text
-->

<div style="float:right"><a href="http://www.mardy.it/archivos/active/1/343_large.jpg"><img width="200px" src="http://www.mardy.it/archivos/active/1/343_large.jpg"></a></div>
<a href="http://it.wikipedia.org/wiki/Semel_in_anno_licet_insanire"><i>"Semel in anno licet insanire"</i></a> (="Un vice in un anno il es permittite esser folle"), es un del tantes expressiones proverbial que ha nulle valor e que de quando in quando servi como excusa pro alicun damno. Io lo debe usar, perque le damno que io ha committite es grande, e pro esser plus precise le damno es grande 43 metros quadrate e costa multe millenas de euros!
Iste matino io ha signate le contracto de acquisition de un appartamento in Lauttasaari, un parve insula in le centro de Helsinki e un zona multo attractive (e expensive...) ben servite per le transporto public (plure pontes lo connecte al terra firme).
Lo que surprende, es que toto ha evenite in minus que un septimana! Io vadeva al agentia, diceva que io esseva interessate in acquirer un appartamento e nos fixava un appuntamento le die sequente pro visitar isto. Duo dies postea, le processo de acquisition esseva jam initiate. Alicun mi collega pensa que io esseva troppo rapide in mi decision &mdash; ille ha ration, ma on non pote esser lente in toto!

Io me movera in le appartamento in majo, e forsan initiara alicun renovation &mdash; ma solmente si alicuno me promitte que ille venira a visitar me: alteremente, le condition actual es plus que satisfacente pro me!


In un nota a latere, io communica que le temperatura actual es -18 grados; <i>lasciate ogni speranza, voi che atterrate!</i>