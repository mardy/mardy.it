<!--
.. title: Un piano di pace per l'Ucraina
.. slug: un-piano-di-pace-per-lucraina
.. date: 2023-01-21 22:33:56 UTC+03:00
.. tags: Italia, Russia, USA, Ucraina, Union Europee, actualitate, italiano, politica, reflexiones
.. category: 
.. link: 
.. description: 
.. type: text
-->

Tra i piani di pace proposti da vari politici europei e statunitensi, sarò
sincero, non ne ho letto nemmeno uno che mi paresse anche lontanamente
credibile. La mia impressione è che siano stati stesi più per raggirare i
propri elettori e presentarsi ai loro occhi come operatori di pace (mentre coi
fatti si supportano l'invio di armi e l'inasprimento delle sanzioni) che per un
genuino impegno, giacché ogni politico che abbia speso anche pochi minuti per
informarsi sulla situazione in Ucraina sa benissimo che questi piani di pace
sono non solo inaccettabili per la Russia, ma proprio impresentabili.

Un piano di pace credibile deve prima di tutto tener conto dei motivi che hanno
spinto la Russia ad invadere l'Ucraina e, soprattutto, che spingono il popolo
russo a supportare la guerra. È certamente legittimo, e forse anche
ragionevole, non credere ai motivi dichiarati ufficialmente: è molto probabile,
anzi, che i motivi che spingono la Russia a continuare questa “operazione
speciale” siano, almeno in parte, altri, di natura economica a vantaggio di
poche persone particolarmente potenti (costruttori di armi in primis). E
possiamo metterci il cuore in pace, e rassegnarci al fatto che questi motivi
non li conosceremo mai; non è nemmeno importante conoscerli, in fondo.

Quello che dobbiamo conoscere è l'umore del popolo russo, e in particolare i
motivi per cui la popolarità del presidente Putin ha ripreso a risalire dopo
l'invasione dell'Ucraina. L'informazione che si dà in occidente certamente non
aiuta in questo, perché è dal 2014 che omette di riportare fatti importanti
sulla guerra in Donbass. Ebbene, al popolo russo vengono quotidianamente
mostrate in televisione e sui giornali le immagini dei civili che perdono la
vita a Donetsk e nelle altre città del Donbass, proprio nelle zone centrali
delle città dove non sono presenti obiettivi militari. Possiamo considerarla
propaganda, certo, ma i fatti esistono e sono solo un'intensificazione di
quello che è avvenuto negli 8 anni precedenti, ben documentato dai rapporti
della missione dell'OSCE e dall'alto commissariato per i diritti umani delle
Nazioni Unite[^1].

Oltre a questo, l'invio massiccio di armi e gli episodi di discriminazione
contro artisti russi, atleti, personaggi della cultura e dello spettacolo,
talora verso la stessa lingua russa, vengono ampiamente pubblicizzati dai mezzi
locali e convincono i Russi del fatto che il loro paese stia combattendo una
guerra esistenziale contro un'orda di fascisti e, militarmente, contro la NATO
intera.

Se davvero ci fosse la volontà, da parte dell'Occidente, di raggiungere la
pace, si dovrebbe lavorare per distruggere questa immagine e disarmare la
propaganda russa togliendole i fatti su cui si basa. In particolare, sono
persuaso che molti dei punti seguenti sarebbero ben visti dalla popolazione
occidentale e demotiverebbero la popolazione russa (inclusi molti soldati
richiamati al fronte) a voler combattere questa guerra fratricida:

1. Rimozione di ogni discriminazione nei confronti della cultura russa e dei
   suoi esponenti.
2. Promessa che l'Ucraina non entrerà nella NATO o in alleanze militari che
   vadano oltre il reciproco impegno alla difesa (ovvero, no ad esercitazioni
   militari congiunte o basi straniere nel territorio dell'Ucraina, sì ad una
   promessa di intervento militare in caso di attacco).
3. Sospensione all'invio di armi finché l'Ucraina non abbia rimosso il titolo
   di eroe nazionale a Stepan Bandera e ad altri membri dell'[organizzazione
   nazista UPA](https://it.wikipedia.org/wiki/Massacri_di_polacchi_in_Volinia_e_Galizia_orientale).
4. Sospensione all'invio di armi finché l'Ucraina continuerà a bombardare i
   centri abitati privi di installazioni militari.

Si noti che nessuno di questi punti richiede la collaborazione o un previo
accordo con altri paesi (anche l'ingresso nella NATO è possibile solo col voto
unanime di tutti i membri esistenti, come ben ci ricorda la Turchia), e tutti
potrebbero essere immediatamente implementati. Quanto più crescerà il numero di
paesi occidentali che porteranno avanti queste politiche, tanto più crescerà
l'incertezza tra la popolazione russa, e col tempo anche l'incomprensione e il
discontento, visto che verrebbero a mancare i motivi ideologici che sostengono
il conflitto da parte russa.

Se poi vogliamo parlare di un piano di pace, concordato tra NATO, Ucraina e
Russia, questo potrebbe essere svolto lungo queste linee:

1. L'Ucraina condanna l'ideologia nazista (quindi Bandera e amici), accetta di
   aprire una commissione internazionale (includente anche la Russia) di
   inchiesta sulle stragi del Maidan e di Odessa.
2. L'Ucraina assegna al russo lo stato di seconda lingua ufficiale, similmente
   allo stato della lingua svedese in Finlandia[^2].
3. L'Ucraina promulga una serie di leggi per garantire una limitata autonomia
   alle 5 regioni attualmente sotto il controllo russo (inclusa la Crimea) e
   l'amnistia per tutti i ribelli che non si siano macchiati di crimini di
   guerra (quindi una sorta di accordi di Minsk estesi a tutte le regioni
   occupate).
4. L'Ucraina promette di non ospitare forze e apparecchiature militari di paesi
   terzi nel proprio territorio, né di partecipare ed esercitazioni militari
   congiunte, senza il consenso della Federazione Russa. Può tuttavia entrare
   in alleanze militari a scopo difensivo.
5. L'Ucraina promette di non mettere mai in atto sanzioni contro la Russia, né
   di richiedere visti ai cittadini russi per l'attraversamento della
   frontiera.
6. L'esercito russo si ritira, e viene temporaneamente sostituito dalle forze
   armate di un paese terzo, non membro NATO, scelto dall'Ucraina.
7. Nuovi referendum, sotto il controllo di osservatori internazionali (incluso
   ucraini e russi) nelle 5 regioni contese. I tempi saranno decisi
   dall'Ucraina. Russia e Ucraina si impegnano a riconoscerne e ad
   implementarne gli esiti.
8. Le missione di pace introdotta al punto 6 viene dissolta.

È fondamentale comprendere che le questioni territoriali sono di secondaria
importanza, e quello che più preme alla popolazione russa è l'avere buoni
rapporti coi paesi confinanti e non doversi preoccupare di colpi di stato,
rivoluzioni colorate fomentate dall'Occidente o di altri tentativi di mettere
il popolo ucraino contro quello russo. Se, per esempio, vi fosse una regione
russa che desiderasse separarsi dalla Federazione e venire annessa alla
Bielorussia, sono convinto che questo potrebbe avvenire in maniera pacifica
senza serie ripercussioni, in quanto i rapporti tra i due paesi sono buoni e la
Bielorussia non viene percepita come una minaccia. Questa era anche la
situazione con l'Ucraina prima del 2014[^3], ed è la situazione a cui si
dovrebbe cercare di ritornare.

[^1]: Si veda per esempio il [rapporto del periodo Maggio-Agosto
  2018](https://www.ohchr.org/sites/default/files/Documents/Countries/UA/ReportUkraineMay-August2018_EN.pdf),
  a pagina 5, punto 22. Altri rapporti sono visibili [in questa lista](https://www.ohchr.org/en/documents-listing?field_content_category_target_id[180]=180&field_content_category_target_id[182]=182&field_geolocation_target_id[1136]=1136&field_entity_target_id[1349]=1349&field_published_date_value[min]=&field_published_date_value[max]=&sort_bef_combine=field_published_date_value_DESC&page=0).
[^2]: Si osservi che in Finlandia lo svedese è la lingua madre del 5% della
  popolazione soltanto, mentre in Ucraina il russo è la lingua madre di circa
  il 30% della popolazione.
[^3]: Non esattamente, visto che vi erano già stati tentativi di rivoluzioni
  colorate e di instaurazione di governi anti-russi. Spero mi sia concessa
  questa semplificazione.
