<!--
.. title: A peace plan for Ukraine
.. slug: un-piano-di-pace-per-lucraina
.. date: 2023-01-21 22:33:56 UTC+03:00
.. tags: Italia, Russia, USA, Ucraina, Union Europee, actualitate, english, politica, reflexiones
.. category: 
.. link: 
.. description: 
.. type: text
-->

Among the peace plans proposed by various European and U.S. politicians, to be
frank, I haven't read a single one which I would consider even remotely
feasible. My impression is that such plans have been redacted more for a need
to fool one's voters and present onself as a peace operator (whereas one
factually supports sending of weapons and tightening of sanctions) than for a
genuine peace effort, since every politician that had spent even just a few
minutes to document oneself on the situation around Ukraine would perfectly
know that these peace plans are not just unacceptable by the Russians, but
plainly unpresentable.

A believable peace plan must first and foremost take into account the reasons
that pushed Russia to invade Ukraine and, above all, those who push the Russian
people to support the war. It's certainly legitimate, and even reasonable, to
doubt the official reasons: on the contrary, it's very likely that the reasons
who push Russia to continue this “special operation” are, at least in part,
others, economical in nature and to the benefit of a few especially powerful
individuals (arm producers above all). We can put our heart at rest, and
accept the fact that we'll never get to know the real reasons; but, on the
other hand, it's not even so important to know them, after all.

What we really need to know is the mood of the Russian population, and
especially the reasons why president Putin's popularity has risen after the
invasion of Ukraine. The mainstream information we get in the West is not
helpful at all in this, because it's since 2014 that it omits reporting
important facts about the war in Donbass. Well, nowadays the Russian people are
constantly fed images of civilians dying in Donetsk and in other cities of the
Donbass, right in the center of the cities, where there are no military
targets. We can call it propaganda, sure, but the facts are real and are just
an aggravated continuation of what has been happening for the past 8 years, all
well documented by the OSCE mission and by the Office of the High Commissioner
of the Human Rights of the United Nations[^1].

Besides, the massive transfer of weapons and the episodes of discrimination
against Russian artists, athletes, personalities of the culture and
entertainment, sometimes against the very Russian language, these are all
widely publicized by local mass media and get the Russians convinced that their
country is fighting an existential war against a horde of fascists, and,
militarily, against the whole of NATO.

If the West had really the will to restore peace it should work to destroy this
representation of itself and disarm the Russian propaganda by removing the
facts on which it's built. Specifically, I'm persuaded that many of the
following points would be well received by the Western population and would
demotivate the Russian people (including many of the soldiers stationed at the
front) in fighting this fratricidal war:

1. Removal of every discrimination against Russian culture and its
   representatives and performers.
2. Promise that Ukraine won't be let into NATO or in other military alliances
   that would go beyond the commitment to reciprocal defense (that is, no to
   joint military drills or foreign bases in the territory of Ukraine, yes to a
   promise of military intervention in case of attack).
3. Pausing the shipment of weapons until Ukraine removes the title of hero of
   Ukraine to Stepan Bandera and other members of the [nazist organisation
   UPA](https://en.wikipedia.org/wiki/Massacres_of_Poles_in_Volhynia_and_Eastern_Galicia).
4. Pausing the shipment of weapons until Ukraine stops bombing civilian
   settlements devoid of military installations.

It should be noted that none of these points require collaboration or
agreements between states (even joining NATO can only happen after the
unanymous vote of all current members, as Turkey reminds us), so they all could
be immediately implemented by any willing state. The bigger the number of
Western countries pushing forward these policies, the more uncertainty will
grow among the Russian population, and will ultimately transform into
incomprehension and dissatisfaction, since this would destroy the ideological
reasons that make the Russians support the conflict.

If we are to speak of a peace plan, agreed among NATO, Ukraine and Russia, then
it could be developed along these lines:

1. Ukraine condemns the nazist ideology (therefore Bandera and friends),
   accepts to open an international commission of inquiry (including Russia as
   well) over the massacres of Maidan square and Odessa.
2. Ukraine grants the status of second official language to the Russian
   language, similarly to how Swedish language is treated in Finland[^2].
3. Ukraine enacts laws to guarantee a limited autonomy to the 5 regions
   currently under Russian control (including Crimea) and amnesty for all those
   rebels that are not found guilty of war crimes (in other words, a sort of
   Minsk accords extended to all the occupied regions).
4. Ukraine promises not to host military forces or equipment from other
   countries in its territory, and to not participate in joint military drills,
   without the consent of the Russian federation. It can, however, join
   defensive military alliances.
5. Ukraine promises to never enact sanctions against Russia, nor to require
   visa from Russian citizens in order to cross its borders.
6. The Russian army withdraws and gets temporarily replaced by the army of a
   third country, not member of NATO, chosen by Ukraine.
7. New referendums, under the supervision of international observers (including
   Ukrainians and Russians) in the 5 contested regions. Times will be
   established by Ukraine. Russia and Ukraine commit to recognize and implement
   their results.
8. The peace mission introduced in point 6 gets wrapped up.

It's of fundamental importance understanding that territorial questions are
only a secondary matter, and that what is most pressing for the Russian people
is to have good relations with the neighbouring countries: not having to worry
about coups, colour revolutions stirred up by the West or about other attempts
to use Ukraine as a weapon against Russia. If, for example, there were a
Russian region that desired to separate itself from the federation and join
Belarus, I'm convinced that this could happen in a peaceful way without serious
repercussions, since the relationships between the two countries are good and
Belarus is not perceived as a threat. This was also the situation with Ukraine
before 2014[^3], and it's the situation to which we should strive to return to.

[^1]: See for example the [report for the period May-August
  2018](https://www.ohchr.org/sites/default/files/Documents/Countries/UA/ReportUkraineMay-August2018_EN.pdf),
  page 5, point 22. More reports can be found [in this list](https://www.ohchr.org/en/documents-listing?field_content_category_target_id[180]=180&field_content_category_target_id[182]=182&field_geolocation_target_id[1136]=1136&field_entity_target_id[1349]=1349&field_published_date_value[min]=&field_published_date_value[max]=&sort_bef_combine=field_published_date_value_DESC&page=0).
[^2]: Note that Swedish in Finland is the native language for just 5% of the
  population, whereas in Ukraine Russian is the native language of about 30% of
  the population.
[^3]: Not exactly, since there had already been attempts at colour revolutions
  resulting in anti-Russian governments. But I hope you'll pass this
  oversimplification of mine here.
