<!--
.. title: Back to Maemo!
.. slug: back-to-maemo
.. date: 2023-01-08 22:48:52 UTC+03:00
.. tags: english,informatica,kdeplanet,labor,maemo,personal,planetmaemo
.. category: 
.. link: 
.. description: 
.. type: text
-->

New year, new job. After [leaving
Canonical](link://slug/leaving-canonical-again) I'm back to working on the same
software platform on which I started working back in 2006: [Maemo](maemo.org).
Well, not exactly the vanilla Maemo, but rather its evolution known as [Aurora
OS](https://en.wikipedia.org/wiki/Aurora_OS_(Russian_Open_mobile_platform)),
which is based on [Sailfish OS](https://sailfishos.org/). This means I'm
actually back to fixing the very same bugs I introduced back then when I was
working in Nokia, since a lot of the middleware has remained the same.

At the moment OMP (the company developing Aurora OS) is mostly (or even
_exclusively_, AFAIK) targeting business customers, meaning corporations such
as the Russian posts and the railway company, whereas the consumer market is
seen as something in the far away future. Just in case you were curious whether
there were any devices on sale with Aurora OS.

I should also explain why I've refused several very well paying job
opportunities from Western companies: it's actually for a reason that has been
bothering me since last March, and it's a very simple one. The fact is that
because of the sanctions against Russia I already had to change bank once (as
the one I was using fell under sanctions), and in these months I've always been
working with the fear of not being able to receive my salary, since new
sanctions are introduced every month and more and more banks are being added to
the blacklist. That's why I've restricted my job search to companies having an
official presence in Russia; and to my surprise (and from some point of view, I
could even say _disappointment_) the selection and hiring processes were so
quick that I received three concrete offers while I was still working my last
weeks at Canonical, and I joined OMP on that very Monday after my last Friday
at Canonical.

I mean, I could have rested a bit, at least until the Christmas holidays, but
no. ☺  Anyway, I'm so far very happy with my new job, and speaking Russian at
work is something totally new for me, both challenging and rewarding at the
same time.
