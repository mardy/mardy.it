<!--
.. title: Un canto russe, in italiano
.. slug: una-canzone-russa-in-italiano
.. date: 2023-06-24 09:21:09 UTC+03:00
.. tags: italiano,Russia,cultura,musica,interlingua
.. category: 
.. link: 
.. description: 
.. type: text
-->

Io vos propone mi traduction del canto russe “Porta me, fluvio” ([Ты
неси меня река](https://www.youtube.com/watch?v=iNe9BdtlS7M) es le version
original):

<iframe
src="https://vk.com/video_ext.php?oid=7200355&id=456239300&hash=0c629be9cd494d8e&hd=1"
width="640" height="360" allow="autoplay; encrypted-media; fullscreen;
picture-in-picture;" frameborder="0" allowfullscreen></iframe>

Mi voce es un pochetto debile, alora si alicuno vole recantar lo melio, io
esserea solmente felice de isto!

