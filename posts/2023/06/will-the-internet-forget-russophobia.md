<!--
.. title: Will the internet forget russophobia?
.. slug: will-the-internet-forget-russophobia
.. date: 2023-06-04 10:41:02 UTC+03:00
.. tags: Bielorussia, Japon, Linux, Qt, Russia, USA, Ucraina, actualitate, english, etica, flickr, informatica, planetmaemo, reflexiones, societate
.. category: 
.. link: 
.. description: 
.. type: text
-->

I've often wondering what will happen when this horrific war in Europe will
finally be over. I won't be discussing politics here, but what is mostly
interesting to me is how (and if) all the companies who made high proclaims
about not doing business with Russia will justify their getting back into the
Russian market. They will probably count on the fact that the war will be long,
and that people will forget what these companies' stance was. After all, the
world has forget about all the companies who collaborated with the Nazi regime,
so we can expect the same to happen with this war.

But I don't think that's right: if you made a mistake, you should be held
accountable for it. You might be wondering what is the “mistake” I'm talking
about: that's **russophobia**, indeed. To put it simply, and make a concrete
example: if The Qt Company stops doing business with Russian companies and
blocks its downloads page to Russian IP addresses because of the war, *without
being forced by the government to do so*, but does not take similar measures
against other countries who wage wars which have caused way more deaths and
displacement of individuals, well, that's what I call “russophobia”. Of course,
I'm aware that there's way more than that, and that the hatred for all what is
Russian (including culture and sport competitions) is an even bigger issue, but
in this blog post I'm especially focused on the IT world, so please forgive my
semi-intentional narrow-mindness on this topic.

Now, I'm fully aware that we live in a mediatic bubble that directs our
decisions in a way that is almost automatic, and I'm sure that most people
working for companies who took russophobic decisions are not themselves
russophobic at all (and I'm not dismissing the possibility that even the very
same people who took these decisions might not be russophobic) and that these
decisions were taken on impulse, because “everyone else is doing the same” and
due to the media pressure that if you don't do that, you might get accused of
supporting the “wrong” side of the war.

But that's not an excuse, especially for “smart” people like IT engineers (and
I put the adjective between quotes [for a
reason](link://slug/the-idiotism-of-software-developers)), and especially after
the initial heat has passed and when, after more than one year of war, we
should have been exposed to different point of views and be able to evaluate
the situation more rationally. It has been therefore especially stunning for me
to learn that the Linux Kernel community, and hence The Linux Foundation, has
recently given room to russophobic behaviours, refusing a patch coming from the
Russian company Baikal (a CPU maker). For the record, the incriminated patch
was not related to supporting hardware produced by this company (not that this
would make the deed less serious, but at least one could have argued that there
could be some spot of logic in it):

```
From: Jakub Kicinski <kuba@kernel.org>
To: Serge Semin <Sergey.Semin@baikalelectronics.ru>
[...]

On Tue, 14 Mar 2023 01:42:24 +0300 Serge Semin wrote:
> From: Serge Semin <Sergey.Semin@baikalelectronics.ru>

We don't feel comfortable accepting patches from or relating 
to hardware produced by your organization.

Please withhold networking contributions until further notice.
```

([here](https://lore.kernel.org/all/20230314103316.313e5f61@kernel.org/) the
link to the original discussion). One week later, someone denounced this as a
violation to the Code of Conduct committee (unfortunately the only link I could
find to this is coming from a [Russian IT
forum](https://www.opennet.ru/openforum/vsluhforumID3/129994.html#529), and any
other references seem to have been removed from DuckDuckGo and Google), only to
receive a reply that it was all fine.

To me this is not fine. The war will end, sooner or later, but it bothers me
that we never learn from the past and repeat the same mistakes over and over.
We apparently know a lot about propaganda, yet we fail to recognize it when it
influences our own mind and actions. My humble contribution is the creation of
a page where I list the companies who have taken russophobic actions, and, on
the opposite side, companies (like Flickr and Zorin OS) who have stood out for
positive messages and helpful actions. My hope is that some of the listed
companies will find the courage to review their actions, and either correct
their stance, or at least clarify their reasons. So, I hereby present

<p style="text-align: center; font-size: 130%">
  <a href="https://github.com/mardy/russophobia">Denouncing russophobia</a>
</p>

where you'll find some of the good and some of the bad companies. I'm sure I'm
missing plenty of them: I just started recollecting my memories and searching
online a couple of days ago. I created this as a GitHub project, because indeed
I'm looking forward for contributions, to help me make the lists more complete.
I need to stress that the fact that a company has announced the suspension of
its business in Russia does not automatically make it russophobic: what we need
to look at is the *reason* for that decision: companies like LEGO and Nintendo,
for example, have suspended their operations citing logistic and financial
reasons; no judgement involved.

Let me repeat it once more, just to make sure there are no misunderstandings:
it's perfectly fine for businesses to take a stance on politics, and sometimes
it might be even praiseworthy; but if a company is international, and does not
apply the same reasoning to other armed conflicts, or seem to care only about
certain human rights violations and not others, then it's a case of double
standards which we need to be aware of, and make the company think twice about
it. And that's also the reason why you won't find any Ukrainian company among
the “bad” ones, because in their case the reaction is perfectly understandable
and they can hardly be accused of adopting double standards (well, technically
speaking, they are adopting double standards, but when you are so directly
impacted I think it does not deserve a blame): if it's your house which burns,
you should definitely scream about it, even if you previously have been silent
about your neighbour house's burning.

**I'm especially looking forward for more “good” companies**, who have shown empathy
towards the people affected by the war (and maybe even collected money to help
them) while refraining from taking the judging role and forgetting about all
the injustice and suffering that other wars have caused (including on that very
same piece of land that suddenly appeared on all newspapers' front pages on
February 24th, 2022). I hope that these companies can serve as an example of
positive action, humanity, and love.
