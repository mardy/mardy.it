<!--
.. title: Una canzone russa, in italiano
.. slug: una-canzone-russa-in-italiano
.. date: 2023-06-24 09:21:09 UTC+03:00
.. tags: italiano,Russia,cultura,musica
.. category: 
.. link: 
.. description: 
.. type: text
-->

Vi propongo la mia traduzione della canzone russa “Fiume portami tu via” ([Ты
неси меня река](https://www.youtube.com/watch?v=iNe9BdtlS7M) è la versione
originale):

<iframe
src="https://vk.com/video_ext.php?oid=7200355&id=456239300&hash=0c629be9cd494d8e&hd=1"
width="640" height="360" allow="autoplay; encrypted-media; fullscreen;
picture-in-picture;" frameborder="0" allowfullscreen></iframe>

La mia voce è un po' debolina, quindi se qualcuno volesse ricantarla meglio, ne
sarei felicissimo!
