<!--
.. title: Un editoriale di Marco Travaglio
.. slug: un-editoriale-di-marco-travaglio
.. date: 2023-03-01 20:37:28 UTC+03:00
.. tags: Italia,Russia,USA,Ucraina,Union Europee,actualitate,information,italiano,politica,reflexiones
.. category: 
.. link: 
.. description: 
.. type: text
-->

Nonostante io non legga più Il Fatto Quotidiano (per i motivi spiegati
[qui](link://slug/il-fatto-quotidiano-plagio-e-propaganda), che restano tuttora
validi), continuo a imbattermi negli editoriali di Marco Travaglio, che spesso
apprezzo. Oggi invece mi sono imbattuto nell'introduzione del suo nuovo libro
[“Scemi di
guerra”](https://www.ilfattoquotidiano.it/in-edicola/articoli/2023/02/24/un-anno-di-bugie-per-mettere-agli-italiani-lelmetto-no-pax/7075675/),
e ve ne riporto un estratto che ho trovato particolarmente incisivo.

---------------------------------

Abbiamo abolito la storia. È vietato raccontare ciò che è accaduto in Ucraina
prima del 24 febbraio 2022: gli otto anni di guerra civile in Donbass dopo il
golpe bianco (anzi, nero) di Euromaidan nel 2014 e le migliaia di morti e feriti
causati dai continui attacchi delle truppe di Kiev e delle milizie filo-naziste
al seguito contro le popolazioni russofone e russofile che, col sostegno di
Mosca, chiedevano l’indipendenza o almeno l’autonomia. Il tutto in barba ai due
accordi di Minsk. La versione ufficiale, l’unica autorizzata, è che prima del
2022 non è successo niente: una mattina Putin s’è svegliato più pazzo del
solito e ha invaso l’Ucraina. Se la gente scoprisse la verità, capirebbe che il
mantra atlantista “Putin aggressore e Zelensky aggredito” vale solo dal 2022:
prima, per otto anni, gli aggressori erano i governi di Kiev (l’ultimo, quello
di Zelensky) e gli aggrediti i popoli del Donbass. Fra le vittime, c’è il
giornalista italiano Andrea Rocchelli, ucciso dall’esercito ucraino… Abbiamo
abolito la geografia. Proibito mostrare la cartina dell’allargamento della Nato
a Est negli ultimi 25 anni (da 16 a 30 membri)… Eppure, che la Nato si sia
allargata a Est, accerchiando e assediando la Russia, minacciandone la
sicurezza con installazioni di missili nucleari sempre più vicine al confine,
in barba alle promesse fatte a Gorbaciov nel 1990, fino all’ultima provocazione
di annunciare l’imminente ingresso nell’Alleanza dei vicini di casa della
Russia – Georgia e Ucraina – è un fatto storico indiscutibile…

L’altra cartina proibita è quella dei Paesi che non condannano o non sanzionano
la Russia, o se ne restano neutrali: quasi tutta l’Asia, l’Africa e l’America
Latina, cioè l’87% della popolazione mondiale. Ma al nostro piccolo mondo
antico occidentale piace far credere che Putin è isolato e noi lo stiamo
circondando. Sul fatto che Cina, India, Brasile e altri paesucoli stiano con
lui o non stiano con noi, meglio sorvolare: altrimenti lo capiscono tutti che
le sanzioni non funzionano… Solo abolendo la storia si può credere al
presidente Sergio Mattarella quando ripete che “l’Ucraina è la prima guerra nel
cuore dell’Europa nel dopoguerra”. E Belgrado bombardata anche dall’Italia nel
1999 dov’è, in Oceania? E chi era il vicepremier del governo D’Alema che
bombardava Belgrado? Un certo Mattarella… Abbiamo abolito il rispetto per le
altre culture. In una folle ondata di russofobia, abbiamo visto ostracizzare
direttori d’orchestra, cantanti liriche, pianiste di fama mondiale, fotografi,
atleti (anche paraolimpici), persino gatti e querce, soltanto perché russi. E
poi censurare corsi su Dostoevskij, cancellare dai teatri i balletti di
Cajkovskij, addirittura estromettere la delegazione russa dalle celebrazioni
per la liberazione di Auschwitz. Come se il lager l’avessero liberato gli
americani o gli ucraini e non l’Armata Rossa… i trombettieri della Nato
propagandano la bufala dell’“euroatlantismo” e gli scemi di guerra se la
bevono, senz’accorgersi che mai come oggi gli interessi dell’Europa sono
opposti a quelli dell’America. 

