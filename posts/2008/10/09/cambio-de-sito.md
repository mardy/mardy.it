<!--
.. title: Cambio de sito
.. slug: cambio-de-sito
.. date: 2008-10-09 00:00:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
-->

Io ha decidite de cambiar le motor usate pro generar mi blog: ora illo es blogger.com.
Le adresse del sito remane le mesme, ma alicun ligamines poterea non functionar.

Io suppone que necuno va leger iste message; ma si pro hazardo tu lo lege, rememora que le adresse principal es <a href="http://www.mardy.it/index.html">http://www.mardy.it</a>.

Si tu recipe iste message per RSS, per favor renova le sorgente per visitar le pagina web del sito.