<!--
.. title: Photos del vacantias estive
.. slug: photos-del-vacantias-estive
.. date: 2008-10-01 00:00:00
.. tags: turismo,Russia
.. category: turismo
.. link: 
.. description: 
.. type: text
-->

<div style="float:right; margin-left:1em"><a href="http://www.mardy.it/galleria/v/Russia/augusto2008"><img src="http://www.mardy.it/galleria/d/7453-2/pict4454.jpg" width="300px"></a></div><p>Vostre patientia ha essite premiate! In le improbabile caso que vos ancora controla iste sito in le sperantia de vider le photos de mi vacantias, que io ha promittite desde tempores immemorabile, ben, ecce a vos un altere demonstration de mi abilitate in mantener le promissas!</p>
<p>Io ha cargate le photos del duo septimanas que io ha passate in Sancte Petroburgo (e localitates proxime) al initio de augusto <a href="http://www.mardy.it/galleria/v/Russia/augusto2008/">hic, in le galleria</a>. Io los ha dividite in plure album-es (circa un album per die), e in cata album il ha un sub-album que se appella &ldquo;Personal&rdquo;, protegite per contrasigno &mdash; si vos non cognosce le contrasigno, per favor <a href="http://www.mardy.it/ia/contacta">contacta me</a> e sia preste a versar un amonta considerabile de moneta pro obtener lo.</p> 
<p>Il non esseva facile seliger un photo pro iste articulo, ma al fin io ha decidite que iste &ldquo;estufa del nive&rdquo; (nota le skis pro skiar sur le nive, in hiberno) es le plus belle e representative &mdash; ma si vos trova photos melior, dice me!</p>
<p>Confidente que io ha plenate vostre tempore libere, io lassa le parola al photos (si alicuno crede que io va commentar e describer los, lassa me dicer que io ha cargate Santa Claus de isto).