<!--
.. title: Involution
.. slug: involution
.. date: 2008-06-24 00:00:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
-->

<div style="float:right; padding-left:1em; padding-bottom:1em"><a href="http://www.mardy.it/galleria/v/Finlandia/PrimePellicula"><img src="http://www.mardy.it/galleria/d/5338-2/img001-003.jpg" width="300px"></a></div>
Durante que le major parte del photographos ha passate del photographia in pellicula (analogic) al photographia digital, io ha percurrite le strata opposite, e ha comprate un photocamera analogic. Le ration de iste selection es in maxime parte debite a mi curiositate pro un technologia provate e laudate pro su qualitate. Io voleva facer un pauco de experientia con le pellicula, ante que su gradual disapparition lo rende obsolete (assi io potera contar a mi numerosissime discendentia, que io ha photographate sur pellicula!).
Io ha comprate de Japon un camera Minolta &alpha;7 (in Europa illo se appella Dynax 7), que probabilemente es le modello le plus avantiate de cameras analogic producite per Minolta; io ha seligite iste camera perque le montatura es perfectemente compatibile con illo del objectivos de mi Sony &alpha;, ergo io non debe affrontar un duple expensa. Le sol nota que merita attention es le facto que le pellicula 35mm es plus grande que le sensor CCD del Sony &alpha; que es circa 24mm, e le objectivos que es projectate specialmente pro le sensor del Sony rendera un immagine partialmente obscur longo le bordos del quadro del pellicula. Del altere latere, isto significa que le objectivos que es compatibile con ambe le formatos, in le 35mm rende un angulo de visual multo major, e isto es un del motivationes que me ha convincite a comprar un camera analogic.
Ma basta parolas! Io ha developpate mi prime pellicula, e le imagines es <a href="http://www.mardy.it/galleria/v/Finlandia/PrimePellicula">in le galleria</a>; io los ha cargate totes, benque multe es quasi impresentabile (pro alicun ration, le focus es errate). Ah!, ante que vos me accusa de incapacitate/follia, lassa me dicer que le photo a latere es realisate con un lente de typo "fisheye" (oculo de pisce), un grandangulo con un accentuatissime distorsion &mdash; isto es un effetto intendite, non impreviste!