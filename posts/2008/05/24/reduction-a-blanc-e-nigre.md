<!--
.. title: Reduction a blanc e nigre
.. slug: reduction-a-blanc-e-nigre
.. date: 2008-05-24 00:00:00
.. tags: arte,informatica
.. category: arte
.. link: 
.. description: 
.. type: text
-->

In <a href="http://www.mardy.it/galleria/v/Finlandia/Lauttasaari/sudest/">iste pagina del galleria</a> io ha cargate mi ultime album de Lauttasaari (infortunatemente pro vos, le &ldquo;ultime&rdquo; debe esser intendite solmente in senso de vicinitate temporal). Benque tote le photos es quasi identic a les que io ha prendite precedentemente, iste vice le subjecto del photos esseva le area in le Sud-Est de Lauttasaari. Con mi (non troppo grande) surprisa io trovava un altere parve plagia, que es multo simile a le que io jam photographava; con le differentia que in iste il non ha un nave pro le jocos del infantes.
Inter le quantitate de photos insignificante que io prendeva, il habeva un que esseva particularmente insignificante, e que io esseva in le puncto de deler; ma pro curiositate, io decideva de experimentar e convertir lo in blanc e nigre, pro vider si le photo acquireva un senso proprie. Le conversion automatic a blanc e nigre reduceva le visibilitate del elementos de disturbo que il habeva in le fundo, ma al mesme tempore destrueva le contrasto e rendeva le imagine plan e enoiante.
Un del multissime utile &mdash; e confuse &mdash; functionalitates de <a href="http://www.gimp.org">The Gimp</a> es le <i>channel mixer</i>, que permitte de specificar le intensitate del tres componentes de color (rubie, verde e blau) del imagine; iste function, combinate con le transformation in monochromia, permitte de calibrar manualmente le &ldquo;peso&rdquo; del colores in maniera tal que le imagine in B/N non resulta totalmente plan, e le differentias inter le areas de color que le autor considera plus importante es preservate.
In basso on vide le resultato &mdash; io ha essayate de calibrar le colores de maniera que le photo appare multo contrastate. Le photo es ancora insignificante, ma a prime vista illo appare interessante e io ha decidite de non deler lo (clicca pro ingrandir lo). :-)
<a href="http://www.mardy.it/galleria/d/5167-1/dsc01531.jpg"><img src="http://www.mardy.it/galleria/d/5169-2/dsc01531.jpg" style="padding: 0.5em; margin-left: auto; margin-right: auto; display: block;"></a>