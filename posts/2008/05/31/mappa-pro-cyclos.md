<!--
.. title: Mappa pro cyclos
.. slug: mappa-pro-cyclos
.. date: 2008-05-31 00:00:00
.. tags: turismo,reflexiones,Finlandia
.. category: turismo
.. link: 
.. description: 
.. type: text
-->

<p>Iste matino, incoragiate per le favorabile conditiones atmospheric, io ha decidite de prender mi bicycletta pro tentar de levar me de mi constante stato vegetative. Il ha multe areas proxime a Helsinki que on pote facilemente attinger per un bicyclo, e io ha visitate solmente un parve fraction de los; pro isto, il me pareva un bon idea probar a explorar un loco nove. Ante partir, io ha consultate le internet pro trovar un mappa del area a visitar (mi spirito explorative non es ancora multo developpate) e io ha trovate que le sito del transportos public contene un section dedicate al cyclistas: in <a href="http://kevytliikenne.ytv.fi/en/">le <em>Journey Planner for cycling</em></a> il ha un mappa interactive del region de Helsinki, Espoo e Vantaa, in le qual il es possibile inserer le location de partentia e de destination e le systema computa le via <em>cyclabile</em> inter los e lo monstra in evidentia in le mappa:<p>
<center><img src="http://www.mardy.it/archivos/imagines/jpcyclo.png"></center>
<p>Le mappa evidentia le tractos de strata de diverse typo; nota que quasi tote le itinere es sur vias cyclabile, sur le qual <b>automobiles non passa!</b></p>
<p>Altere belle functiones de iste sito include le possibilitate de evidentiar locos de interesse pro cyclistas e sportivos in general: servicios de prestito de bicyclos, parcamentos pro bicyclos, tractos de strata especialmente rapide o scarpate, locos pro sport (externe, claudite, acquatic, etc. &mdash; oh, mesmo pro animales!), cafés e refugios, semaphoros e altere cosas plus o minus interessante como stationes, scholas, botecas, etc.<br>
In ultra, il es possibile anque evidentiar alicun percursos especialmente interessante:</p>
<div style="float:left; padding:1em">
<img src="http://www.mardy.it/archivos/imagines/jpscenic.png">
Le percurso scenic (nota le scala!)
</div>
<div style="float:left; padding:1em">
<img src="http://www.mardy.it/archivos/imagines/jpespoo.png">
Le percurso del costa de Espoo
</div>
<p style="clear:both">sin considerar un lista de percursos national e un mappa del stratas que evidentia le varie typos de terreno.</p>
<p>Le question del die es: perque il non ha un servicio simile in Italia? Ah, que stupide, io quasi oblidava: <em>in Italia nos non ha percursos cyclabile!</em></p>

<p style="padding-top:2em">P.S.: no, io non ha cyclate tote le percurso del prime mappa! Io tosto prendeva altere stratas, plus proxime al costa marin, e arrivava a Haukilahti (que es le loco marcate in le mappa per le distantia &ldquo;8.0 km&rdquo;) e postea invertiva le direction e tornava a casa per le via decidite per <i>Journey Planner</i>.</p>