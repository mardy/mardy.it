<!--
.. title: Aviditate
.. slug: aviditate
.. date: 2008-05-22 00:00:00
.. tags: arte,informatica
.. category: arte
.. link: 
.. description: 
.. type: text
-->

Mi prime experimento con <a href="http://www.gimp.org">The GIMP</a>, le plus famose programma pro Linux de edition de imagines:
<a href="http://www.mardy.it/galleria/v/Finlandia/Lauttasaari/sudest/dsc01537.jpg.html"><img src="http://www.mardy.it/galleria/d/5175-2/dsc01537.jpg" style="padding: 0.5em; margin-left: auto; margin-right: auto; display: block;"></a><center><i>Aviditate</i></center>
<p>Normalmente io non ama modificar le photographias con processos informatic, perque io ha le sensation de &ldquo;fraudar&rdquo; le spectator. On deberea producer photos belle, sin besonio de alterar los. Certemente le alteration per computator de un photo pote render resultatos excellente, ma io crede que illo es un arte per se, differente de lo que usualmente on intende pro &ldquo;photographia&rdquo;.
Ma post que on debe probar toto in le vita, io ha probate a modificar un photo (lo que vos vide) per mantener le colores sur le ponte, e render blanc e nigre toto le resto. Le resultato non es perfecte, ma si on non aggrandi le photo, le defectos es difficilemente perceptibile.
Totevia io non es totalmente satisfacite, perque le photo non transmitte le senso que io voleva poner in illo &mdash; forsan un augmento de contrasto haberea adjutate. Pena es, que ora il es troppo tarde: io salvava le photo modificate sur le archivo original, que ergo es perdite...