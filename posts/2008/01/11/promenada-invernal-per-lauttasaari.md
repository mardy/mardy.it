<!--
.. title: Promenada invernal per Lauttasaari
.. slug: promenada-invernal-per-lauttasaari
.. date: 2008-01-11 00:00:00
.. tags: Helsinki,natura
.. category: Helsinki
.. link: 
.. description: 
.. type: text
-->

<div style="float:right"><a href="http://www.mardy.it/ia/node/1269"><img src="http://www.mardy.it/archivos/active/0/1273_small.jpg" width="300px"></a></div>
Post cinque menses, le dominica passate io ha facite ancora le promenada per Lauttasaari que io faceva quando mi parentes esseva hic a visitar me, in augusto. Nunc le panorama es cambiate completemente: le mar es partialmente glaciate, e mi curiositate ha essite specialmente trovar le puncto ubi le mar passa de glacie in aqua. Inter le glacie e le aqua liquide il ha uno o duo metros de mar semiliquide, gelatinose, que se move per le undas, ma multo plus pigremente.
Infortunatemente le conditiones atmospheric non esseva multo ben, perque le jam debile sol esseva quasi totalmente coperite per nubes (que in le dies successive ha portate un pauco de nive e, con le altiamento del temperatura, pluvia). Ma post que io non sape quando io facera ancora le mesme promenada io prefere condivider le photos ora, ante que passa altere cinque menses e le glacie potera esser trovate solmente in le refrigerator. Alora, si vos vole, <a href="http://www.mardy.it/ia/node/1269">reguarda le photos con un tremor de frigido</a>.