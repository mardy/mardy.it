<!--
.. title: Galleria
.. slug: galleria
.. date: 2008-01-29 00:00:00
.. tags: informatica
.. category: informatica
.. link: 
.. description: 
.. type: text
-->

Post que io notava que le sito esseva particularmente lente, e sovente le galleria del imagines non functionava (illo non appareva del toto!), io ha volite probar un altere programma pro monstrar mi operas <em>an</em>artistic: <a href="http://mardy.it/galleria/main.php">le resultato es hic</a>.
In theoria, iste systema deberea esser plus rapide, e le aspecto esthetic un poco melior, ma non ben integrate con le resto del sito. Io ancora es indecise si utilisar iste systema (que es appellate <a href="http://gallery.menalto.com/">Gallery2</a>) o restar con le actual: un importante <i>pro</i> de Gallery2 es que le importation del imagines es extrememente rapide e simple, e le possibilitates de personalisation es plus ample.

Si alicuno ha commentos in proposito, io es tote oculos. :-)