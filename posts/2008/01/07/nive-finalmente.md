<!--
.. title: Nive, finalmente!
.. slug: nive-finalmente
.. date: 2008-01-07 00:00:00
.. tags: Helsinki,natura
.. category: Helsinki
.. link: 
.. description: 
.. type: text
-->

Le prime nive del anno ha cadite; non multe, ma bastante pro coperir stratas e tectos de blanc. Le photo sequente ha essite prendite del porta de mi casa.
<a href="http://www.mardy.it/archivos/active/0/1267_large.jpg"><img src="http://www.mardy.it/archivos/active/0/1267_small.jpg" style="padding: 0.5em; margin-left: auto; margin-right: auto; display: block;"></a>
<p>E isto es le strata principal de Lauttasaari (nota le enorme traffico!):
<a href="http://www.mardy.it/archivos/active/0/1266_large.jpg"><img src="http://www.mardy.it/archivos/active/0/1266_small.jpg" style="padding: 0.5em; margin-left: auto; margin-right: auto; display: block;"></a>
<p>In caso que alicuno se pone le question: no, le floccos de nive non es tanto luminose in Finlandia! Illos appare luminose perque illos reflecteva le flash de mi camera. De facto, le floccos de iste dies es multo parve e le nive es multo sic &mdash; lo que es perfecte, perque le nive appare plus blanc e non deveni sordide multo facilemente.
<p>Ma io debe concluder con un mal nova: mercuridi le temperatura se altiara e tornara a 0 grados (o forsan positive), lo que significa que tote le nive va disfacer se. :-(