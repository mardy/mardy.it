<!--
.. title: Photos de Treviso, Natal 2007
.. slug: photos-de-treviso-natal-2007
.. date: 2008-01-05 00:00:00
.. tags: turismo,treviso
.. category: turismo
.. link: 
.. description: 
.. type: text
-->

<div style="float:right"><a href="http://www.mardy.it/ia/node/1156"><img src="http://www.mardy.it/archivos/active/0/1232_small.jpg" width="300px"></a></div>
A causa de mi permanentia in Finlandia, quando io retorna in Italia mi oculos es plus simile a les de un tourista que a les de un habitante native. Le <i>syndrome del photo-tourista</i> me colpa e io senti un incontrollabile desiro de photographar toto lo que io vide; ma fortunatemente, le capacitate de mi photocamera limita le quantitate de photos que io pote prender, e le velocitate (o lentessa) del connection internet limita le quantitate de photos que io pote cargar in le sito.
Le symptomas de mi morbo es visibile <a href="http://www.mardy.it/ia/node/1156">in le galleria</a>, ubi io ha cargate le photos de Treviso (anque nocturne), que probabilemente essera banal e enoiante pro illes qui pote vider lo omne die, ma io spera que illos portara un pauco de curiositate pro illes qui nunquam ha essite in iste locos.
Il ha plure photos prendite in le mesme loco; a vos le carga de identificar lo.