<!--
.. title: Napoli retorna in Oriente
.. slug: napoli-retorna-in-oriente
.. date: 2008-09-10 00:00:00
.. tags: politica,Italia,actualitate
.. category: politica
.. link: 
.. description: 
.. type: text
-->

In <a href="http://www.ilgiornale.it/a.pic1?ID=277050">un articulo in le magazin <i>Il Giornale</i></a> del 19 julio 2008, nostre amatissime prime ministro Silvio Berlusconi annuncia &ldquo;Io ha reportate Napoli in le Occidente&rdquo;, pro significar que ille habeva solvite le emergentia del immunditias que desde plure menses (o annos) affligeva le citate.
Le problema ha essite &ldquo;solvite&rdquo; per portar le immunditias foras del citate e arder los in le periferia, in locos distante del oculos del populo; e <a href="http://it.youtube.com/watch?v=mtHnPYgEVJ0">impedir omne manifestation de opinion contrari</a>, in violation del articulo 21 del constitution (que nostre politicos non cognosce, ma necuno es perfecte).
E finalmente il es un nova de iste dies, que Napoli ha ancora abandonate le Occidente:
<center><object width="425" height="344"><param name="movie" value="http://www.youtube.com/v/hYlAw88E3rA&hl=en&fs=1"></param><param name="allowFullScreen" value="true"></param><embed src="http://www.youtube.com/v/hYlAw88E3rA&hl=en&fs=1" type="application/x-shockwave-flash" allowfullscreen="true" width="425" height="344"></embed></object></center>
E illes qui sperava in le establimento del recolta differentiate del immunditias ha essite ancora deluse...

P.S.: Si, io sape que io debe ancora publicar le photos de mi vacantias in Sancte Petroburgo! Ma attende, le patientia es le virtute del fortes!
(ma io non dice que io va portar Sancte Petroburgo in Occidente...)