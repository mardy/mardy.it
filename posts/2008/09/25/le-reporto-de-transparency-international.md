<!--
.. title: Le reporto de Transparency International
.. slug: le-reporto-de-transparency-international
.. date: 2008-09-25 00:00:00
.. tags: politica,Finlandia,Italia,curiositates
.. category: politica
.. link: 
.. description: 
.. type: text
-->

<div style="float:right; margin-left:1em"><img src="http://www.mardy.it/archivos/imagines/cpi_2008_420.gif"></div>
<p><a href="http://www.transparency.org">Transparency Internation</a> ha publicate le tabula del &ldquo;Indice de Perception de Corruction&rdquo; (CPI, in anglese <i>Corruption Perception Index</i>), que mesura le grado de corruption del officieros e del politicos, derivate per un quantitate de fontes differente.</p>
<p>Iste anno, le situation de Finlandia e Italia se ha pejorate; Finlandia ha passate del prime placia que illo teneva in le 2007, a un misere e vergoniose quinte position, durante que Italia ha precipitate del 41me al 55me placia, que es comocunque un position dignitose pro un pais como le nostre.<p>
Secundo le studio, paises como Botswana, Oman, Bahrain e Bhutan, que io vos defia a locar exactemente in un mappa geographic sin nomines, es minus corrupte que Italia; ma iste studio es obviemente non veritabile, perque illo es in total contrasto con le facto que nulle citatano italian cognosce alicun persona que ha essite effectivemente in prison a causa de corruption!</p>
<p style="margin-top:1em">Ma pro le credulones, ecce le tabulas pro le <a href="http://www.transparency.org/policy_research/surveys_indices/cpi/2007">2007</a> e pro le <a href="http://www.transparency.org/news_room/in_focus/2008/cpi2008/cpi_2008_table">2008</a></p>