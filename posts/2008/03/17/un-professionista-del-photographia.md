<!--
.. title: Un professionista del photographia
.. slug: un-professionista-del-photographia
.. date: 2008-03-17 00:00:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
-->

Isto es le definition de lo que io <em>non</em> es. Ma pro illuder vos (e me mesme) que io es un professionista del photographia, io ha comprate un photocamera semi-professional: le <a href="http://www.sony.net/Products/dslr/a350">Sony &alpha;350</a>. Io voleva comprar un Sony &alpha; (lege: &ldquo;alpha&rdquo;, illo es le littera grec) desde januario, quando io ordinava le &alpha;100; ma post alicun septimanas le boteca me informava que ille modello non plus esseva in production, e alora io ordinava un &alpha;200. Ma proprio quando iste modello esseva proxime al distribution, altere duo modellos de Sony Alpha esseva annunciate, le &alpha;300 e le &alpha;350, que respecto al &alpha;200 ha in plus le function <i>LiveView</i>, que significa que in le display LCD on pote sempre vider le subjecto assi como le sensor CCD lo vide (iste functionalitate es presente in tote le cameras digital compacte, ma solmente in pauc modellos de cameras reflex), e alicun megapixel plus. Curiosemente, le &alpha;300, que ha functionalitates inferior al &alpha;350, es commercialisate plus tarde, e a un precio plus alte. Io imagina que isto es un error, ma io ha essite silente perque isto non es <i>cosa nostre</i>. ;-)
Heri, in compania de duo sympathic <i>couchsurfers</i> de Croatia e Serbia, io ha facite un visita al insula de Suomenlinna, e ha &ldquo;baptisate&rdquo; mi Sony Alpha &mdash; benque le die esseva gris e legermente nivose. Le photos es in le galleria, a <a href="http://www.mardy.it/galleria/v/Finlandia/Suomenlinna/Suomenlinna20080316/">iste adresse</a>; de facto, illos es indistinguibile de altere photos prendite con un camera compacte (al minus per mi oculos inexperte), si non per le resolution, que totevia non es visibile in le version reducite que io ha cargate in le sito.
Ma pro dar un exemplo, io monstra un imagine complete:
<a href="http://www.mardy.it/galleria/d/3113-1/dsc00140.jpg"><img src="http://www.mardy.it/galleria/d/3113-1/dsc00140.jpg" style="padding: 0.5em; margin-left: auto; margin-right: auto; display: block;" width="640"></a>
E un detalio del mesme imagine, al resolution de 14 megapixels (le sensibilitate ISO es 400, ergo il ha un pauco de rumor, in le areas obscur):
<img src="http://www.mardy.it/archivos/imagines/DetalioRota.jpg" style="padding: 0.5em; margin-left: auto; margin-right: auto; display: block;">
Ma istos es detalios insignificante; lo que importa es que quando io prende photographias con le Sony Alpha io me senti moralmente obligate a facer melio!