<!--
.. title: Lauttasaari blanc e blau
.. slug: lauttasaari-blanc-e-blau
.. date: 2008-03-30 00:00:00
.. tags: Helsinki,natura
.. category: Helsinki
.. link: 
.. description: 
.. type: text
-->

<div style="float:right"><a href="http://www.mardy.it/galleria/v/Finlandia/Lauttasaari/martio2008/"><img src="http://www.mardy.it/galleria/d/3567-2/dsc00604.jpg" width="300px"></a></div>
Io es un character abitudinari, totalmente sin phantasia! Anque hodie, pro prender alicun photos, io ha repetite le promenada a costa del mar in Lauttasaari, e de consequentia le photos non pote esser troppo differente de los qui vos jam ha vidite 1000 vices. Ma pro vostre fortuna, le subtil strato de nive (illo non esseva tanto subtil, alicun dies antea!) colora le panoramas de blanc, assi que anque le plus povres qui solmente dispone de un monitor blanc/nigre non es multo disavantagiate.

Clicca sur le photo pro vader al album. Nota que quando on vide le photos, on pote cliccar ancora sur le imagine pro aggrandir lo plus, usque a un dimension maxime de 2048x2048 (un bon compromisso inter qualitate e dimension).