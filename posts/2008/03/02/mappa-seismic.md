<!--
.. title: Mappa seismic
.. slug: mappa-seismic
.. date: 2008-03-02 00:00:00
.. tags: actualitate
.. category: actualitate
.. link: 
.. description: 
.. type: text
-->

Hodie io audiva le nova que alicun concussiones seismic esseva registrate in le Appennino Tusco-Emilian; pro curiositate, io cercava alicun datos super le eventos seismic actualmente observate in Italia, e trovava un sito interessante. Il es le <a href="http://www.emsc-csem.org/index.php?page=home">European-Mediterranean Seismological Centre</a>, que reporta informationes super le plus recente registrationes de tremores de terra, quasi in tempore real. In particular, le pagina <a href="http://www.emsc-csem.org/index.php?page=home&sub=gmap">http://www.emsc-csem.org/index.php?page=home&sub=gmap</a> reporta le eventos seismic registrate in le ultime 48 horas, in un mappa. Per cliccar sur un evento, on lo pote locar sur le mappa.
Si io ha ben comprendite, le datos es complete pro le Europa, durante que del resto del mundo on monstra solmente le tremores habente un magnitudo superior a un certe valor. Ma il esserea interessante comparar un mappa seismic del Italia con illo del Japon... forsan alicuno cognosce un servicio simile pro le altere areas del mundo?