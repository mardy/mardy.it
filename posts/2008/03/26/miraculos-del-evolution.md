<!--
.. title: Miraculos del evolution
.. slug: miraculos-del-evolution
.. date: 2008-03-26 00:00:00
.. tags: curiositates,natura,paese
.. category: curiositates
.. link: 
.. description: 
.. type: text
-->

Reguarda iste photographia con attention:
<img src="http://www.mardy.it/archivos/imagines/cebus.jpg" style="padding: 0.5em; margin-left: auto; margin-right: auto; display: block;">
Illo es un <i>Cebus capucinus</i>, o <a href="http://en.wikipedia.org/wiki/White-headed_Capuchin">&ldquo;capucino del capite blanc&rdquo;</a>, un simia que habita in le forestas del America del sud. Perque io es interessate in iste animal? Le responsa es in mi jardin (le catto rubie es mi adorate Pastiglio):
<a href="http://www.mardy.it/galleria/d/3170-1/dsc00391.jpg"><img src="http://www.mardy.it/galleria/d/3172-2/dsc00391.jpg" style="padding: 0.5em; margin-left: auto; margin-right: auto; width: 400px; display: block;"></a>
Vamos aggrandir le photo del ospite...
<a href="http://www.mardy.it/archivos/imagines/CattoSimia.jpg"><img src="http://www.mardy.it/archivos/imagines/CattoSimia.jpg" style="padding: 0.5em; margin-left: auto; margin-right: auto; width: 200px; display: block;"></a>
Ah!!! Le <b>catto-simia</b>!!!

Illes qui vole vider mi jardin, con Pastiglio e le catto-simia, es benvenite <a href="http://www.mardy.it/galleria/v/Italia/jardin01/">in le galleria</a>.