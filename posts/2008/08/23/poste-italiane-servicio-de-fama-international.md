<!--
.. title: Poste Italiane: servicio de fama international
.. slug: poste-italiane-servicio-de-fama-international
.. date: 2008-08-23 00:00:00
.. tags: Italia,curiositates,actualitate
.. category: Italia
.. link: 
.. description: 
.. type: text
-->

Iste vespere io curiosava in Ebay, inter le multitude de offertas de objectivos photographic, sin un particular idea. Mi interesse esseva captivate per alicun lentes Zenitar, in quanto <em>Made in Russia</em>, e immediatemente io cliccava pro satisfacer mi curiositate e vider le detalios del offerta. Le venditor es un boteca electronic de Moskva, e quasi tote le material que illes ha es manufacite in Russia. Ma io sape que vos non es interessate in isto. Le ration pro le qual iste offerta es tanto interessante pro me (e le ration pro le qual io scribe iste lineas in loco de vader a dormir) es un nota in grande characteres rubie que es presente in tote le insertiones de iste venditor:
<div style="margin: 0pt auto; padding: 0.5em; text-align: center;"><img src="http://www.mardy.it/archivos/imagines/banner_top_english.jpg"></div>
Sin emphatisar le errores grammatical del nota, io lo traduce pro illes qui non comprende anglese: &ldquo;<i>Attention! <b>Nos non face expeditiones a Italia.</b> Il ha grande retardos e multe paccos ha essite perdite. Tote le compras per compratores italian essera cancellate. Nos spera que vos comprendara</i>&rdquo;. Io memora que ante alicun tempore io trovava annuncios simile in Ebay, ubi on diceva que on non expediva a Italia e China. Ma ora, nos ha remanite sol. <b>Le sol pais in le mundo al qual le russos non invia paccos es Italia</b>, perque nostre servicio postal non es considerate efficiente e digne de confidentia. Isto pote apparer sensational pro illes qui non ha vivite in Italia; ma pro nos, ubi es le surprisa?