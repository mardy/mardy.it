<!--
.. title: Incontro nordic
.. slug: incontro-nordic
.. date: 2008-08-29 00:00:00
.. tags: amicos,turismo,cultura,Svedia,Linköping
.. category: amicos
.. link: 
.. description: 
.. type: text
-->

<div style="float:right; padding-left:1em; padding-bottom:1em"><a href="http://www.mardy.it/galleria/v/eventos/IncontroNordic2008/dsc03227.jpg.html"><img src="http://www.mardy.it/galleria/d/6530-2/dsc03227.jpg" width="300px"></a></div>
<p>Le incontro nordic de interlingua es un evento biennal que ha loco in un pais scandinave, usualmente in Svedia viste que le costo del vita non es exaggeratemente alte e que il ha plus interlinguistas active. Io participava nonobstante que io non es propriemente un nordico, e trovava que io non esseva le sol <i>participante abusive</i> del gruppo: il habeva altere <i>spiones</i> de Germania, Hungaria, Francia, Grande Britannia e, curiosemente, Japon.</p>
<p>Isto non esseva exactemente un conferentia, ma un reunion; in le prime tres dies il habeva cursos de interlingua pro comenciantes e cursos avantiate, que non faceva parte del incontro nordic ma esseva realisate pro facilitar le participation de tote interessatos, e pro unir duo eventos in le mesme occasion. Officialmente, le incontro durava solmente duo dies, initiante le postmeridie de venerdi e terminante le postmeridie de dominica. Io arrivava jovedi, in le tarde nocte <a href="http://www.mardy.it/ia/node/1314">pro causas que io non va repeter</a>, e ergo non participava al cursos de interlingua (isto explica perque io scribe tanto mal). Ma io poteva facer multe altere cosas interessante, per exemplo
<div style="padding: 0.5em; width: 400px; margin: 0 auto; text-align: center"><a href="http://www.mardy.it/galleria/v/eventos/IncontroNordic2008/dsc03203.jpg.html"><img src="http://www.mardy.it/galleria/d/6512-2/dsc03203.jpg" style="display:block; width: 400px;"></a></div>
mangiar fructos que se trovava in abundantia, visitar le universitate de matematica
<div style="padding: 0.5em; width: 400px; margin: 0 auto; text-align: center"><a href="http://www.mardy.it/galleria/v/eventos/IncontroNordic2008/dsc03249.jpg.html"><img src="http://www.mardy.it/galleria/d/6545-2/dsc03249.jpg" style="display:block; width: 400px;"></a></div>
e notar como iste studentes debe observar le regulationes le plus stricte e tyrannic,
<div style="padding: 0.5em; width: 400px; margin: 0 auto; text-align: center"><a href="http://www.mardy.it/galleria/v/eventos/IncontroNordic2008/dsc03268.jpg.html"><img src="http://www.mardy.it/galleria/d/6557-2/dsc03268.jpg" style="display:block; width: 400px;"></a></div>
spiar le organisatores durante que illes confabula,
<div style="padding: 0.5em; width: 400px; margin: 0 auto; text-align: center"><a href="http://www.mardy.it/galleria/v/eventos/IncontroNordic2008/dsc03280.jpg.html"><img src="http://www.mardy.it/galleria/d/6575-2/dsc03280.jpg" style="display:block; width: 400px;"></a></div>
o personas qui roba preciosissime libros in interlingua,
<div style="padding: 0.5em; width: 400px; margin: 0 auto; text-align: center"><a href="http://www.mardy.it/galleria/v/eventos/IncontroNordic2008/dsc03333.jpg.html"><img src="http://www.mardy.it/galleria/d/6605-2/dsc03333.jpg" style="display:block; width: 400px;"></a></div>
o curre pro facer un photo de gruppo con le tempore contate,
<div style="padding: 0.5em; width: 400px; margin: 0 auto; text-align: center"><a href="http://www.mardy.it/galleria/v/eventos/IncontroNordic2008/dsc03349.jpg.html"><img src="http://www.mardy.it/galleria/d/6623-2/dsc03349.jpg" style="display:block; width: 400px;"></a></div>
visitar villages phantasma, e pro finir
<div style="padding: 0.5em; width: 400px; margin: 0 auto; text-align: center"><a href="http://www.mardy.it/galleria/v/eventos/IncontroNordic2008/dsc03429.jpg.html"><img src="http://www.mardy.it/galleria/d/6686-2/dsc03429.jpg" style="display:block; width: 400px;"></a></div>
esser spectator de un concurso sportive pro conilios. Sin oblidar altere evenimentos importantissime, qual le intervista a me per parte de <a href="http://zalaegerszeg.blogspot.com/">Peter Kovacs</a> que ha revivificate <a href="http://www.interlingua.com/radio">Radio Interlingua</a>, <a href="http://www.mardy.it/galleria/v/eventos/IncontroNordic2008/dsc03320.jpg.html">le bon surprisa de Ingvar Stenström</a> (un del veteranos &mdash; o melio, <i>juvene</i> veteranos &mdash; de interlingua) quando on le ha conseniate un copia de su libro in anteprima (le libro es probabilemente in phase de publication in iste dies), e altere cosas plus o minus interessante que vos pote certemente trovar si vos <a href="http://www.mardy.it/galleria/v/eventos/IncontroNordic2008">visita le galleria</a>.
