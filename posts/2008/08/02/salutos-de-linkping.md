<!--
.. title: Salutos de Linköping
.. slug: salutos-de-linkping
.. date: 2008-08-02 00:00:00
.. tags: actualitate
.. category: actualitate
.. link: 
.. description: 
.. type: text
-->

Io es currentemente in <a href="http://www.linkoping.se/index.htm">Linköping</a>, le citate del Svedia que ha essite seligite como sito pro le incontro nordic de <a href="http://www.interlingua.com">interlingua</a>. Si, apparentemente io anque ha devenite un nordico &mdash; le cosa me disturba un poco, ma le dolor es tolerabile.
Le incontro termina dominica, ma pro vider alicun photos de illo vos debera attender plus tempore, nam mi vacationes va durar tres septimanas (ma io non va ancora revelar qual es le destination successive, pro crear suspance).
Io spera que le volo de retorno de Linköping essera minus travaliate que illo de vadita: illo esseva retardate de cinque minutas, postea de dece, postea de ancora cinque minutas, postea illo esseva cancellate e nos esseva promittite un solution alternative, infin (o quasi) illo esseva restaurate e schedulate pro le vespere, ma tosto retardate ancora, e finalmente (io promitte que isto es le fin) illo partiva a 21:15, con un retardo de quasi cinque horas in respoecto al schedulation originari.
Ma io es patiente.