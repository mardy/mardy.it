<!--
.. title: (in)Faustus!
.. slug: infaustus
.. date: 2008-04-18 00:00:00
.. tags: Helsinki,cultura
.. category: Helsinki
.. link: 
.. description: 
.. type: text
-->

<a href="http://www.mardy.it/archivos/imagines/faustus.png"><img src="http://www.mardy.it/archivos/imagines/faustus.png" style="padding: 0.5em; margin-left: auto; margin-right: auto; width: "400px"; display: block;"></a>
Isto es le manifesto del representation de nostre tragedia, Faustus, extrahite del opera <a href="http://en.wikipedia.org/wiki/The_Tragical_History_of_Doctor_Faustus">The tragical history of Doctor Faustus</a> per Christopher Marlowe. Ancora il non es clar si nos va representar un tragedia, o si nostre representation essera un tragedia.
In omne caso, illes qui se trova in Helsinki in le 9 de majo e vole passar duo horas in maniera differente, pote reservar un sedia per scriber a <a href="mailto:helsinkitheatre@gmail.com">helsinkitheatre@gmail.com</a> (in anglese!). Le gruppo se ha formate in septembre 2007, e nos ha initiate a practicar iste <em>tragedia</em> del initio de iste anno, quasi cata septimana. Le recitation essera in anglese, pronunciate con varie accentos differente a causa del differente provenientia del actores. Io parlara in anglese con accento interlinguista. ;-)