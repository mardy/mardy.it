<!--
.. title: Il confronto Veltroni - Berlusconi
.. slug: il-confronto-veltroni-berlusconi
.. date: 2008-04-10 00:00:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
-->

(io scribe in italiano perque io dubita que non-italianos es interessate in isto!)

Diffondo un video tratto dalla trasmissione Annozero di qualche tempo fa, in cui il giornalista indipendente (uno dei pochi!) <a href="http://voglioscendere.ilcannocchiale.it/">Marco Travaglio</a> lamenta l'assenza di un confronto tra i leader dei due maggiori partiti italiani:
<center><object width="425" height="355"><param name="movie" value="http://www.youtube.com/v/hIx__n_wyxM&hl=it"></param><param name="wmode" value="transparent"></param><embed src="http://www.youtube.com/v/hIx__n_wyxM&hl=it" type="application/x-shockwave-flash" wmode="transparent" width="425" height="355"></embed></object></center>
Restando in tema di elezioni (io ho già votato qualche giorno fa), presento un altro video tratto dalla stessa trasmissione in cui Travaglio parla delle liste dei candidati alle camere, togliendoci ogni dubbio circa l'onorabilità dei futuri onorevoli:
<center><object width="425" height="355"><param name="movie" value="http://www.youtube.com/v/IjQ5ygQJ_Vg&hl=it"></param><param name="wmode" value="transparent"></param><embed src="http://www.youtube.com/v/IjQ5ygQJ_Vg&hl=it" type="application/x-shockwave-flash" wmode="transparent" width="425" height="355"></embed></object></center>
Quindi, per concludere, pubblicità progresso, per ricordare qual è <b>l'unico partito pulito</b>:
<center><a href="http://www.antoniodipietro.com/2008/04/lunico_voto_valido_1.html" target="_blank"> <img src="http://www.antoniodipietro.com/immagini2/bannervotovalido.jpg" border="0"></a></center>