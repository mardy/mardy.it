<!--
.. title: San Petroburgo, parte 3
.. slug: san-petroburgo-parte-3
.. date: 2008-07-11 00:00:00
.. tags: turismo,Sancte Petroburgo
.. category: turismo
.. link: 
.. description: 
.. type: text
-->

Un ecclesia que multo me place exteriormente, ma que io non visitava internamente, es illo del Salvator super le Sanguine Versate:
<div style="padding: 0.5em; width: 400px; margin: 0 auto; text-align: center"><img src="http://www.mardy.it/galleria/d/5619-2/img_2150.jpg" style="display:block; width: 400px;"><i>Ecclesia del Salvator sur le Sanguine Versate<br>Le cupola plus alte pare pertiner a un gelato</i></div>
Le die sequente, dominica, nos habeva in programma le visita al fortalessa de Petro e Paolo, ma un pluvia insistente nos discoragiava, e in ultra nos non habeva sufficiente energias pro saltar super le pontes altiate que duce al insula ubi le fortalessa se trova. Le pontes normalmente non es altiate durante le die, ma solmente in le nocte; totevia, ille die esseva un die festive in Russia (le festa del familia, si io non erra), e il habeva alicun celebrationes que imponeva le apertura del pontes:
<div style="padding: 0.5em; width: 400px; margin: 0 auto; text-align: center"><img src="http://www.mardy.it/galleria/d/5649-2/img_2205.jpg" style="display:block; width: 400px;"><i>Io voleva saltar, ma le policia me stoppava &mdash; homines de poca fide!</i></div>
In le postmeridie le tempore se ameliorava, e nos lo passava in un parco de amusamentos. Io probava alicun attractiones extreme, le plus &ldquo;forte&rdquo; del quales esseva iste (io sape que illo pare quasi innocue, ma on debe probar lo pro comprender!):
<div style="padding: 0.5em; width: 400px; margin: 0 auto; text-align: center"><img src="http://www.mardy.it/galleria/d/5661-2/img_2229.jpg" style="display:block; width: 400px;"><i>Io voleva saltar, ma le policia me stoppava &mdash; homines de poca fide!</i></div>
Con isto, se completa le narration de lo que io ha vidite in San Petroburgo. Ma si alicuno me demandava qual es le cosa que plus me ha impressionate in iste viage, io responderea que illo non es in iste lista. De facto, illo non es in San Petroburgo, ma in un village vicin, del qual io non pote dicer le nomine (ma que le plus attente de vos trovara facilemente <a href="http://www.mardy.it/galleria/v/Russia/Spb20080705">in le galleria</a>):
<div style="padding: 0.5em; width: 400px; margin: 0 auto; text-align: center"><img src="http://www.mardy.it/galleria/d/5674-2/img_2162.jpg" style="display:block; width: 400px;"><i>Un ridente village, de location incognite</i></div>
Yulia non voleva que io prendeva iste photos, e non vole que io dice que iste village se trova in Russia; pro iste ration, io non va dicer lo. Ma io debe absolutemente dicer que, ben que le exterior del casas e del palacios da un impression miserabile, le interior (al minus lo que io ha vidite) es multo plus dignitose del interior de mi appartamento in Helsinki que, como mi visitatores sape, jam es excellente.
Le elementos plus importante de iste village es, in ordine crescente, un statua de Lenin,
<div style="padding: 0.5em; width: 400px; margin: 0 auto; text-align: center"><img src="http://www.mardy.it/galleria/d/5696-2/img_2176.jpg" style="display:block; width: 400px;"><i>Ille es identic!</i></div>
le hospital,
<div style="padding: 0.5em; width: 400px; margin: 0 auto; text-align: center"><img src="http://www.mardy.it/galleria/d/5705-2/img_2182.jpg" style="display:block; width: 400px;"><i>Io ha resistite a un tentativa de censura</i></div>
e, sin umbra de dubios, le theatro:
<div style="padding: 0.5em; width: 400px; margin: 0 auto; text-align: center"><img src="http://www.mardy.it/galleria/d/5684-2/img_2168.jpg" style="display:block; width: 400px;"><i>Faustus ha essite hic!</i></div>
Vos debe excusar mi senso de humorismo tanto academic. De facto, io esseva fortemente attracte per iste village; le conditiones del infrastructuras public, evidentemente negligite per le administratores del municipalitate, contrasta con le bontate e le spirito de initiativa del personas qui vive in le area: mi impression es lo de un loco que le incurantia vole oblidar, e que le inhabitantes vole resurger. Per isto, io experiva admiration pro ille gentes qui vive, labora e ride in lor pais.
Oh, ma io non ha ancora scribite qual es le recordo plus importante del viage:
<div style="padding: 0.5em; width: 400px; margin: 0 auto; text-align: center"><img src="http://www.mardy.it/galleria/d/5687-2/img_2169.jpg" style="display:block; width: 400px;"><i>Le tubos!</i></div>
Jam on los poteva vider in le photo precedente: le tubos es ubicunque in le village. Il ha passerellas pro le pedones que los superpassa, e pro consentir le passage del carros le tubos se altia del terreno e forma portales multo characteristic. On me diceva que solmente in ille village il ha iste situation. Le question successive es: que passa in iste tubos? Aqua calide. Si, isto es le responsa. Io non comprende como isto pote functionar durante le hiberno, e quando io ha demandate explicationes, on me ha respondite que, quando le tubos ha essite placiate in le village, isto esseva le solution plus economicamente conveniente. Io imagina que le convenientia de ille momento ha essite destruhite per le expensas del dispersion del calor in le annos sequente, ma como sempre eveni, le solutiones temporari es le plus permanente (si io non erra, isto es anque un expression popular russe).
Io spera que, in un futuro proxime, le administration municipal construe tubos sub le terreno; ma sin remover le tubos existente, assi que multe altere touristas pote passar sub illos.
