<!--
.. title: San Petroburgo, parte 2
.. slug: san-petroburgo-parte-2
.. date: 2008-07-10 00:00:00
.. tags: turismo,Sancte Petroburgo
.. category: turismo
.. link: 
.. description: 
.. type: text
-->

Multe photos in le galleria es prendite durante un tour in barca, con un voce que commentava e narrava le historia del locos e del monumentos. In russo, ma fortunatemente mi interprete personal me traduceva le informationes essential pro le superviventia.
<div style="padding: 0.5em; width: 600px; margin: 0 auto; text-align: center"><img src="http://www.mardy.it/galleria/d/5508-2/img_2038.jpg" style="display:block; width: 600px;"><i>Le Palacio de Hiberno, quasi indistinguibile del acqua e del celo</i></div>
Infortunatemente le faciada principal del Palacio de Hiberno, lo que se monstra del placia (appellate originalmente &ldquo;le placia del Palacio&rdquo;) esseva sub manutention, coperite per structuras metallic e panellos touristicamente non multo appreciabile, e pro iste ration illo es absente del album photographic. Pro compensar iste mancantia, vos pote gauder del vision de iste palacio:
<div style="padding: 0.5em; width: 400px; margin: 0 auto; text-align: center"><img src="http://www.mardy.it/galleria/d/5499-2/img_2031.jpg" style="display:block; width: 400px;"><i>In honor de mi fratre, un rarissime exemplar de </i>ambassada japonese</div>
Postea, nos ha admirate le cruciator Aurora (o &ldquo;Avrora&rdquo;, con le pronunciation russe), del qual io ha legite le historia ante duo minutos, <a href="http://it.wikipedia.org/wiki/Aurora_(incrociatore)">in Wikipedia</a> (le mesme pagina es disponibile in varie linguas, estraniemente non interlingua), e pote assi narrar vos que illo esseva &ldquo;baptisate&rdquo; in le majo del 1900, e ergo es optimisticamente plus vetule que le major parte de vos.
<div style="padding: 0.5em; width: 400px; margin: 0 auto; text-align: center"><img src="http://www.mardy.it/galleria/d/5526-2/img_2057.jpg" style="display:block; width: 400px;"><i>Le cruciator Aurora, actualmente un museo</i></div>
In continuar le percurso in barca, nos navigava proxime al Jardin de Estate, anque isto appellate con multe originalitate. Ma isto nos consenti al minus de saper que le Czar russe passava su hibernos in un palacio, e su estates in un jardin.
Al termino del excursion nautic, nos promenava per le centro del citate, e un del attractiones plus remarcabile es securmente iste,
<div style="padding: 0.5em; width: 400px; margin: 0 auto; text-align: center"><img src="http://www.mardy.it/galleria/d/5535-2/img_2062.jpg" style="display:block; width: 400px;"><i>le museo del chocolate!</i></div>
Nos non lo visitava, habente le suspecto que un barra de chocolate plus vetule que 50 annos non es probabilemente bon a mangiar, ma supertoto io crede que <i>si, 50 annos retro, altere personas non lo mangiava, il debeva haber un motivo</i>. Un altere cosa curiose se trovava in le placia del Palacio:
<div style="padding: 0.5em; width: 400px; margin: 0 auto; text-align: center"><img src="http://www.mardy.it/galleria/d/5553-2/img_2070.jpg" style="display:block; width: 400px;"><i>Un simplice autobus?</i></div>
Nonobstante le apparentias de un autobus ordinari, si on reguarda attentivemente le pictura on pote leger &ldquo;WC&rdquo;; iste autobus es parcate in le placia como WC mobile e estheticamente non horribile. O forsan illo <i>es</i> un bus totalmente ordinari, ma le autista ha oblidate de clauder le portas, e le publico con urgentias los usa pro exercitar lor besonios primari.
De un cathedral multo alte io ha prendite alicun photos panoramic del citate, ma necun de los es digne del publication in prime pagina. Nos ha continuate le promenada in un parco, ubi il habeva plure festeamentos de matrimonios, principalmente a scopo photographic (al minus isto es mi impression). Oh, io quasi oblidava de celebrar le technologia futuristic de San Petroburgo! Vide isto:
<div style="padding: 0.5em; width: 400px; margin: 0 auto; text-align: center"><img src="http://www.mardy.it/galleria/d/5604-2/img_2142.jpg" style="display:block; width: 400px;"><i>Ancora venti secundos</i></div>
In alicun lumines pro le traffico pedonal il ha le indication de quante secundos on debe ancora attender, si le signal es rubie, o de quante tempore on ha a disposition pro passar, si illo es verde. Io dubita que isto va altiar considerabilemente le nivello qualitative del vita del citatanos, ma il es un cosa que io non memora de haber vidite in ulle altere loco. Del altere latere, le indicationes temporal que il ha in le stationes del metro es, in mi opinion, totalmente inutile: illos monstra le tempore passate del ultime traino o, in altere parolas, quante secundos ha passate desde le arrivata del precedente traino. Pro abbatter ulteriormente le utilitate de iste indication, on me ha dicite que le trainos non sempre passa con le mesme frequentia, assi que le cognocentia del tempore passate del ultime traino pote esser utile solmente pro lamentar se con expressiones simile a iste: &ldquo;Io ha mancate mi traino pro solmente X minutos e Y secundos!&rdquo;.

<center>
Fin del parte 2
<i>Io va scriber le resto in un futuro insperabilemente proxime</i>
Per ora, continua a reguardar le photos <a href="http://www.mardy.it/galleria/v/Russia/Spb20080705/">in le galleria!</a>
</center>
