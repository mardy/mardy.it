<!--
.. title: Attende...
.. slug: attende
.. date: 2008-07-07 00:00:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
-->

<div style="float:right">
<a href="http://www.mardy.it/galleria/v/Finlandia/Pellicula2/"><img src="http://www.mardy.it/galleria/d/5454-2/img002-018.jpg" width="300px"></a>
</div><p>Le actualisation de mi blog, que jam esseva infrequente, in le ultime tempores es ancora plus critic, quasi inexistente. E le cosa le plus grave es que io non saperea dicer qual es le rationes de isto.
Un ration poterea esser le aquisition de un <a href="http://www.wii.com">Nintendo Wii</a>, accompaniate per Mario Kart (si tu anque lo ha, e vole excambiar le codice, <a href="mailto:mardy78@yahoo.it">scribe me</a>), que es un passatempore irresistibile; o forsan le passion photographic, viste que multe de mi recente invios concerne le photographia; o forsan... un femina? No, isto non pote esser!
Qualcunque sia le ration, io promitte solemnemente que in breve tempore io devenira diligente e vos informara super le recentissime (e brevissime) viage a San Petroburgo (ubi io passava mi fin de setimana, e ha retornate heri nocte).</p>
<p>Pro calmar vostre ardente expectation, io vos contenta con le exhibition in le galleria de mi <a href="http://www.mardy.it/galleria/v/Finlandia/Pellicula2/">secunde pellicula 35mm</a>. Iste vice io ha demandate al boteca photographic de dar me le photos in version digitalisate, in un CD; le costo total (developpamento plus scannerisation) es de 13 euros, e io ha essite multo satisfacite per le resultato.</p>