<!--
.. title: Pin Ion
.. slug: pin-ion
.. date: 2008-07-31 00:00:00
.. tags: amicos,Helsinki,cultura
.. category: amicos
.. link: 
.. description: 
.. type: text
-->

<div style="float:right; padding-left:1em; padding-bottom:1em"><a href="http://www.mardy.it/galleria/v/eventos/pinion"><img src="http://www.mardy.it/galleria/d/6468-2/dsc03159.jpg" width="300px"></a></div>
<a href="http://www.pinion.fi">Pin Ion</a> es un banda finlandese qui sona musica rock. Io cognosce le cantante del gruppo, H-P (istos es le initiales del nomine, que remane un secreto non revelabile), que es anque le marito del arrangiator artistic de nostre gruppo theatral.
Heri le cinque <i>pinniones</i> sonava in le bar <a href="http://www.texasco.fi/">Texas</a>, que nonobstante le nomine se trova in le centro de Helsinki, e io vadeva con alicun amicos a audir lor musica. Alicun cantos esseva in finlandese, ma le major parte esseva in anglese; totevia isto non significa que io poteva comprender lor texto, que esseva difficilemente audibile (ma isto es un &ldquo;problema&rdquo; commun al cantos rock, especialmente quando illos es sonate a alte volume in un parve local). Ma le musicas esseva appreciabile e postea audir los in le bar io ha discargate alicun mp3-es del mesme cantos del sito del Pin Ion-es e los trova multo belle.

Io ha prendite le occasion pro facer alicun photos del evento, que vos pote &ldquo;admirar&rdquo; <a href="http://www.mardy.it/galleria/v/eventos/pinion/">in le galleria</a>.