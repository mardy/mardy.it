<!--
.. title: San Petroburgo, parte 1
.. slug: san-petroburgo-parte-1
.. date: 2008-07-09 00:00:00
.. tags: turismo,Sancte Petroburgo
.. category: turismo
.. link: 
.. description: 
.. type: text
-->

Como promittite, io va scriber super mi viage a San Petroburgo, realisate le fin de septimana passate; si, solmente duo dies, sabbato e dominica, in expectation de vader pro un periodo plus longe in augusto.
Pro initiar, io presenta le medio de transporto:
<div style="padding: 0.5em; width: 400px; margin: 0 auto; text-align: center"><img src="http://www.mardy.it/galleria/d/5467-2/img_2017.jpg" style="display:block; width: 400px;"><i>Ford Transit!</i></div>
Le partentia esseva fixate al horas 20:00, ma esseva posticipate de circa medie hora a causa de alicun viagiatores in retardo; evidentemente le compania del mini-bus prefere attender medie hora que perder 25 euros de billetos (si, isto es le precio!). E isto pare esser un situation bastante normal, perque necuno protestava o se lamentava. Le viage ha requirite circa 7 horas, ma gran parte de iste tempore esseva empleate pro recuperar alicun viagiatores que se trovava in varie partes del Finlandia (fortunatemente non troppo distante de Helsinki), como Vantaa, e, al conclusion del viage, pro portar a varie addresses specific de San Petroburgo les qui lo demandava (fortunatemente le possibilitate non es extendite al complete territorio del Russia). Alteremente, io crede que le viage haberea requirite solmente 5 horas, viste anque que nos esseva multo fortunate in le passage del confinio (il non habeva quasi ulle persona ultra a nos).

Al matino, quando io ha arrivate a destination, mi amica Yulia me attendeva con su patre, pro portar me a casa. <em>No, io non va explicar qui es Yulia!</em> Per le automobile, isto requireva non plus que 20 minutos, durante que quando in le sequente dies nos refaceva le mesme percurso con medios public (bus plus metro), le tempores se dilatava, e le transporto prendeva circa trenta o quaranta minutos. A iste proposito, ante mi viage un amica russa me diceva que alicun turistas italian qui visitava San Petroburgo esseva multo impressionate per le profonditate del metro; e io confirma lor impression: le tempore que prende le scalas mobile pro vader a basso es circa 3 o 4 minutos, dependente del station. Il ha 4 lineas de metro, a nivellos differerente.
<div style="padding: 0.5em; width: 400px; margin: 0 auto; text-align: center"><img src="http://www.mardy.it/galleria/d/5472-2/img_2018.jpg" style="display:block; width: 400px;"><i>Un station del metro &mdash; Площадь Александра Невского (placia de Alexander Nevsky)</i></div>
Iste photo del metro me ha costate le bellessa de 100 rublos perque immediatemente post que io lo ha prendite, un officiero de policia me ha communicate que il es prohibite prender photos del stationes, como mesura anti-terroristic. Ridicule, nonne? Excludente le facto que le photos del stationes es presente in abundante quantitate in le internet, io dubita que terroristas essera fermate per iste prohibition, sia perque illes poterea facilemente prender photos sin que le policiero les vide, sia perque le policiero es quasi sempre absente e sia perque probabilemente illes non essera espaventate per le pagamento de iste grandissime summa de danaro. Mmm... attende... quante euros vale 100 rublos? Circa 3. (Tres, TRES, <b>TRES</b>). Ah.

<center>
Fin del parte 1
<i>Io va scriber le resto in un futuro sperabilemente proxime</i>
Per ora, reguarda le photos <a href="http://www.mardy.it/galleria/v/Russia/Spb20080705/">in le galleria!</a>
</center>
