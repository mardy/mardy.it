<!--
.. title: Le presidente del republica italian ignora le constitution?
.. slug: le-presidente-del-republica-italian-ignora-le-constitution
.. date: 2008-07-24 00:00:00
.. tags: politica,Italia
.. category: politica
.. link: 
.. description: 
.. type: text
-->

<small><i>(pro le lectores qui lege iste blog pro le prime vice: le lingua usate es <a href="http://www.interlingua.com">interlingua</a>, un lingua international immediatemente comprensibile per milliones de personas sin necessitate de studio previe)</i></small>

Giorgio Napolitano, le actual presidente italian, ha heri (23 Julio 2008) firmate un importante lege, technicamente appellate &ldquo;lodo Alfano&rdquo; (e non technicamente appellate &ldquo;lege porcada&rdquo;), que dona le immunitate juridic total al quatro plus importante cargas politic pro le exercitio de lor mission: le presidente del republica, le prime ministro (qui actualmente es le sempre-innocente Silvio Berlusconi), e le presidentes del duo cameras parlamentari.
Iste lege, que esseva necessari pro ridiculisar le democratia italian, pare esser in conflicto con le tertie articulo del constitution italian, que dice: <i>&ldquo;Tote citatanos [...] es equal in fronte al lege, sin distinction de sexo, racia, lingua, religion, opiniones politic, conditiones personal e social. [...]&rdquo;</i>. Le &ldquo;lodo Alfano&rdquo; es un lege ordinari, ergo illo non pote contradicer o modificar lo que es scripte in le constitution (solmente un lege constitutional pote limitar un lege constitutional); ma alora, como conciliar le duo cosas?
Le conclusion es simple: si tote citatanos es equal in fronte al lege, ma le presidente del republica e su amicos intime non lo es, isto significa que <em>iste quatro personas non es citatanos italian</em>. Le presidente del republica italian, durante le periodo de su carga, non es un citatano italian. Esque ille va restituer su passaporto? In omne caso, le italianos es governate per quatro refugiatos, que ha un altissime probabilitate de esser criminal: de facto, benque le impunitate dura solmente pro le periodo de lor mission, le facto que illes es vetule e le lentessa del justitia italian es quasi un garantia que illes nunquam pagara pro lor crimines &mdash; que illes pote committer apertemente.
Que pote facer le citatanos italian pro stoppar iste &ldquo;banda del quatros&rdquo;, como le popular humorista <a href="http://www.beppegrillo.it">Beppe Grillo</a> los appella? Si le justitia non ha poter super illes, le unic maniera es eliminar les physicamente, ma le lege non lo consenti. Alora, on pote solmente attender, e sperar que le natura face su corso rapidemente, ante que illes committe crimines irreparabile. Deo, salva nos!

<center><object width="425" height="344"><param name="movie" value="http://www.youtube.com/v/3GICsf4V6a8&hl=en&fs=1"></param><param name="allowFullScreen" value="true"></param><embed src="http://www.youtube.com/v/3GICsf4V6a8&hl=en&fs=1" type="application/x-shockwave-flash" allowfullscreen="true" width="425" height="344"></embed></object>
Mi dedica al membros del banda del quatro</center>
<center><object width="425" height="344"><param name="movie" value="http://www.youtube.com/v/2sNxqXJZZYs&hl=en&fs=1"></param><param name="allowFullScreen" value="true"></param><embed src="http://www.youtube.com/v/2sNxqXJZZYs&hl=en&fs=1" type="application/x-shockwave-flash" allowfullscreen="true" width="425" height="344"></embed></object></center>