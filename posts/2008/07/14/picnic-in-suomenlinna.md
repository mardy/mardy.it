<!--
.. title: Picnic in Suomenlinna
.. slug: picnic-in-suomenlinna
.. date: 2008-07-14 00:00:00
.. tags: amicos,Helsinki
.. category: amicos
.. link: 
.. description: 
.. type: text
-->

In compania de Lassi e mi hospites de Taiwan, Yiju e June, nos ha vadite a Suomenlinna pro facer un picnic. Le photos es <a href="http://www.mardy.it/galleria/v/Finlandia/Suomenlinna/Picnic/">in le galleria</a>, ubi on pote admirar un version magnificate de iste <em>juvene</em> estimator de Batman,
<div style="padding: 0.5em; width: 400px; margin: 0 auto; text-align: center"><img src="http://www.mardy.it/galleria/d/5822-2/dsc02393.jpg" style="display:block; width: 400px;"></div>
un incredibile collision de un nave contra le insula,
<div style="padding: 0.5em; width: 400px; margin: 0 auto; text-align: center"><img src="http://www.mardy.it/galleria/d/5841-2/dsc02416.jpg" style="display:block; width: 400px;"></div>
(pro illes qui vole creder lo) e un ave &mdash; non me demanda le nomine, io es ignorante &mdash; que ha girate su capite de 360 + 180 = 540 grados:
<div style="padding: 0.5em; width: 400px; margin: 0 auto; text-align: center"><img src="http://www.mardy.it/galleria/d/5862-2/dsc02503.jpg" style="display:block; width: 400px;"></div>
Obviemente in le galleria il ha photos multo plus belle de istes, ma io es troppo timide e modeste pro monstrar lo al prime pagina. Va e admira los in contemplation!

Oh, io debe commentar anque iste photo, alteremente on non comprende perque io lo poneva in le galleria:
<div style="padding: 0.5em; width: 400px; margin: 0 auto; text-align: center"><img src="http://www.mardy.it/galleria/d/5859-2/dsc02497.jpg" style="display:block; width: 400px;"></div>
Isto es le folla que voleva prender le barca pro retornar a Helsinki: multo plus de quanto le barca (que es comocunque multo grande) pote portar. Alora un parte de illes ha remanite in Suomenlinna pro altere 20 minutos, a attender le barca successive.
Si on considera le position del photographo, il non es difficile imaginar que ille face parte del secunde gruppo...