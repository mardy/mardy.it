<!--
.. title: Photos de Tallinn
.. slug: photos-de-tallinn
.. date: 2008-02-10 00:00:00
.. tags: turismo
.. category: turismo
.. link: 
.. description: 
.. type: text
-->

Io ha publicate in le galleria alicun <a href="http://www.mardy.it/galleria/v/Estonia">photos de Tallinn</a>. Como vos pote vider, io ha seligite de publicar los in le nove galleria. Io non es ancora completemente practic de iste nove systema, ergo le configuration que io ha seligite es probabilemente imperfecte (e non definitive); si vos trova alicun errores o incommoditates, per favor <a mailto:mardy78@yahoo.it>signala me los</a>. Pro le album del Estonia, io ha experimentalmente activate le function de <i>rating</i> del photos, assi que il es possibile votar pro le varie photos (le voto va de 1 a 5 stellas). Il ha anque un function de <i>slideshow</i>: si on clicca on le parve symbolo <img src="http://mardy.it/galleria/themes/carbon/images/slideshow.gif"> que se trova in alto a sinistra de omne album on videra le imagines del album in sequentia (on pote cambiar le tempore de monstra e le dimension del picturas).
Il ha altere cosas que on pote adder o cambiar &mdash; le tempore me portara consilios.

Pro restar in thema de photographia, io ha decidite de comprar un photocamera reflex del serie <a href="http://www.sony.net/Products/dslr/">Sony Alpha</a>, uno inter le A200, A300 e A350; illos essera disponibile in le botecas in circa un mense, ergo io ha un pauco de tempore pro decider (ma quasi certamente mi selection essera le A200). Mi ideas es minus clar in respecto al objectivos; certemente io va comprar le <a href="http://www.sony.net/Products/dslr/lenses_sample.html?SAL1870">DT 18-70mm</a>, ma ultra a isto io non sape. Il ha multe objectivos possibile, ma le precios non es sempre amicabile... :-)