<!--
.. title: Tabula rasa
.. slug: tabula-rasa
.. date: 2008-02-06 00:00:00
.. tags: arte,Helsinki
.. category: arte
.. link: 
.. description: 
.. type: text
-->

Post le labores estive de renovamento de mi appartamento, io me promitteva que io non haberea plus laborate pro al minus altere sex menses. Io ha mantenite le promissa &mdash; sin grande effortio, io admitte &mdash; e hodie io retorna con un altere promissa: io non va laborar in mi appartamento pro al minus altere duo menses. Eh, si, io es de novo fatigate per un labor enorme. Reguarda iste photo:
<a href="http://www.mardy.it/archivos/imagines/img_1966.jpg"><img src="http://www.mardy.it/archivos/imagines/img_1966.jpg" width="400px" style="padding: 0.5em; margin-left: auto; margin-right: auto; display: block;"></a>
Le furno a microundas non es in le pictura; ma illo sta usualmente sur le parve tabula a sinistra, e a pena il ha spatio sufficiente pro illo. Ma le problema plus grande es quando io debe extraher le platto del furno: io poner lo in un altere tabula, si io rememora de liberar un pauco de spatio. Alora, io ha dedicate iste fin de septimana a trovar un solution e, apparentemente, lo ha trovate:
<a href="http://www.mardy.it/archivos/imagines/img_1976.jpg"><img src="http://www.mardy.it/archivos/imagines/img_1976.jpg" width="400px" style="padding: 0.5em; margin-left: auto; margin-right: auto; display: block;"></a>
Il non ha essite facile fixar le tabula al paretes: le foration del muros esseva un interprisa al limite del capacitates de mi perforator, e in alicun punctos le vites non habeva un bon prisa &mdash; e de facto, illos ancora non lo ha. Isto me lassa alicun dubitas super le stabilitate de mi opera; al minus, il pare que illo pote sustener le furno a microundas, ma io non va adventurar me in poner multe altere cosas.
Le tabula es blanc, ma io va cercar un copertura tal que lo rende simile al altere mobiles del cocina. Ma anque si io lo trova, io non va installar lo ante duo menses! :-)