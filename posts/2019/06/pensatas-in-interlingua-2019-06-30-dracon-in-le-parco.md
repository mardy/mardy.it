<!--
.. title: Pensatas in interlingua - 2019-06-30 - Dracon in le parco
.. slug: pensatas-in-interlingua-2019-06-30-dracon-in-le-parco
.. date: 2019-06-30 19:14:43 UTC+03:00
.. tags: interlingua,video,curiositates,arte,Russia,Sancte Petroburgo
.. category: 
.. link: 
.. description: 
.. type: text
-->

<center>
<iframe width="560" height="315" src="https://www.youtube.com/embed/AYe6wGRh_Gk?rel=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</center>

Quando vos imagina un cosa que ha nulle senso, e vos es secur que iste cosa non
pote exister in ulle parte del mundo, il ha un bon probabilitate de trovar lo
in Russia.
