<!--
.. title: WIP: changing the backend for contacts in Ubports
.. slug: wip-changing-the-backend-for-contacts-in-ubports
.. date: 2019-06-13 17:59:35 UTC+03:00
.. tags: english,Ubuntu,Ubports,informatica,planetmaemo
.. category: 
.. link: 
.. description: 
.. type: text
-->

More than one year has passed since the [initial
announcement](https://list.ubports.com/pipermail/ubports-dev/2018-March/000005.html)
of my plan to investigate using a different backend for contact storage. If you
want to get a better understanding of the plan, that mail is still a good read
-- not much has changed since them, planning wise.

The reason for this blog post is to give a small update on what has happened
since then, and as a start nothing can be better than a couple of screenshots:

<div>
<figure style="float:left; margin-left: 3em; width: 340px">
  <img src="/archivos/imagines/blog/ubports-carddav-accounts.png" class="shadow-sm" />
  <figcaption>Adding CardDAV accounts in the Addressbook application</figcaption>
</figure>

<figure style="float:left; margin-left: 3em; width: 340px">
  <img src="/archivos/imagines/blog/ubports-carddav-contact.png" class="shadow-sm" />
  <figcaption>Aggregated contact details from multiple sources</figcaption>
</figure>
<div style="clear:both" />
</div>

In other words, that means that contact synchonisation works, both with the new
CardDAV protocol (for which we'll have preconfigured setups for NextCloud and
OwnCloud accounts) and with Google Contacts, for which we are now using a
different engine. What you see in the second screenshot (although indeed it's
not obvious at all) is that the new qtcontacts-sqlite backend performs
automatic contact merging based on some simple heuristics, meaning that when
synchonising the same contact from multiple sources you should not happen to
find a multitude of semi-identical copies of the contact, but a single one
having all the aggregated details.

Before you get too excited, I have to say that this code is pre-alpha quality
and that it's not even available for testing yet. The next step is indeed to
setup CI so that the packaggqes get automatically built and published to a public
repository, at which point I'll probably issue another update here in my blog.


The boring stuff
----------------

And now some detail for those who might wonder why this feature is not ready
yet, or would like to get an idea on the time-frame for its completion.

Apart from a chronical lack of time from my part, the feature complexity is due
to the large number of components involved:

* [qtcontacts-sqlite](https://git.sailfishos.org/mer-core/qtcontacts-sqlite):
  the QtContacts backend we are migrating to. This is a backend for the
  QtContacts API (used by our Addressbook application) which uses a SQLite
  database as storage for your contacts.
* [buteo-sync-plugin-carddav](https://git.sailfishos.org/mer-core/buteo-sync-plugin-carddav):
  the CardDAV plugin for Buteo (our synchronisation manager). This plugin is
  loaded by Buteo and synchronises the contacts between a CardDAV remote source
  and the `qtcontacts-sqlite` database.
* [buteo-sync-plugins-social](https://git.sailfishos.org/mer-core/buteo-sync-plugins-social):
  a Buteo plugin which can synchronise contacts from a multitude of sources,
  including Google, Facebook and Vk. At the moment we only care about Google,
  but once this feature has landed we can easily extend it to work with the
  other two as well.
* [address-book-app](https://github.com/ubports/address-book-app): this is our
  well-known Contacts application. It needs some minor changes to adapt to the
  `qtcontacts-sqlite` backend and to support the creation of new CardDAV,
  NextCloud and OwnCloud accounts.
* [QtPim](https://github.com/ubports/qtpim-opensource-src-packaging): the
  contacts and calendar API developed by the Qt project. Our Contacts
  application is using the front-end side of this API, and the
  `qtcontacts-sqlite` component implements the backend side. There are some
  improvements proposed by Jolla, which we need to include in order to support
  grouping contacts by their initials.

The other tricky aspect is that the first three projects are maintained by
Jolla as part of the [Sailfish OS](https://sailfishos.org/), and while on one
side this means that we can share the development and maintenance burden with
Jolla, on the other side of the coin it means that we need to apply extra care
when submitting changes, in order not to step on each other's shoes.
Specifically, Sailfish OS is using a much older version of `QtPim` than Ubports
is, and the APIs between the two versions have changes in an incompatible
version, so that it's nearly impossible to have a single code base working with
both versions of `QtPim`. Luckily `git` supports branches, and Chris from Jolla
was kind enough to create a branch for us in their upstream repository where
I've proposed our changes ([and they are a
lot!](https://git.sailfishos.org/mer-core/qtcontacts-sqlite/merge_requests/30)).

However, this is not as bad as it sounds, and the fact that I have a roughly
working version on my development device is a good sign that things are moving
forwards.
