<!--
.. title: Intervista al dottor Fabio Franchi sui vaccini
.. slug: intervista-al-dottor-fabio-franchi-sui-vaccini
.. date: 2019-06-26 22:02:25 UTC+03:00
.. tags: video,italiano,scientia
.. category: 
.. link: 
.. description: 
.. type: text
-->

Vi propongo questa intervista, realizzata da [Byoblu](http://www.byoblu.com), al dottor Fabio Franchi:

<center>
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/X2ORxdfbwZM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</center>

Si tratta di un punto di vista sui vaccini molto diverso da quello propagandato
dai mezzi di infomazione, con conclusioni sorprendenti ma ben argomentate.
Se avete un'oretta di tempo, spendetela per guardare questo video: non ve ne
pentirete.
