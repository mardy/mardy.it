<!--
.. title: Looking out for ideas: federated / aggregated content
.. slug: looking-out-for-ideas-federated-aggregated-content
.. date: 2019-07-17 17:55:04 UTC+03:00
.. tags: english,informatica,kdeplanet,planetmaemo,programmation,reflexiones,securitate
.. category: 
.. link: 
.. description: 
.. type: text
-->

I was looking at TripAdvisor, some days ago. It's a very useful site, filled
with user-generated advice and reviews, which has become almost a must for
travellers.  But I never like it when a private entity gets so much power over
our lives (even if it's — **currently** — exercised in total fairness).

I would like to have a *federated* TripAdvisor-like network. I duckduckwent for
a while, but I didn't find anything similar (if you know of some project of
that kind, please let me know in the comments).

But thinking more about the issue, I realised that I probably wouldn't even
bother to type my reviews into that site; I have a blog, so ideally I would
like to have the option to write my review here, and then have the site import
it. Technically, it could work with a webhook, or even a periodic check
(real-time updates would not be a requirement here) over a URL I've linked to
in my profile on that site. Then, in order for the posts to be imported, they
would have to be entered in a standard format: maybe some keywords (or
invisible HTML elements) could be used as markers for the content that need to
be extracted from an otherwise ordinary blog post, or the relevant content
could be replicated in a different format in the HTML headers (this, though,
would require some additional work).

And while we are at it, why not extend this to other social networks? I use
[Mastodon](https://soc.ialis.me/@mardy), for example, and occasionally I send
out a *toot* with a link to my latest blog post in there. But it would be much
nicer if I could somehow set a special mark into my posts while composing them,
to have them automatically *tooted* out on my account (this could probably be
implemented as a standalone service, authorized to act on my Mastodon account —
similarly to how the [Mastodon-Twitter crossposter
app](https://crossposter.masto.donte.com.br/) works).

But I'm rather confident that I'm not the only one having this kind of needs,
and that's why I'm writing this blog post: maybe someone out there has already
found a solution, or has some more concrete ideas? If so, I'm all eyes!
