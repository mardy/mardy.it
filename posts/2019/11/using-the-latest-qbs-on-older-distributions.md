<!--
.. title: Using the latest QBS on older distributions
.. slug: using-the-latest-qbs-on-older-distributions
.. date: 2019-11-17 21:48:08 UTC+03:00
.. tags: Linux,Qt,Ubports,Ubuntu,english,informatica,kdeplanet,programmation,AppImage
.. category: 
.. link: 
.. description: 
.. type: text
-->

A short announcement, probably meaningless to most people, but who knows: I've
created an Ubuntu [PPA with the latest
QBS](https://launchpad.net/~mardy/+archive/ubuntu/qbs-on-lts). The reason why
this might make some sense is that this PPA targets the older Ubuntu
distributions. It's currently built for 14.04 (Trusty, which is no longer
supported by Canonical), and I'll eventually upload the QBS package for 16.04,
too.

This package can be useful to people distributing applications in the [AppImage
format](https://appimage.org/), where one usually builds the application in one
older distribution in order to increase the chances of it being runnable in as
many Linux distributions as possible. A simpler way to obtain QBS on Trusty is
to install QtCreator, but that's not trivial to install in an automated way and
might not come with the latest QBS. Especially when building on Ubuntu, a
package from a PPA is much easier to install.

This QBS is built statically, and won't install any Qt libraries on your
system; this is good, because it allows you to use whatever Qt version you like
without any risk of conflicts.
