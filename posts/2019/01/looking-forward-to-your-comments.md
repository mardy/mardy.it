<!--
.. title: Looking forward to your comments
.. slug: looking-forward-to-your-comments
.. date: 2019-01-30 23:21:07 UTC+03:00
.. tags: english,kdeplanet,planetmaemo,programmation
.. category: 
.. link: 
.. description: 
.. type: text
-->

It took a few days, but I've finally migrated my site to
[Nikola](https://getnikola.com). I used to have `blog.mardy.it`
served by [Google's Blogger](https://blogger.com), the main sections of
`www.mardy.it` generated with [Jekyll](https://jekyllrb.com/), the image
gallery served by the old and glorious [Gallery2](http://galleryproject.org/),
plus a few leftovers from the old Drupal site.

<center>
<a href="https://flic.kr/p/qrA6U6"><img src="https://farm8.staticflickr.com/7522/16042128481_bbb2bde24f_c.jpg"></a>
<a href="https://flic.kr/p/qrA6U6">discussion</a> by <a href="https://www.flickr.com/photos/nalejandro/">Nicolas Alejandro</a>, on Flickr
</center>

While Jekyll is cool, I was immediately captivated by Nikola's ease of use and
by its developers' promptness in answering questions in the
[forum](https://groups.google.com/forum/#!forum/nikola-discuss). Also, one nice
thing about Nikola (and Pelican, too) which I forgot to mention in [my previous
post](link://slug/choosing-static-site-generator) is it's support for
multilingual sites. I guess I'll have to translate this post in interlingua
too, to give you a demonstration. :-)

Anyway, while I've fallen in love with static site generators, I still would
like to give people the chance of leaving comments. Services like Disqus are
easy to integrate, but given the way they can be (ab)used to track the users, I
prefered to go for something self hosted. So, enter
[Isso](https://posativ.org/isso/).

Isso is a Python server to handle comments; it's simple to install and
configure, and offers some nice features like e-mail notifications on new
replies.

My Isso setup
-------------

Integrating Isso with Nikola was relatively easy, but the desire to keep a
multilingual site and some hosting limitation made the process worth spending a
couple of words.

#### FastCGI

First, my site if hosted by [Dreamhost](https://dreamhost) with a very basic
subscription that doesn't allow me to keep long-running processes. After
reading [Isso's quickstart guide](https://posativ.org/isso/docs/quickstart/) I
was left quite disappointed, because it seemed that the only way to use Isso is
to have it running all the time, or have a `nginx` server (Dreamhost offers
Apache). Luckily, that's not quite the case, and more deployment approach are
described [in a separate
page](https://posativ.org/isso/docs/extras/deployment/), including one for
`FastCGI` (which is supported by Dreamhost). Those instructions are a bit
wrong, but yours truly [submitted some
amendments](https://github.com/posativ/isso/pull/528) to the documentation
which will hopefully go live soon.

#### Importing comments

Isso can import comments from other sites, but an importer for Blogger (a.k.a.
blogspot.com) was missing. So I wrote a quick and dirty tool for that job, and
[shared](https://github.com/posativ/isso/pull/529) it in case it could be
useful to someone else, too.

#### Multilingual sites

The default configuration of Nikola + Isso binds the comments to the exact URL
that they were entered into. What I mean is that if your site supports multiple
languages, and a user has entered a comment to an entry while visiting the
English version of the site, users visiting the Italian version of the site
would see same blog entry, but without that comment. That happens regardless of
whether the blog entry has been translated into multiple languages or not: it's
enough that the site has been configured for multiple languages.

[My solution](https://github.com/getnikola/nikola/issues/3215) to fix the issue
could not be accepted into Nikola as it would break old comments in existing
sites, but if you are starting a new multilingual site you should definitely
consider it.

Testers welcome
---------------

Given that I've deployed Isso as a `CGI`, it's understandable that it's not the
fastest thing ever: it takes some time to startup, so comments don't appear
immediately when you open a page. However, once it's started it stays alive for
several seconds, and that seems to help with performance when commenting.

Anyway, the real reason why I've written all this is to kindly ask you to write
a comment on this post :-) Extra points if you leave your e-mail address and
enable the reply notifications, and let me know if you receive a notification
once I'll reply to your comment. As far as I understand, you won't get a
notification when someone adds an unrelated comment, but only when the "reply"
functionality is used.

But really, should the commenting system be completely broken, I'm sure you'll
[find a way to contact me](link://slug/about), if you need to. :-)
