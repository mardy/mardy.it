<!--
.. title: Io attende vostre commentos
.. slug: looking-forward-to-your-comments
.. date: 2019-01-30 23:21:07 UTC+03:00
.. tags: interlingua,kdeplanet,planetmaemo,programmation
.. category: 
.. link: 
.. description: 
.. type: text
-->

Finalmente io ha migrate le sito a un nove systema (appellate
[Nikola](https://getnikola.com)),
que promitte facilitate de uso unite a grande possibilitates de integration con
altere servicios. Si vos es interessate al detalios technic io vos invita a
leger le version anglese de iste pagina (vide le ligamine al summitate), que es
multo plus extensive que isto.

<center>
<a href="https://flic.kr/p/qrA6U6"><img src="https://farm8.staticflickr.com/7522/16042128481_bbb2bde24f_c.jpg"></a>
<a href="https://flic.kr/p/qrA6U6">discussion</a> by <a href="https://www.flickr.com/photos/nalejandro/">Nicolas Alejandro</a>, on Flickr
</center>

Le motivo de iste entrata es principalmente lo de colliger vostre _feedback_ si
vos nota alicun cosa qui non functiona, in particular le systema de commentos;
a un prime test illo me sembla functionar ben, ma io non vole riscar de guastar
le tempore de mi lectores per invitar les a commentar si -- guai! -- illes
scribe longissime commentos que post es perdite.

Alora, si vos ha tempore e voluntate, per favor adde un breve commento a iste
entrata, e contacta me [in un altere maniera](link://slug/about) si vos trova
que isto non functiona.
