<!--
.. title: Elezioni: diffidate dei finti sovranisti
.. slug: elezioni-diffidate-dei-finti-sovranisti
.. date: 2019-05-19 18:37:40 UTC+03:00
.. tags: italiano,politica,Italia,electiones
.. category: 
.. link: 
.. description: 
.. type: text
-->

Da un anno a questa parte la parola “sovranità” ha acquistato importanza nel
dibattito politico, e l'epiteto “sovranista” è spesso affibbiato agli
intellettuali e ai politici, a volte come dispregiativo, altre volte invece 
rivendicato dal soggetto stesso come un attributo virtuoso. Tra questi ultimi,
tuttavia, si nascondono alcune personalità che di sovranista hanno solo uno
slogan e una mezza idea che sbandierano ad ogni comparsa televisiva, mentre si
candidano in un partito, sia esso la Lega o il Movimento 5 stelle, i cui
vertici restano saldamente fedeli all'Unione Europea e alla NATO.


Cambieremo l'Unione Europea dal di dentro
-----------------------------------------

<center>
<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2F215624181810278%2Fvideos%2F349505595754657%2F&#038;show_text=0&#038;width=560" width="560" height="315" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true"></iframe><br />
Diretta FB con i candidati Elia Torrisi (Più Europa) e Francesca Donato (Lega)<br />
</center>

Quando sentite un politico fare questa affermazione, potete star certi che si
tratta di una persona che ha un'idea molto confusa su cosa significhi
“sovranità”. Promettere di tentare di cambiare l'UE dall'interno significa
promettere di premere qualche volta il pulsante nelle sessioni del parlamento
europeo in modo da farsi sembrare un ribelle, ma senza mai scalfire l'idea che
vuole le istituzioni europee scavalcare quelle nazionali.

Ascoltate per esempio l'intervista a Francesca Donato, candidato della Lega,
raccolta nel video che trovate qui sopra; accostata al candidato di Più Europa
le differenze si fanno estremamente tenui. Certo, vi sono divergenze sull'idea
di come l'UE debba operare, ma nessuno sembra mettere più in dubbio il fatto
che il nostro paese debba continuare ad operare all'interno dei dettami imposti
da un sistema sovranazionale. 

“Vogliamo un Europa più democratica”, si dice, senza spiegare come si possono
conciliare interessi nazionali tanto diversi: se per quasi tutti i paesi
dell'unione, ad eccezion fatta di Italia, Spagna e Grecia, è conveniente poter
accedere alle arance e all'olio prodotti in Nordafrica, **un voto democratico
supporterebbe l'attuale abbattimento dei dazi**.

Si parla anche di uniformare la fiscalità, affinché non esistano paradisi
fiscali all'interno dell'Unione Europea; ma davvero possiamo pensare che
Olanda, Irlanda, Lussemburgo e altri paesi possano accettare di vedersi alzare
le tasse dall'Unione Europea?

E come risolvere il problema delle delocalizzazioni all'interno dell'UE?
Ritorna anche qui la parola "uniformare", che raccoglie tuttavia un significato
molto più utopico di quanto possa sembrare: un serio processo di uniformazione
delle varie situazioni lavorative, che non miri a un'appiattimento dei diritti
al ribasso, dovrebbe passare per l'introduzione di dazi all'interno dell'UE,
regolati in modo da stimolare i paesi ad innalzare le condizioni lavorative
(ovvero: ti tolgo i dazi sulle esportazioni solo quando le condizioni nel tuo
paese migliorano), ma questo sarebbe in diretta contrapposizione con le forze
capitalistiche che attualmente sono al potere. In altre parole, non avverrà
mai.


Euro o no euro?
---------------

La Lega candida anche un brillante economista, Antonio Maria Rinaldi, che in
più occasioni ha criticato il sistema Euro (come la sopracitata Francesca
Donato, a capo del [progetto Eurexit](https://www.progettoeurexit.it)). Sembra
tuttavia che la volontà di uscire dall'euro sia un po' scemata da quando le
candidature sono state annunciate, preferendo parlare di “riformare il sistema
euro”.


I sovranisti al guinzaglio di Trump
-----------------------------------

Le personalità candidate dalla Lega vantano un curriculum di tutto rispetto, e
pure le idee che portavano avanti (specialmente prima della candidatura) erano
per me del tutto condivisibili. Tuttavia a mio parere si sono candidate nel
partito sbagliato, un partito guidato al guinzaglio da Trump (che a sua volta è
il burattino di Netanyahu) e che difficilmente seguirà la volontà dei suoi
elettori. Mi piacerebbe poter chiedere ai candidati leghisti se supportano le
sanzioni contro il Venezuela, se i Palestinesi hanno diritto ad un loro stato,
e molti altri quesiti ai quali (immagino, ritendendole persone intelligenti) le
loro risposte sarebbero molto diverse da quelle di Salvini. Avrebbero il
coraggio di professare apertamente le loro opinioni? E in caso di voto,
avrebbero il coraggio di disobbedire al padrone?

Non che la situazione nella sponda del Movimento 5 stelle sia molto migliore:
benché al suo interno vi siano persone informate e rette (una su tutte, Manlio
di Stefano), all'atto pratico il partito non ha dato mostra di voler cambiare
lo status quo sul piano internazionale; sul Venezuela c'è stato un timido
tentativo di alzare la testa, ma sanzioni alla Russia, attaccamento all'UE e
alla NATO restano ben saldi.

