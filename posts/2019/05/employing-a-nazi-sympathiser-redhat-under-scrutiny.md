<!--
.. title: Employing a nazi sympathiser - RedHat under scrutiny
.. slug: employing-a-nazi-sympathiser-redhat-under-scrutiny
.. date: 2019-05-06 22:30:44 UTC+03:00
.. tags: english,Ucraina,kdeplanet,informatica,politica
.. category: 
.. link: 
.. description: 
.. type: text
-->

> **Update**: According to some rumors, RedHat has swiftly acted and the
> protagonist of this story is no longer in their ranks. You still might want to
> read this post, though, as it was not much about facts, but opinions. :-)
> - 15.05.2019

Politics is hard, and when you mix politics with other, more mundane aspects of
your daily life, the fight between political ideas, freedom of speech, and
other principles reach such a complexity that finding clear-cut answers becomes
quite hard.

Let's take for example the case, uncovered by the Ukrainian journalist [Anatoly
Shariy](https://en.wikipedia.org/wiki/Anatoly_Shariy), of a RedHat employee who
appears to be a Nazi sympathiser, [openly posing doing the Nazi salute right in
Berlin](https://www.youtube.com/watch?v=xhIQSJn6s4E&feature=youtu.be&t=651). It
will be interesting to see how (and if) RedHat will react to this information;
there are also other spaces to watch, given that the guy in question has been
giving talks in several conferences and is a very active member of the Eclipse
project.

Let's put aside the fact that in Germany exposing Nazi symbols is a crime, and
let's suppose that the guy did the Nazi salute in some other place where that's
not illegal; what would your reaction be, if you where the employer, or a
conference organizer, or if you had some position of authority in the open
source project that this guy is contributing to?

On a personal level, taking a distance from this fellow and cutting all ties
would be a rather understandable step (unless, of course, you share his views);
but on a professional/business level, would that be the appropriate decision?
If your answer is positive, without hesitation, then this post is for you.


Showing a nice image
--------------------

People have been fired or removed from their posts for much less than this,
that is true. Two cases which come to mind are [James
Damore](https://firedfortruth.com/), fired from Google for advancing the idea
that the low women representation in the IT industry might be due to some
biological traits, and [Brendan Eich](https://brendaneich.com/), who ceded to
pressure and [resigned from its post of CEO of
Mozilla](https://blog.mozilla.org/blog/2014/04/05/faq-on-ceo-resignation/)
after the news of his past donations to an anti-gay movement went public. I'm
sure there are plenty of other similar cases out there, but these are the ones
I happened to notice at the time. And I don't like how these cases ended.

While the two cases are extremely different from each other, the common
denominator is that -- in my opinion -- the companies (or the person itself, in
Eich's case) took a decision based exclusively at the perceived PR outcome, and
not on a matter of principle. And what is worse, is that this way of handling
disagreement is a great threat to freedom of speech and freedom of expression.

The world is full of racists, misoginists, homophobes, antisemitics, and, in
general, of people embracing ideologies which we despise. But I'm convinced
that the only way to fight these ideologies is opening a personal, one to one,
dialogue with the individual in question; getting to know their background, the
reasons why they came to believe in such things, and trying to find a way --
ideally, by providing yourself as a virtuous example -- to instill a doubt into
their convictions, and eventually to demolish them.

The firing of someone who has expressed some non-welcome ideas acts as a threat
to everyone else sharing the same views, and will lead to the result that the
problem will be swept under the carpet: fire one, and you'll never know about
the other dozens.


The benefits a good fight
-------------------------

I can imagine some of you screaming in disagreement: “But if you don't get rid
of them, the whole working environment becomes poisonous: employees will no
longer feel at ease (not to mention customers!)”. True, but we can take other
actions, other than just fire the employee: we can actually talk. First of all,
we can get him to openly state his position. It's possible (and I think this
nicely applies to Damore's case) that we misunderstood what he really meant, or
maybe he didn't express himself properly, or (even better) he has already
changed his mind. But if that's not the case, I believe there can be a good
opportunity to actually improve the situation: get people to talk.

I'm probably being naive here, but if I were the employer I would ask all the
employees to report, anonymously, whether they feel comfortable continuing
working with this guy. Then, organize one to one (private) meetings between
this person and all his colleagues, of at least five minutes in length, and
maybe repeat the whole round of conversations one more time, a couple of weeks
later. And finally, get once again everyone's anonymous feedback, and draw the
conclusions.

The reason why I wrote that *I'm naive* is because I think we'd see a clear
improvement in the answers, and we'd probably contributed, if not in
freeing one person from a poisonous ideology, at least in instilling some
doubt.


Back to reality
---------------

Even if they read my post, I'm quite positive that RedHat will fire this guy:
being in the same open source community as Google and Mozilla, the push to
react in a similar way as their peers is just too strong (and that's why I
condemn how the previous cases have unfolded: they traced a road which has now
become too hard to avoid).

(In the specific case I confess I woudn't regret it, and -- despite all what
I've written above -- I believe that firing this employee would be the proper
retribuition: not for being a nazi, but because this guy had a school teacher
in Ukraine fired, after he publicly accused her of showing an old soviet song
to her pupils; a song that was not even about communism, and which is not at
all forbidden in Ukraine!)
