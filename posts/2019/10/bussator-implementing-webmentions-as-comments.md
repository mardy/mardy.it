<!--
.. title: Bussator: implementing webmentions as comments
.. slug: bussator-implementing-webmentions-as-comments
.. date: 2019-10-04 11:36:34 UTC+03:00
.. tags: english,kdeplanet,planetmaemo,informatica,indieweb
.. category: 
.. link: 
.. description: 
.. type: text
-->

Recently I've grown an interest to the [indieweb](https://indieweb.org):
as big corporations are trying to dictate the way we live our digital
life, I'm feeling the need to [take a break from at least some of
them](link://slug/growing-up-or-leaving-facebook) and getting somehow
more control over the technologies I use.

Some projects have been born which are very helpful with that (one above
all: [NextCloud](https://nextcloud.com/)), but there are also many older
technologies which enable us to live the internet as a free distributed
network with no owners: I'm referring here to protocols such as HTTP,
IMAP, RSS, which I perceive to be under threat of being pushed aside in
favor of newer, more convenient, but also more oppressive solutions.

Anyway. The indieweb community is promoting the empowerment of users, by
teaching them how to regain control of their online presence: this pivots
arund having one's own domain and use self-hosted or federated solutions
as much as possible.

One of the lesser known technologies (yet widely used in the indieweb
community) is [webmentions](https://indieweb.org/webmention): in simple
terms, it's a way to reply to other people's blog posts by writing a
reply in your own blog, and have it shown also on the original article
you are replying to. The protocol behind this feature is an
[recommendation approved by the W3C](https://www.w3.org/TR/webmention/),
and it's actually one of the simplest protocol to implement. So, why not
give it a try?

I already added support for comments in my blog (statically generated
with [Nikola](https://getnikola.com)) by deploying
[Isso](https://posativ.org/isso/), a self-hosted commenting system which
can even run as a FastCGI application (hence, it can be deployed in a
shared hosting with no support for long-running processes) — so I was
looking for a solution to somehow convert webmentions into comments, in
order hot to have to deal with two different commenting systems.

As expected, there was no ready solution for this; so I sat down and
hacked up [Bussator](https://gitlab.com/mardy/bussator), a WSGI
application which implements a webmention receiver and publishes the
reply posts as Isso comments. The project is extensible, and Isso is only
one of the possible commenting systems; sure, at the moment it's indeed
the only one available, but there's no reason why a plugin for Static
Man, Commento, Remark or others couldn't be written. I'll happily accept
merge requests, don't be shy — or I can write it myself, if you convince me to (a nice Lego box would make me do anything 😉).

The first user of Bussator is this blog, and while the project code is
well covered by unit tests, we all know that real life is all another
matter; so please bear with me, if not everything works as it should. And
that's why I'll be thrilled to see your webmentions replies here. 

Ah, and webmentions will allow me to get your [Twitter likes and
replies](https://twitter.com/mardy/status/1180072676318158848) published
as comments, too — this thanks to [Brid.gy](https://brid.gy/)!
