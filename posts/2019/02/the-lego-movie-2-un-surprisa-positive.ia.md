<!--
.. title: The Lego Movie 2, un surprisa positive
.. slug: the-lego-movie-2-un-surprisa-positive
.. date: 2019-02-10 22:06:01 UTC+03:00
.. tags: interlingua,cinema,lego
.. category: 
.. link: 
.. description: 
.. type: text
-->

Heri io e mi sponsa ha accompaniate mi filios al cinema, pro guardar le film
"[The Lego Movie 2](https://www.imdb.com/title/tt3513498)". Isto es un film de
animation tridimensional que, como le titulo suggere, narra le aventuras de
nostre amate *minifiguras* Lego.

<center>
<figure>
<iframe width="640" height="360" allowfullscreen="true" src="https://www.comingsoon.it/videoplayer/embed/?idv=30888" frameborder="0" id="csVPlr"><a href="https://www.comingsoon.it/film/the-lego-movie-2/54930/video/?vid=30888">Le <i>trailer</i> del version italian del film</a></iframe>
<figcaption>Le <i>trailer</i> del version italian del film</figcaption>
</figure>
</center>

Il es possibile que isto sia causate da mi basse expectationes, ma io confessa
que le film me placeva multo, e in alicun momentos esseva multo plus *profunde*
de altere filmes mirate a un publico plus adulte. E a iste puncto io vos
consilia de stoppar le lectura de iste entrata, si vos plana de guardar le
film: io va revelar alicun partes del historia, e si vos non vole que vostre
surprisa sia guastate, stoppa de leger me hic.

(A iste puncto uno poterea demandar se qual es le senso de parlar vos de iste
film, viste que illo obviemente non es interessante pro vos; ma io non va poner
me iste question, e continua.)

Lo que plus me colpava es un drammatic *colpo de scena* (inexpectate inversion
del eventos) que mirabilemente presenta un confusion del conceptos de *bon* e
*perfide*: in breve, isto occurre quando Emmet, le protagonista, es sur le
puncto de liberar su amate Lucy e altere su amicos qui esseva captivate per le
folle e perfide habitantes de un planeta lontan. Il es in iste momento, que
Lucy comprende que illes non del toto es folle e perfide, ma sinceremente
amicabile; illa comprende que illa esseva victima de un mal comprension causate
per su prejudicios, e alora illa cerca, inutilemente, de convincer Emmet a
abandonar su plano de destruction de iste population extra-terrestre.

Iste momento es terribilemente actual, mesmo in le terminologia usate, quando
Emmet accusa Lucy de haber essite le victima de un **lavage del cerebro**
(anglese: *brainwashing*) que la impedi de vider le realitate, sin realisar que
il es proprio ille, le qui ha un vision completemente distorte del realitate e
ha totalmente invertite le conceptos de ben e mal.

Iste passage ben describe le problema del incommunicabilitate inter personas
qui ha ideas opposite e qui es profundemente convincite que *le altere* non
comprende, e que **nos ha le mission de portar le al veritate**. Un tema que es
sempre actual, e jammais solvite.

Complexivemente, le film es multo agradabile, con multe action, un poco de
comicitate, e anque alicun momentos multo *toccante* emotivemente — benque isto
pote esser causate del facto que io, in guardar le film con mi filios,
empathisava con illes e projectava super me lor emotiones (o melio: lo que *io*
imaginava esser lor emotiones).
Un hora e medie que io non regretta haber passate in le cinema!
