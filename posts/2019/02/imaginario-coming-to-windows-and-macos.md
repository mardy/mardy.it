<!--
.. title: Imaginario coming to Windows and MacOS
.. slug: imaginario-coming-to-windows-and-macos
.. date: 2019-02-08 20:07:10 UTC+03:00
.. tags: english,imaginario
.. category: 
.. link: 
.. description: 
.. type: text
-->

Good news for the intrepid photographers!
[Imaginario](http://imaginario.mardy.it) is now available for Windows and MacOS
— in addition to Linux, of course. Don't believe me? Here's a couple of
screenshots, then:

<center>
<figure>
  <a href="/archivos/imagines/imaginario-on-windows.png"><img src="/archivos/imagines/imaginario-on-windows.png" width="80%" /></a>
  <figcaption>Imaginario on Windows</figcaption>
</figure>
</center>

(click on the image to enlarge it)

<center>
<figure>
  <a href="/archivos/imagines/imaginario-on-macos.png"><img src="/archivos/imagines/imaginario-on-macos.png" width="80%" /></a>
  <figcaption>Imaginario on MacOS</figcaption>
</figure>
</center>

Of course, this does not mean that I'm inviting everyone to start using it:
Imaginario is still alpha quality. While it shouldn't just randomly delete your
photos, there are many rough edges here and there, so use it with extreme
caution.

Still, I'll appreciate if someone having access to these capitalistic operating
systems could give a quick try to Imaginario (you can download it from
[here](http://imaginario.mardy.it/releases.html)) and let me know if it starts
at all.  In case you wish to further explore it, you'll unavodably stumble upon
a lot of bugs; you can also [let me know about
them](https://gitlab.com/mardy/imaginario/issues).

