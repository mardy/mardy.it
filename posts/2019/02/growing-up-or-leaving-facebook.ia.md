<!--
.. title: Crescer, o abandonar Facebook
.. slug: growing-up-or-leaving-facebook
.. date: 2019-02-20 22:38:49 UTC+03:00
.. tags: interlingua,amicos,reflexiones,personal,globalisation
.. category: 
.. link: 
.. description: 
.. type: text
-->

Io ha finalmente decidite de cancellar mi conto de Facebook. Isto es un
decision que io initiava a considerar jam alicun annos retro, ma al qual io
usque ora ha preferite lo de limitar maximemente mi usage del sito: io lo
utilisa solmente in *modalitate de sol scriptura*, i.e. solmente pro diffunder
propaganda politic e, generalmente, pro **render lo un loco horribile anque pro
tote le alteres**.
Mi rationamento esseva iste: si io esseva un persona famose, alora io
abandonarea le sito sin hesitation, con grande ruito e impacto super les qui me
seque; ma post que io es connesse solmente con pauc personas, le unic maniera
pro haber un impacto (ben que minime) es lo de remaner e “salvar” mi amicos,
convincer lo super mi opiniones e/o incoragiar les con mi comportamento a
abandonar le sito.

<center>
<figure>
  <a href="/archivos/imagines/blog/leaving-facebook.jpg"><img src="/archivos/imagines/blog/leaving-facebook.thumbnail.jpg" width="80%" /></a>
  <figcaption>Il existe vita anque foras de Facebook</figcaption>
</figure>
</center>

Totevia, plus le tempore passava e plus io initiava a dubitar super le bontate
de iste idea. Mi objectivo, in fundo, es que le personas lassa Facebook; ma si
tanto que io remane in Facebook como totes, mesmo si protestante e destructive,
io es comocunque parte del *business* de Facebook e non presenta in ulle
maniera un defia al status quo. Il non ha alicun possibilitate de convincer
alicun a migrar a plattaformas alternative (o a abandonar le retes social del
toto) durante que nos mesme continua a esser in illo. In ultra, io sape anque
troppo ben que il es practicamente impossibile convincer un persona super
proprie ideas in un rete social; e anque mi plano perverse de render horribile
iste loco pro totes non pote functionar: mesmo si uno perdeva su patientia,
ille simplemente me bloccarea e continuarea como antea.

Del altere latere, per abandonar definitivemente Facebook il ha certemente le
possibilitate que necuno se curara de isto, ma al minus **io lo ha lassate**.
Io non plus essera un *resursa* pro Facebook. In le grandor del schemas, isto
es multo poco, un parve gutta in le oceano, ma io crede que isto es le
resolution melior. Esque io lo regrettara? Personalmente, non del toto: io ha
cessate de *consumar* Facebook jam desde un par de annos. Certemente, il es
possibile que le numero de visitas a mi blog e a mi canal in Youtube (io
parlara de iste altere monstro un altere vice) diminuera; in nostre tempore, il
non ha multe personas qui sape como utilisar un signalibro in lor navigator
(pro non parlar del [fluxos RSS](https://ia.wikipedia.org/wiki/RSS)) ma,
honestemente, il non ha multo que io pote facer pro remediar al situation.

Le gigantes technologic del interrete rende nostre vita plus facile, sin
dubita; il ha devenite plus facile tener se in contacto, trovar informationes,
consolidar un opinion *mainstream*. Toto es talmente conveniente, que illo nos
face non solmente plus pigre, ma anque plus incapace, al puncto que sin lor
servicios nos nos sentirea perdite e sin resursas. Il non plus se tracta
solmente de un dependentia psychologic: illos deveni un necessitate practic.
Isto, in torno, nos rende debile, facile a manipular e, ultimemente, sclavos.

Io crede in le interrete como un *inter rete*: un loco distribuite,
interconnesse, **sin padron**. Mi providitor de spatio web decide de censurar
me e clauder mi sito? Ben, io aperi un altere sito con un altere providitor. Ma
si Facebook te claude le accesso, immediatemente tu perde le connessiones a tu
amicos, al gruppos in le qual tu participava, le mercato, le eventos local, le
novas del die, tu profilo, **tu historia**.

E io, io vole viver in un mundo ubi io es le padron de mi vita.
