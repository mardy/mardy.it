<!--
.. title: Growing up, or leaving Facebook
.. slug: growing-up-or-leaving-facebook
.. date: 2019-02-20 22:38:49 UTC+03:00
.. tags: english,amicos,kdeplanet,reflexiones,personal,globalisation
.. category: 
.. link: 
.. description: 
.. type: text
-->

I've finally decided to delete my Facebook account. I've been pondering this
decision for several years now; my previous choice on the matter was to
just use it as little as possible, in *write-only mode*, just to spread political
propaganda and generally **make it a horrible place for everybody else too**. My
reasoning was the following: if I had a huge following, then I'd definitely
leave and make a big noise (and impact); but since I'm mostly connected with
real acquaintances only, my only way to make a (albeit minimal) impact is to
stay there and “save” my friends by convincing them of my opinions and/or by
being so annoying that they'd decide to leave first.

<center>
<figure>
  <a href="/archivos/imagines/blog/leaving-facebook.jpg"><img src="/archivos/imagines/blog/leaving-facebook.thumbnail.jpg" width="80%" /></a>
  <figcaption>There's life out there, too</figcaption>
</figure>
</center>

However, the more time passes, the more I start doubting this position. What I
really want is for people to leave Facebook; but as long as I'm there, as
everybody else is there, no matter how grumpy or destructive we might be, we
are still part of Facebook's business and not challenging the status quo in any
meaningful way. We are not going to convince anyone to migrate to alternative
platforms (or just leave all social media for good) while we continue to
maintain a presence in there. Also, I know too well that it's nearly impossible
to change someone's mind via social media, and my plan to make the place
horrible for everybody is not going to work either: if people get annoyed,
they'll just mute me and that's it.

On the other hand, if I leave Facebook for good, there's still a high chance
that nobody will care, but at least **I've quit**. I'll no longer be a
*resource* for Facebook. On the grand scheme, this is very little, a tiny drop
in the ocean, but I believe it's the best I can do. Will I suffer it?
Personally, not at all: I've not been *consuming* Facebook for a couple of
years by now. But certainly, it's possible that the number of visits to my blog
or YouTube channel (we'll talk about this other monster some other time) will
decrease; not many people know how to use browser bookmarks (let alone an [RSS
feed](https://en.wikipedia.org/wiki/RSS)) nowadays but, honestly, there's not
much I can do about it.

The tech giants of the internet are certainly making our lives easier: it has
become easier to stay in touch, to find information, to consolidate a
mainstream opinion. It's so convenient, that it is making us not only lazier,
but also dumber, to the point that we'd feel lost and powerless without their
services. We are not talking only about a psychological addiction anymore: they
are becoming a practical necessity. This, in turn, makes us weak, easy to
manipulate and, ultimately, slaves.

I believe in the internet as the *inter net*: a distributed, interconnected
place, **with no owner**. My hosting provider decides to censor me and shuts
down my site? Fine, I open a new one in another hosting. If Facebook kicks you
out, you immediately lose access to your friends, the groups you followed, the
marketplace, local events, your daily news, your identity, **your history**.

And me, I want to live in a world where I'm the owner of my life.
