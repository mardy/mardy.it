<!--
.. title: How to be an awesome member of the Free Software community
.. slug: how-to-be-an-awesome-member-of-the-free-software-community
.. date: 2019-02-14 23:01:37 UTC+03:00
.. tags: english,programmation,Linux,kdeplanet
.. category: 
.. link: 
.. description: 
.. type: text
-->

No, this is not a tutorial, unfortunately. The main reason being that, while I
strive to do my best, I don't consider myself to be an excellent member of the
Free Software community, let alone be able to teach others about it. But this
morning I got an email from the FSF about a [campaign for St. Valentine's
day](https://www.fsf.org/blogs/community/show-your-love-for-free-software-this-valentine2019s-day),
which reminded me of something I've been planning to do since a long time ago,
but never got to it.

<center>
<a data-flickr-embed="true" data-footer="true"  href="https://www.flickr.com/photos/hankinsphoto/6638666521/in/photolist-b7CUgB-e2WDRo-4WgoiS-83doSp-CXcvVv-6dViCG-3pYq1-kVMN5-dpKE72-3a5xNj-dpKRMD-dpKMnk-BdqeU-7rsPWF-dpKCVz-dpKTwE-4oixxV-4jVwp5-9eYiQi-fAVTN-NEA1Tr-hFNo1-5hwNbn-4XqHkW-9dF88M-mUaTZ-4JyiHd-a1BmHu-dpL5t4-fEKbUj-MLWNY-eaAt2-9eBwA2-7DKJgJ-dsqMiX-jTdJG4-8ptdcK-4sbJ3S-5HQLtj-bJX2ag-pJaeS-9P8b2-J9Fged-9vygAR-drF4dZ-c4NPMj-5TXuSm-avx59c-L1MvD-2Bc7Li" title="Love"><img src="https://farm8.staticflickr.com/7160/6638666521_9dd0c8ff67_b.jpg" width="700" height="466" alt="Love"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
</center>

I want to publicly send huge thanks to Robin Mills from the [Exiv2
project](https://github.com/Exiv2/exiv2), and not only because I've been
fruitfully using his work in three (!) projects of mine
([PhotoTeleport](http://www.phototeleport.com), [Mappero
 Geotagger](http://www.mardy.it/mappero-geotagger/) and
 [Imaginario](http://imaginario.mardy.it)), but also, and especially, for being
an extremely pleasant interlocutor. On the web, yes. Whereas most people tend
to be more thorny and touchy in their interactions over the internet, Robin
has always been friendly and coversational, trying to form a bond with some
complete foreigner who just happened to report a bug on Exiv2.

Just adding some bits of information about one's personal events ([such as
travels](https://github.com/Exiv2/exiv2/issues/182)) can make an enormous
difference on how one's attitude is perceived. [Mentioning that you visited the
place](https://github.com/Exiv2/exiv2/pull/640) that is familiar to the bug
reporter almost makes one forget of being sitting in front of a computer on the
internet, because your mind flies to that place. Considering that even on this
personal blog of mine I'm kind of reticent about speaking of my private life, I
cannot help appreciating the friendly attitude that Robin reserves for people
writing on a bug tracker.

You are a wonderful netizen, Robin. A happy Valentine Day to you and your
family. Thank you.
