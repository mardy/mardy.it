<!--
.. title: Crescere, ovvero abbandonare Facebook
.. slug: growing-up-or-leaving-facebook
.. date: 2019-02-20 22:38:49 UTC+03:00
.. tags: italiano,amicos,reflexiones,personal,globalisation
.. category: 
.. link: 
.. description: 
.. type: text
-->

Ho finalmente deciso di cancellare il mio profilo Facebook. È una decisione che
ho iniziato a considerare già alcuni anni fa, ma alla quale ho fino ad ora
preferito quella di limitare al minimo il mio uso della piattaforma, usandola
solo in *modalità di sola scrittura*, ovvero soltanto per diffondere propaganda
politica e in generale per **renderlo un posto orribile anche per tutti gli
altri**.
Il mio ragionamento era il seguente: se fossi un personaggio famoso, allora sì
che abbandonerei la piattaforma senza indugi, generando un gran chiasso e un
impatto su chi mi segue; ma siccome sono connesso con poche persone, l'unica
maniera per avere un impatto (per quanto minimo) è quella di restare e
“salvare” i miei amici convincendoli delle mie opinioni e/o invogliandoli col
mio comportamento ad abbandonare la piattaforma prima di me.

<center>
<figure>
  <a href="/archivos/imagines/blog/leaving-facebook.jpg"><img src="/archivos/imagines/blog/leaving-facebook.thumbnail.jpg" width="80%" /></a>
  <figcaption>C'è vita anche là fuori</figcaption>
</figure>
</center>

Tuttavia col passare del tempo si sono rafforzati i miei dubbi sulla bontà di
questa idea.  Quello a cui miro, in fondo, è che la gente si liberi di
Facebook; ma finché resto in Facebook come fanno tutti, per quanto io possa
protestare o comportarmi in maniera distruttiva, sarò comunque parte del
*business* di Facebook e non costituirò in alcun modo una sfida allo status
quo. Non abbiamo nessuna possibilità di convincere nessuno a migrare a
piattaforme alternative (o ad abbandonare del tutto le reti sociali) finché noi
stessi continuiamo ad esserne parte. Inoltre, so fin troppo bene che è
praticamente impossibile convincere qualcuno delle proprie idee su una rete
sociale, e anche il mio piano malvagio di rendere orribile questo posto per
tutti non può funzionare: se anche qualcuno perdesse la pazienza, mi bloccherà
e via.

D'altra parte, lasciando definitivamente Facebook c'è sì un'alta probabilità
che la mia dipartita non importi a nessuno, ma al meno **io me ne sono
andato**. Non sarò più una *risorsa* per Facebook. Nella grandezza dello schema
questo è molto poco, una piccola goccia nel mare, ma ritengo che sia l'unica
via d'uscita sensata. Lo rimpiangerò? Certamente no: è già da un paio d'anni
che praticamente non leggo gli *aggiornamenti* dei miei amici in Facebook.
Certo, è senz'altro possibile che il numero delle visite al mio blog o al
canale YouTube (di quest'altro mostro parleremo un'altra volta) ne soffrirà: di
questi tempi non molte persone sanno come usare i segnalibri del browser (per
non parlare dei [flussi RSS](https://it.wikipedia.org/wiki/RSS)) ma,
onestamente, non c'è molto che io possa fare per rimediare alla situazione.

I giganti tecnologici della rete stanno certamente rendendo le nostre vite più
facili: è diventato più facile tenersi in contatto, trovare informazioni,
consolidare l'opinione sul *mainstream*. È tutto talmente conveniente che ci ha
resi non solo più pigri, ma anche più incapaci, al punto che senza i loro
servizi ci sentiremmo persi e senza risorse. Non si parla più di una semplice
dipendenza psicologica: stanno diventendo una necessità pratica.  Questo, a sua
volta, ci rende deboli, facili da manipolare e, in ultima istanza, schiavi.

Io credo in internet come una *inter net* (inter-rete): una rete distribuita,
interconnessa, **senza un padrone**. Il mio fornitore di spazio web decide di
censurarmi e chiudermi il sito?  Bene, ne apro un altro con un altro fornitore.
Ma se Facebook ti chiude l'accesso, immediatamente perdi le connessioni ai tuoi
amici, ai gruppi in cui partecipavi, il mercatino, gli eventi locali, le
notizie del giorno, il tuo profilo, **la tua storia**.

Io, se permettete, vorrei essere il padrone della mia vita.
