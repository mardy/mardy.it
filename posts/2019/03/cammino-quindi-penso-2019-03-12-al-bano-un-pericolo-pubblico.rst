.. title: Cammino quindi penso - 2019-03-12 - Al Bano, un pericolo pubblico
.. slug: cammino-quindi-penso-2019-03-12-al-bano-un-pericolo-pubblico
.. date: 2019-03-12 18:35:36 UTC+03:00
.. tags: italiano,video,Cammino quindi penso,Ucraina
.. category: 
.. link: 
.. description: 
.. type: text

.. youtube:: doj8LDtPtLY
   :align: center

Il cantante Al Bano è entrato, suo malgrado, nella lista nera dei nemici dell'Ucraina.

Vedi `l'intervista su Repubblica
<https://video.repubblica.it/cronaca/al-bano-nella-lista-nera-dellucraina-lartista-terrorista-io-ammiro-putin-che-male-ce/329171/329769>`_.

