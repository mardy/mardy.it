.. title: Ubports at the LinuxPiter conference
.. slug: ubports-at-the-linuxpiter-conference
.. date: 2019-03-13 19:07:23 UTC+03:00
.. tags: video,english,Ubuntu,Ubports,informatica,maemo,Sancte Petroburgo,kdeplanet,planetmaemo
.. category: 
.. link: 
.. description: 
.. type: text

Last November I was invited to talk at the `LinuxPiter
<https://linuxpiter.com/>`_ conference. I held a presentation of the `Ubports
project <https://ubports.com/>`_, to which I still contribute in my little
spare time.

The video recording from the conference has finally been published:

.. youtube:: _gWTiVPahT0
   :align: center

(there's also a `version in Russian <https://www.youtube.com/watch?v=pYiqOzZZg-w>`_)

There was not a big audience, to be honest, but those that were there expressed
a lot of interest in the project.
