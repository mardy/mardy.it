.. title: Cammino quindi penso - 2019-03-05 - Perché ho lasciato Facebook
.. slug: cammino-quindi-penso-2019-03-05-perche-ho-lasciato-facebook
.. date: 2019-03-05 22:05:57 UTC+03:00
.. tags: italiano,video,Cammino quindi penso
.. category: 
.. link: 
.. description: 
.. type: text

.. youtube:: _Rts_hneWmo
   :align: center

Reti sociali alternative:

* Diaspora (simile a Facebook): https://joindiaspora.com/
* Mastodon (simile a Twitter): https://joinmastodon.org/

Queste reti sono distribuite, ovvero manca un server centrale. Consistono di
diversi nodi connessi tra di loro attraverso un protocollo aperto.
