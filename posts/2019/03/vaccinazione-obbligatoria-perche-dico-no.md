.. title: Vaccinazione obbligatoria, perché dico no
.. slug: vaccinazione-obbligatoria-perche-dico-no
.. date: 2019-04-08 18:40:57 UTC+03:00
.. tags: italiano,scientia
.. category: 
.. link: 
.. description: 
.. type: text

La mia contrarietà all'obbligatorietà del trattamento vaccinale si può
sintetizzare in una riga: ritengo il corpo umano sacro e inviolabile. Almeno
quando l'individuo è nel pieno delle sue facoltà mentali, deve essere libero di
decidere autonomamente, senza pressioni, se accettare o meno inoculazioni o
altri interventi chirurgichi (come anche l'impianto di circuiti elettrici
sottocutanei).

Mentre quello appena esposto è il punto cardinale della mia obiezione, vi
propongo altre considerazioni più o meno razionali che mi portano a rifiutare
l'imposizione della vaccinazione. Per oggi mi limiterò ad un aspetto che
ritengo particolarmente preoccupante, ma mi riprometto di tornare
sull'argomento con altri articoli, in futuro.

Una premessa doverosa: credo fermamente nell'efficacia dei vaccini! Certamente
possono sempre verificarsi casi di mala sanità, che non dobbiamo mai ignorare,
ma quanto segue è scritto con l'assunto di disporre di processi di vaccinazione
perfetti.


Un'evoluzione al contrario
--------------------------

<center>
<a data-flickr-embed="true"  href="https://www.flickr.com/photos/21436930@N02/2272168514" title="evoluzione"><img src="https://farm3.staticflickr.com/2158/2272168514_723199b250_z.jpg?zz&#x3D;1" width="640" height="198" alt="evoluzione"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
</center>

Mi sarà sfuggito perché non seguo attentamente il dibattito sui vaccini, ma un
argomento che non ho mai sentito esposto è quello sull'ostacolamento del
processo evolutivo. La sola esposizione di questo argomento mi farà passare per
cinico e disumano, quindi permettetemi di premettere che io stesso mi sono
sempre ritenuto un “errore” evolutivo: a causa della mia gracilità sono
convinto che, fossi nato uno o due secoli prima, sarei deceduto già in tenera
età per qualche banale malattia, in assenza dei progressi medici nel frattempo
avvenuti. Avremmo dovuto disperarci per questa perdita? Forse, ma probabilmente
al mio posto avrebbe poi visto la luce un altro mio fratello, più forte di
costituzione e magari anche un po' più intelligente.

Ora, il motivo a sostegno della vaccinazione obbligatoria che sento ripetere
più di altri è quello per cui, raggiungendo la cosiddetta “immunità di gregge”,
anche gli individui immunodeficienti (ed altri che per motivi medici non
possono essere vaccinati) vengono protetti dalle malattie, in quanto queste non
trovano abbastanza “terreno fertile” per diffondersi. Tra questi individui
“salvati” grazie all'immunità di gregge vi è un numero di immunodeficienti
congeniti, ovvero che hanno ereditato l'immunodeficienza geneticamente, dai
propri genitori.

Quindi, l'immunità di gregge protegge questi individui, che altrimenti
avrebbero contratto la malattia e — in assenza di cure adeguate — avrebbero
visto ridotte le loro possibilità di raggiungere l'età riproduttiva. In altre
parole, viene meno (o viene notevolmente ridotto) il meccanismo di selezione
naturale con la conseguenza che, nel lungo periodo, la percentuale di
immunodeficienti congeniti è destinata ad aumentare.

Invito il lettore a considerare quanto scritto sopra in termini puramente
scientifici e a non imputarmi discorsi che non ho mai pronunciato. Il mio
movente è presentare il mio ragionamento e sottometterlo ad una critica
fattuale, prima di stimolare una conversazione sulle conseguenze della sua
eventuale correttezza.

Ma riprendiamo il discorso: quale sarebbe l'effetto di un aumento della
percentuale di individui immunodeficienti? Evidentemente, superato un certo
limite (che, non essendo un esperto, non posso stimare, ma presumo si tratti di
una percentuale inferiore al 5%) la popolazione non sarà più coperta
dall'immunità di gregge, perché questi individui non potranno essere vaccinati.
La comparsa di un epidemia virale, già nota oppure nuova, potrebbe avere un
effetto devastante sulla popolazione; tanto più devastante, quanto più alta la
percentuale di immunodeficienti (quindi, paradossalmente, saremmo costretti a
sperare che una tale epidemia si verifichi il prima possibile, in modo da
limitare i danni!).

Qualcuno potrebbe obiettare che, in fondo, lo scenario che ho sopra descritto
non è peggiore di quello che avverrebbe se la percentuale degli individui
immunodeficienti fosse mantenuta a bassi livelli dalla selezione naturale: nel
secondo caso, infatti, vi sarebbe un effetto altrettanto tragico, ma spalmato
in un arco di tempo più lungo. La critica è certamente fondata, ma si può
ribaltare in questo modo: se tanto l'effetto è lo stesso, perché insistere
sulla necessità di raggiungere l'immunità di gregge?

