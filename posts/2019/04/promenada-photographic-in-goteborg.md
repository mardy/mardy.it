<!--
.. title: Promenada photographic in Goteborg
.. slug: promenada-photographic-in-goteborg
.. date: 2019-04-15 19:47:24 UTC+03:00
.. tags: interlingua,Svedia,photographia,turismo
.. category: 
.. link: 
.. description: 
.. type: text
-->

Io me promenava con alicun collegas per le stratas de Goteborg, armate de
nostre photocameras e desiderose de capturar alicun quadros interessante.

<center>
<figure>
<a data-flickr-embed="true"  href="https://www.flickr.com/photos/mardytardi/albums/72157677720171997" title="Göteborg photowalk"><img src="https://live.staticflickr.com/7823/46885061464_8532e2484f_c.jpg" width="800" height="535" alt="Göteborg photowalk"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
<figcaption>Un vista de Goteborg. <a href="https://www.flickr.com/photos/mardytardi/albums/72157677720171997">Clicca pro vider tote le photos</a>.</figcaption>
</figure>
</center>

Goteborg es un bellissime citate, ma io preferiva evitar de photographar cata
edificio e palacio (ben que multe de illos es vermente mirabile!), que totes
pote admirar in le interrete, e in vice cercar de esser plus original, e
capturar detalios o quadros plus inusual.

Io spera que le resultato vos place.
