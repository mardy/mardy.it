<!--
.. title: A critical view on the blockchain
.. slug: a-critical-view-on-the-blockchain
.. date: 2019-04-24 22:59:45 UTC+03:00
.. tags: english,kdeplanet,planetmaemo,informatica,blockchain
.. category: 
.. link: 
.. description: 
.. type: text
-->

At the beginning of this month I participated to the
[foss-north](https://foss-north.se) conference, in Gothenburg, and took the
stage to give a short presentation of the blockchain technology. Given that my
talk was somehow critical of the blockchain (or rather, of the projects using
it without due reason) I was prepared to receive a wave of negative remarks,
assuming that all the hype surrounding this technology would have infected a
good part of my audience as well. I was therefore positively surprised when
several people came to me afterwords to express their appreciation for my
speech, appreciation that now makes me confident enough to share the video of
the presentation here too:

<center>
<iframe width="560" height="315"
src="https://www.youtube.com/embed/5VcdC3A-ezg" frameborder="0"
allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
allowfullscreen></iframe>
</center>

I want to publicly thank [Johan Thelin](http://www.thelins.se/johan/blog/) and
all the other foss-north staff and volunteers who organized such a successful
conference. They also managed to get the video recordings out in a surprisingly
short time. Indeed, the above video is taken from the [foss-north
YouTube channel](https://www.youtube.com/channel/UCQvR8lgE9rishcKT_hZT6eQ),
which I recommend you to visit as there were a lot of good talks at the
conference; the topics were so varied, that I'm sure you'll find at least a
couple of talks of your interest.
