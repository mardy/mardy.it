.. title: Cammino quindi penso - 2019-04-09 - Fecondazione artificiale
.. slug: cammino-quindi-penso-2019-04-09-fecondazione-artificiale
.. date: 2019-04-09 23:34:38 UTC+03:00
.. tags: italiano,video,Cammino quindi penso,etica
.. category: 
.. link: 
.. description: 
.. type: text

.. youtube:: wHcBQPEkYcI
   :align: center

Nel Nebraska, una signora di 61 anni ha dato alla luce una bambina
concepita mediante la fecondazione artificiale.
