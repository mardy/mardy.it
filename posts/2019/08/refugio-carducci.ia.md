<!--
.. title: Refugio Carducci
.. slug: refugio-carducci
.. date: 2019-08-11 16:13:46 UTC+03:00
.. tags: interlingua,Italia,natura,turismo
.. category: 
.. link: 
.. description: 
.. type: text
-->

Le promenada del pais de
[Auronzo](https://it.wikipedia.org/wiki/Auronzo_di_Cadore) al [refugio
Carducci](http://rifugiocarducci.eu/), que nos completava heri, esseva un del
plus belle excursiones que io rememora. E certemente anque un del plus
difficile e emotionante, sia per le grande disnivello (plus que 1300
metros — le refugio se trova a 2297 metros de altitude) sia pro alicun
tractos del percurso multo exponite (on dice que le percurso es
“exponite” quando sub de illo il ha un precipio). Ma permitte me de
initiar del principio.

<figure style="margin: 0 auto; width: 512px">
  <img src="/archivos/imagines/blog/carducci-tabula.jpg" class="shadow-sm" />
  <figcaption>Le tabula al initio del percurso.</figcaption>
</figure>

Le excursion initia in le localitate “Pian de la Velma”, proxime a
Auronzo, a circa 960 metros de altitude. Post alicun metros de cammino,
nos incontrava le tabula photographate hic in alto, que informava que
continuar le percurso (marcate per le numero 103) esseva prohibite a
causa de alicun grande lapsos de terreno que occurreva in le 2015. Ma
post que le personal de nostre hotel nos consiliava iste excursion, sin
mentionar iste blocco, nos assumeva que iste tabula non debeva esser
prendite troppo seriosemente, e decideva continuar.

Post un decena de minutas nos esseva superate de un altere excursionista, qui nos confirmava que le percurso habeva essite aperite justo un septimana retro, e que nos poteva ignorar le aviso del tabula, que essera tosto removite.

<figure style="margin: 0 auto; width: 512px">
  <img src="/archivos/imagines/blog/carducci-ponte.jpg" class="shadow-sm" />
  <figcaption>Le ponte que colliga le nove percurso con le vetule.</figcaption>
</figure>

De facto, nos jam habeva trovate le puncto ubi le sentiero 103 originari
esseva bloccate per alicun ramos, e il habeva un deviation per un altere
sentiero, que pareva un de ille sentieros traciate per le cercatores de
fungos. Iste sentiero ascendeva plus ripidemente al monte, e a un certe
puncto le multe radices que emergeva del terreno revelava como iste
sentiero habeva essite scavate multo recentemente. Anque quando le
sentiero entrava in le bosco, il esseva evidente que non multe personas
lo habeva percurrite, perque le terreno esseva un poco instabile. In un
par de occasiones le bosce se aperiva, e le sentiero passava trans
precipitios petrose, que nos transversava con un poco de paura (reguarda
le video infra a 29 secundas).

<figure style="margin: 0 auto; width: 340px">
  <img src="/archivos/imagines/blog/carducci-nove.jpg" class="shadow-sm" />
  <figcaption>Un tracto del nove sentiero 103. On pote notar le ramos recentemente taliate pro permitter le passage.</figcaption>
</figure>

Le sentiero se developpa longo al valle Giralba, e in plure punctos
incrocia le torrente Giralba, que da le nomine al valle (o vice versa?).
Le ruito del torrente esseva ben audibile pro le quasi totalitate del
percurso, e in alicun punctos il habeva le possibilitate de biber e de
facer refornimento de aqua.

<figure style="margin: 0 auto; width: 512px">
  <img src="/archivos/imagines/blog/carducci-torrente.jpg" class="shadow-sm" />
  <figcaption>Le torrente que scava le valle.</figcaption>
</figure>

Un belle e alte cascata, que scendeva longo le parete petrose del montanias, esseva anque visibile. Ma nos non plus lo incontrava durante nostre percurso, e non poteva vider lo plus de vicin; forsan il esserea possibile attinger al cascata per alicun altere percurso, ma nos non voleva riscar de facer deviationes — nostre destination esseva ancora distante.
 
<figure style="margin: 0 auto; width: 340px">
  <img src="/archivos/imagines/blog/carducci-cascata.jpg" class="shadow-sm" />
  <figcaption>Le torrente e, in alto in le montanias, le cascata.</figcaption>
</figure>

A 1335 metros de altitude (si le GPS de mi photocamera es correcte) se
trova un ponte de ligno que transversa le torrente, e post alicun metros
on se reconjunge con le vetule sentiero 103 originari, que nos sequera
desde iste momento.

Le differentia inter le terreno del nove e del vetule sentiero esseva
evidente: le terreno del vetule sentiero esseva plus dur e dava le
impression de esser plus stabile (benque, io debe dicer, le nove tractos
del sentiero numquam se rumpeva sub nostre passos). Ma in alicun punctos anque iste parte del sentiero semblava haber esser colpate per lapsos de petras.

<figure style="margin: 0 auto; width: 512px">
  <img src="/archivos/imagines/blog/carducci-petras.jpg" class="shadow-sm" />
  <figcaption>Un puncto partialmente obstruite per un lapso de petras.</figcaption>
</figure>

Un loco particularmente agradabile esseva un rivulo de aqua, immerse in
le verde. Illo se trova a 1823 metros de altitude e es indicate per un
parve tabula que porta le inscription “acqua”. Iste loco semblava
prendite de un fabula o de un pictura de fantasia, tanto illo esseva
verde e circumdate per milles de parve flores. Vos pote vider lo a 2:28 minutas in le video infra.

<figure style="margin: 0 auto; width: 340px">
  <img src="/archivos/imagines/blog/carducci-monte.jpg" class="shadow-sm" />
  <figcaption>Le vista deveniva sempre plus belle, a man a man que nos ascendeva.</figcaption>
</figure>

Con le augmentar del altitude, le aspecto del sentiero cambiava: le pinos
deveniva sempre plus rar, e anque le arbustos de [pino
mugo](https://it.wikipedia.org/wiki/Pinus_mugo) al fin lassava le spatio
a pratos verde e plen de flores. Il es curiose, como non mesmo le herba
pote crescer plus alte que 15-20 centimetros, a certe altitudes.

<figure style="margin: 0 auto; width: 512px">
  <img src="/archivos/imagines/blog/carducci-prato.jpg" class="shadow-sm" />
  <figcaption>A 2000 e plus metros de altitude, il habeva multissime de
iste flores con petalos «lanose». Io non sape le nomine de iste flores —
suggestiones es benvenite in le commentos.</figcaption>
</figure>

Nos jam poteva vider le refugio, benque illo esseva ancora 300 metros
plus in alto. Isto nos comfortava, ma al mesme tempore nos tosto
observava como le vicinantia esseva solmente apparente: ben que nos
fatigava e ascendeva, le refugio semblava sempre mantener le mesme
distantia de nos.

<figure style="margin: 0 auto; width: 512px">
  <img src="/archivos/imagines/blog/carducci-flores-jalne.jpg" class="shadow-sm" />
  <figcaption>Iste flores jalne anque esseva multo commun (e anque de
iste flores io ignora le nomine).</figcaption>
</figure>

In le parte final del sentiero le pratos cedeva sovente lor placia a
mantellos de petras de varie dimentiones. Iste petras infortunatemente es
instabile (especialmente excursionistas qui descende rapidemente face
rotolar multe petras sub lor passos), e le forma del sentiero pote
cambiar un poco. De facto, le refugio es tanto proxime que il ha plure
percursos possibile pro attinger lo; le problema es que non totes es
equalmente facile, alora on debe esser attente a seliger le via le plus
apte a proprie capacitates.

<figure style="margin: 0 auto; width: 512px">
  <img src="/archivos/imagines/blog/carducci-lontan.jpg" class="shadow-sm" />
  <figcaption>2120 metros. Le refugio es ben visibile (aggrandi le
photographia pro vider lo!). De iste altitude, le sentiero deveniva plus
petrose e instabile.</figcaption>
</figure>

Finalmente nos attingeva le refugio. Nos prendeva un cappuccino e un
chocolate calide, accompaniate per duo partes de tortas. Post iste
effortio (qui durava quatro horas e medie, includente multe pausas per
photographias), isto esseva un premio ben meritate!

<figure style="margin: 0 auto; width: 512px">
  <img src="/archivos/imagines/blog/carducci-refugio.jpg" class="shadow-sm" />
  <figcaption>Nostre destination.</figcaption>
</figure>

Io ha assemblate un breve video con varie registrationes prendite durante le ascension al refugio. Isto pote dar vos un idea plus clar super lo que vos expecta, si vos decide probar le mesme excursion:

<center>
<iframe width="560" height="315" src="https://www.youtube.com/embed/hQ7EFIlx1aY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</center>
