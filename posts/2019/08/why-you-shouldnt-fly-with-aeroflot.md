<!--
.. title: Why you shouldn't fly with Aeroflot
.. slug: why-you-shouldnt-fly-with-aeroflot
.. date: 2019-08-15 19:51:00 UTC+03:00
.. tags: kdeplanet,english,viages
.. category: 
.. link: 
.. description: 
.. type: text
-->

I'm writing this while sitting in the Moscow airport, in a state which is a mix
of tiredness, anger and astonishment. At this time I should be already at home,
in Saint Petersburg, but something went very wrong and I feel the need to vent
my frustration here. Please bear with me :-)

The story goes like this: we booked a flight from Venice to Saint Petersburg,
with a change in Moscow. The time between flights was 1 hour and 20 minutes —
that's not plenty of time, but it's still much longer than other changes I've
had in the past in other airports. And I should stress that this flight
combination was suggested to me by the Aeroflot company's website, so this time
should be enough for the transfer. At least, this was my assumption.

And it was wrong: in spite of the flight from Venice to Moscow landing in time,
in spite of us not losing a minute and performing all the passport and security
checks without delays (we didn't even let the kids visit the toilet!), in spite
of us running along the corridors as soon as we realized that we *might* be
late, we arrived at the gate just in time to see it closing in front of us. And
no, rules are rules, so they wouldn't let us board.

The guy at the gate comforted us by saying that planes from Moscow from Saint
Petersburg fly every hour, so we could just take the next one, for free. We
tried to protest, but to no avail; we went to the Aeroflot ticket desk,
explained the situation, and they told us that we could take the flight 6 hours
later. And no, it didn't matter that we had kids; the employee at the desk told
us that we should be grateful to them, that we could fly for free, as they were
making an exception for us.

Yes, you've read it correctly: they asked us to fly 6 hours later, and instead
of giving us any compensation (as I once got from Finnair, for example, when
their flight was late), **we had to be grateful** for not having to buy the
tickets again!

And I want to make one thing clear: there was no announcement about the
transfer time being tight, nor in the plane nor at the airport; no one came
forward asking for passengers transferring to St. Petersburg and inviting us to
skip the lines for the various controls, nothing. Our names were not called,
ever. All that is the norm in all other similar situations I've experienced. If
that does not happens, it means — but maybe I'm being too naive — that the
second plane is waiting (or that there's still plenty of time to board it).

Luckily they found that an earlier flight, which was *only* three hours late
than our initial expected departure had three available seats, so my wife and
kids are boarding that place right now while I'm writing this. I'll take the
other flight in three hours, hoping that there won't be any more surprises.

Lesson of the day: always fly from Finland (if you live in Saint Petersburg,
like me, that's close), and in any case avoid Aeroflot.
