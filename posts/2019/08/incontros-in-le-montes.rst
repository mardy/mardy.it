.. title: Incontros in le montes
.. slug: incontros-in-le-montes
.. date: 2019-08-07 19:47:31 UTC+03:00
.. tags: interlingua,Italia,natura
.. category: 
.. link: 
.. description: 
.. type: text

Io vos presenta alicun breve videos con animales que io incontrava hodie
e heri in le montanias del `Cadore
<https://en.wikipedia.org/wiki/Cadore>`_, ubi io passa alicun dies de
vacation.

.. youtube:: u-Vy2NiN4VE
   :align: center

In le sentiero que porta al summitate del monte Tudaio, io incontrava
iste sympatic serpente. Illo non es venenose, ma io non lo sapeva e
esseva un poco espaventate.

.. youtube:: yxVMmuf7fwg
   :align: center

Iste capriolos se promenava proxime al cemeterio de Lorenzago di Cadore.
Circa 25 annos retro io sovente visitava iste loco, ma numquam videva
ulle capriolo. Ma iste vice io esseva multo fortunate.

.. youtube:: f1UXX7orh_8
   :align: center

E, pro finir, duo scuriolos. Illos me pare differente de los que io
sovente vide in Helsinki o in Sancte Petroburgo, perque lor pancia es
marcatemente blanc. Forsan anque le pancia del scuriolos nordic es clar,
e io simplemente non lo notava perque le resto del corpore es plus clar.

Vole ben excusar me pro le mal qualitate del registration: mi photocamera non es multo bon pro capturar videos, e le lente que io usava ha un *auto-focus* multo problematic.

