<!--
.. title: Migrating to a new Mastodon instance
.. slug: migrating-to-a-new-mastodon-instance
.. date: 2019-08-20 17:39:00 UTC+03:00
.. tags: amicos,english,informatica,kdeplanet,personal,planetmaemo
.. category: 
.. link: 
.. description: 
.. type: text
-->

The wonders of improvised Mastodon instances: [one node](https://soc.ialis.me)
disappears after an outage caused by a summer heatwave, leaving its users no
way to migrate their data or to notify their followers.

After about one month of waiting for the node to come up or give some signals
of life, I've decided to create a new account on another instance. If you use
Mastodon and you were following me, please forgive me for the annoyance and
[follow me again here](https://mstdn.io/@mardy).
