.. title: Ancora un “serpente cec”
.. slug: ancora-un-serpente-cec
.. date: 2019-08-08 22:00:46 UTC+03:00
.. tags: interlingua,Italia,natura
.. category: 
.. link: 
.. description: 
.. type: text

Hodie io incontrava un altere “`serpente cec
<https://it.wikipedia.org/wiki/Anguis_fragilis>`_” (italiano:
*orbettino*).

.. youtube:: E3VtcAC-RTU
   :align: center

Secundo le zoologos, isto non es un ver serpente, ma un
*lacerta sin pedes*. Io non es secur que io comprende a fundo le differentia, ma isto non importa.
