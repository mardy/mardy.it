<!--
.. title: Rifugio Carducci
.. slug: refugio-carducci
.. date: 2019-08-11 16:13:46 UTC+03:00
.. tags: italiano,Italia,natura,turismo
.. category: 
.. link: 
.. description: 
.. type: text
-->

La passeggiata da
[Auronzo](https://it.wikipedia.org/wiki/Auronzo_di_Cadore) al [rifugio
Carducci](http://rifugiocarducci.eu/), che abbiamo compiuto ieri, è stata una
delle più belle escursioni che io ricordi.
E certamente anche una delle più difficili ed emozionanti, sia per il notevole
dislivello (più di 1300 metri — il rifugio si trova a 2297 metri d'altezza),
sia per alcuni tratti molto esposti (ovvero con il sentiero costeggiante un
precipizio). Ma permettetemi di andare con ordine.

<figure style="margin: 0 auto; width: 512px">
  <img src="/archivos/imagines/blog/carducci-tabula.jpg" class="shadow-sm" />
  <figcaption>La tabella al inizio del percorso.</figcaption>
</figure>

L'escursione inizia in località “Pian de la Velma” (non chiedetemi cosa significhi), poco più su di Auronzo di Cadore, a circa 960 metri di altezza.
Dopo alcuni metri di cammino, abbiamo incontrato il cartello fotografato qui
sopra, che ci informava che proseguire il percorso (segnato dal numero 103) era
vietato a causa di una frana occorsa nel 2015.
Tuttavia, visto che il personale del nostro albergo ci aveva consigliato questa
escursione, senza peraltro menzionare questo divieto, decidemmo di non prendere
questo cartello troppo sul serio, e di continuare il cammino.

Dopo una decina di minuti ci superò un altro escursionista, che ci confermò
l'agibilità del sentiero, aperto solo una settimana prima. Il cartello non era
ancora stato rimosso, forse per dimenticanza.

<figure style="margin: 0 auto; width: 512px">
  <img src="/archivos/imagines/blog/carducci-ponte.jpg" class="shadow-sm" />
  <figcaption>Il ponte che collega il nuovo percorso col vecchio.</figcaption>
</figure>

Di fatto noi avevamo già trovato il punto dove il sentiero 103 originario era
stato intenzionalmente bloccato con alcuni rami, e dove partiva la deviazione
per un altro sentiero che sembrava uno seguito dai cercatori di funghi.
Questo sentiero saliva abbastanza rapidamente, e a un certo punto le molte
radici che emergevano dal terreno rivelavano come si trattasse di un sentiero
scavato molto recentemente. Anche quando il sentiero entrava nel bosco, era
evidente che non era stato percorso da molte persone, perché il terreno era un
po' instabile. In un paio di occasioni il bosco si apriva e il sentiero si
appoggiava su dei ghiaioni in costa, che attraversammo con un po' di paura (si
veda il video alla fine dell'articolo, a 29 secondi).

<figure style="margin: 0 auto; width: 340px">
  <img src="/archivos/imagines/blog/carducci-nove.jpg" class="shadow-sm" />
  <figcaption>Un tratto del nuovo sentiero 103. Si possno notare i rami recentemente tagliati per permettere il passaggio.</figcaption>
</figure>

Il sentiero si sviluppa lungo la val Giralba, e in alcuni punti incrocia
l'omonimo torrente che da il nome alla valle (o viceversa?).  Il fragore del
torrente era ben udibile lungo quasi tutto il percorso, e in alcuni punti si
aveva la possibilità di bere e rifornirsi d'acqua.

<figure style="margin: 0 auto; width: 512px">
  <img src="/archivos/imagines/blog/carducci-torrente.jpg" class="shadow-sm" />
  <figcaption>Il torrente che scava la valle.</figcaption>
</figure>

Una bella e alta cascata, che scendeva lunto la parete rocciosa, era a tratti
visibile, ma preferimmo evitare di deviare dal percorso per tentare di
raggiungerla — la nostra mèta era ancora distante.
 
<figure style="margin: 0 auto; width: 340px">
  <img src="/archivos/imagines/blog/carducci-cascata.jpg" class="shadow-sm" />
  <figcaption>Il torrente e, in alto sulla montagna, la cascata.</figcaption>
</figure>

A 1335 metri di altezza (se il GPS della mia macchina fotografica è corretto)
si trova un ponte di legno, di recente costruzione, che attraversa il torrente;
dopo alcuni metri ci si ricongiunge al vecchio sentiero 103 originario, che si
seguirà da qui in avanti.

La differenza tra il terreno del nuovo e del vecchio sentiero ci apparve
evidente: il terreno del vecchio sentiero era più duro e dava l'impressione di
essere più stabile (benché, a dire il vero, nemmeno i nuovi tratti del sentiero
si danneggiavano sotto i nostri passi).
Ma in alcuni punti anche questa parte del sentiero sembrava essere stata
interessata da frane.

<figure style="margin: 0 auto; width: 512px">
  <img src="/archivos/imagines/blog/carducci-petras.jpg" class="shadow-sm" />
  <figcaption>Un punto parzialmente ostruito da una frana.</figcaption>
</figure>

Un luogo particolarmente piacevole era un rivolo d'acqua immerso nel verde: si
trova a 1823 metri di altezza ed è indicato da un cartello con su scritto
“acqua” posto sul terreno di fianco al sentiero. Questo luogo sembrava preso da
qualche fiaba o da un dipinto fantasy, tanto era adornato di muschi e di mille
fiorellini. Lo potete vedere a 2:28 minuti del video in basso.

<figure style="margin: 0 auto; width: 340px">
  <img src="/archivos/imagines/blog/carducci-monte.jpg" class="shadow-sm" />
  <figcaption>La vista si faceva più bella man mano che si saliva.</figcaption>
</figure>

Con l'aumentare dell'altezza, l'aspetto del sentiero cambia: i pini divengono sempre più rari, e anche i cespugli di
[pino
mugo](https://it.wikipedia.org/wiki/Pinus_mugo) infine lascieranno il posto a prati verdi e pieni di fiori. È cursioso come nemmeno l'erba riesca a crescere oltre i 15-20 centimetri, a certe altezze.

<figure style="margin: 0 auto; width: 512px">
  <img src="/archivos/imagines/blog/carducci-prato.jpg" class="shadow-sm" />
  <figcaption>A 2000 e più metri di altezza, si trovavano moltissimi di questi fiori dai petali lanosi. Non conosco il nome di questi fiori —
suggerimenti sono benvenuti nei commenti.</figcaption>
</figure>

Già potevamo scorgere il rifugio, benché si trovasse ancora a 300 metri più in alto.
Questo ci confortava, ma allo stesso tempo fummo costretti a osservare come la
vicinanza era soltanto apparente: nonostante si faticasse e si salisse, il
rifugio sembrava mantenersi alla stessa distanza da noi.

<figure style="margin: 0 auto; width: 512px">
  <img src="/archivos/imagines/blog/carducci-flores-jalne.jpg" class="shadow-sm" />
  <figcaption>Anche questi fiori gialli erano molto comuni (e pure di essi ignoro il nome).</figcaption>
</figure>

Nella parte finale del sentiero i prati cedono spesso il passo a ghiaioni.
Questi sono generalmente instabili (e soprattutto gli escursionisti più veloci
ne fanno rotolare parecchio, di ghiaino) e la forma del sentiero può cambiare
col tempo. D'altro canto, il rifugio è così vicino che ci sono diversi
possibili percorsi per arrivarci, a quel punto; il problema è che non tutte le
varianti sono ugualmente facili, quindi bisogna stare attenti a scegliere la
via più adatta alle proprie capacità.

<figure style="margin: 0 auto; width: 512px">
  <img src="/archivos/imagines/blog/carducci-lontan.jpg" class="shadow-sm" />
  <figcaption>2120 metri. Il rifugio è ben visibile (si ingrandisca la
foto per vederlo). Da quest'altezza in poi, il sentiero diventa più
ghiaioso e instabile.</figcaption>
</figure>

Finalmente arrivammo al rifugio. Prendemmo un cappuccino e una cioccolata
calda, accompagnati da due fette di torta. Dopo questa fatica (che durava
quattro ore e mezza, contando le molte pause fotografiche), il premio era
ben meritato!

<figure style="margin: 0 auto; width: 512px">
  <img src="/archivos/imagines/blog/carducci-refugio.jpg" class="shadow-sm" />
  <figcaption>La nostra mèta.</figcaption>
</figure>

Ho compilato un breve video con varie registrazioni effettuate durante l'ascesa. Forse saranno di aiuto a chi volesse intraprendere la stessa escursione, per farsi un'idea di ciò che lo attende:

<center>
<iframe width="560" height="315" src="https://www.youtube.com/embed/hQ7EFIlx1aY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</center>
