<!--
.. title: On Richard Stallman and people who cannot read
.. slug: on-richard-stallman-and-people-who-cannot-read
.. date: 2019-09-17 19:38:26 UTC+03:00
.. tags: english,linguas,informatica,Linux,planetmaemo
.. category: 
.. link: 
.. description: 
.. type: text
-->

We live in strange times. People are so filled with hatred and prejudices that
their brain becomes unable to parse the simplest sentences. I take this issue
to heart, because it could happen to anyone — it has happened to me before
(luckily, only in private online conversations), where an acquaintance of mine
accused me of saying things I never said. And it happens to famous people all
the time. Guys, just because you hate person X, you should not skip over parts
of their speech or suppress context in order to make it look like they said
something terrible or stupid, when they didn't.

Now it happend to Richard Stallman, with a whole wave of hateful people
accusing him of saying something that he didn't say. Let's start with the [VICE
article](https://www.vice.com/en_us/article/9ke3ke/famed-computer-scientist-richard-stallman-described-epstein-victims-as-entirely-willing),
titled "Famed Computer Scientist Richard Stallman Described Epstein Victims As
'Entirely Willing'", which insists in quoting only two words out of Stallman's
sentence:

> Early in the thread, Stallman insists that the “most plausible scenario” is
> that Epstein’s underage victims were “entirely willing” while being
> trafficked.

Except that he didn't say that. Why not quote the whole sentence? It's not such
a long sentence, really! Just follow [the link to the
source](https://medium.com/@selamie/remove-richard-stallman-fec6ec210794),
which provides a complete excerpt of Stallman's words:

> We can imagine many scenarios, but the most plausible scenario is that she
> presented herself to him as entirely willing. Assuming she was being coerced
> by Epstein, he would have had every reason to tell her to conceal that from
> most of his associates.

Now, English is not my native language, but I read it well enough to understand
that “to present oneself as” and “to be” are different expressions having very
different meanings (and, in most context, actually opposite ones!).  You don't
need to be Shakespeare to understand that. You only need to either *stop
hating* or, if you really cannot help it, at least *stop projecting your
prejudices onto the people you hate*. Hate makes you blind.

It's sad to see otherwise intelligent people take [stupid
decisions](https://blog.halon.org.uk/2019/09/gnome-foundation-relationship-gnu-fsf/)
because of such misunderstandings.

I for one, stand in solidarity with Richard Stallman and with the English language.

(please note that this is not an endorsement of everything Stallman might have
said in the past; I don't follow him that closely, and it may be that he also
happened to say terrible things in this very thread. I'm only commenting *this
very specific issue*, and I know that *in this very specific issue* he's being
wrongly accused)
