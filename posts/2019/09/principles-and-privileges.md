<!--
.. title: Principles and privileges
.. slug: principles-and-privileges
.. date: 2019-09-04 21:17:22 UTC+03:00
.. tags: philosophia,kdeplanet,english
.. category: 
.. link: 
.. description: 
.. type: text
-->

I intended to write a reply to [Jos Poortvliet's blog post
“Principles”](https://blog.jospoortvliet.com/2019/09/principles.html), and I
swear I did enter it as a comment to his Google-powered blog, but something
went wrong and when I tried to publish it I lost all what I'd written. So I
decided to let some hours pass, ponder a bit more over the subject, and write a
more exhaustive answer on my own blog.

<figure style="margin: 0 auto; width: 512px">
  <img src="/archivos/imagines/blog/a-morte-il-fascio.jpg" class="shadow-sm" />
  <figcaption>“A morte il fascio” — <i>death to fascism</i>.</figcaption>
</figure>

Before going into the core of the topic, I want to point out that when I start
thinking over the questions posed by Jos my thought can't help jumping to the
Marxist concept of classes: the very fact that you are considering the
possibility of refusing a business because of principles means that you can
*afford* doing that, while most people just can't. The same applies to similar
questions, such as declining a well paying job because of ethical reasons, or
boycotting some products, eating healthy food, crossing the ocean on a clean
yacht instead of polluting the air by plane, etc.: being able to make these
choices already imply that *you are in a privileged position* (in some cases,
extremely privileged!).

Why am I saying that? Certainly not to diminish the value of your ethical
choices! By all means, please continue to do your best to change things!
However, while you can proudly look at yourself in the mirror, please *don't
look down on people who don't seem to be as responsible as you*: they might
simply not have the choice, or not be able to *afford* it. Or maybe — that's
also possible — they are not aware that they can make this choice.

So, besides being excellent yourself, one thing you could do in order to have a
much bigger impact is to help other people get into a condition such that they
would also be able to afford to take the brave choices you go proud of.

That was a long premise indeed, but it might help to understand the rest of my
answer.

Which is: yes, I would (to a question like “Would you work with a company that
builds rockets and bombs to earn money for Nextcloud development?”). Which
might surprise many, especially those of you who know that I'm a hardcore
pacifist. In reality, my answer would be much more faceted, depending on who
you are and on the weight of your possible refusal. For example, if I were a
superstar with a million followers, then I'd definitely refuse: such a
decision, made public, would have a strong propagandistic effect. But when you
are an ordinary software developer, with a dozen friends (or even a few
hundreds) and no direct channel to the media, what would be the outcome of your
refusal? It might impress a handful of people, maybe, but the evil company
would easily find someone else to replace you and you'd lose a well paying job
(which could help you to *afford* making more ethical choices). But more
important than that, you would have cut out a communication channel to the
people who might benefit the most from your presence — and from your message.

I would answer “yes” because I'd like to take a chance to know what people on
the other side of the fence think. What are their reasons? Are they aware of
the issues that bother me? It's possible that I'm overestimating myself, but
even if there was a tiny, microscopic chance of instilling a doubt, of
delivering a message or providing a good example, I would leave no stone
unturned to grasp this chance. Of course, I'm not thinking of “converting” the
management of a corporation; but some of my peers inside that company might
start questioning things. And then, of course, you can always leave, when
enough is enough; but joining and then leaving a deal has a higher impact than
no joining at all. Or, if you like metaphores, *in order to clean something,
you first have to get your hands dirty*.

Before reading Jos's post, I've been considering another question, which is
vaguely related. Suppose that you were the owner of a bakery, and that you knew
that one of your customer is a nazi. Would you still sell bread to him? Suppose
that he's the only nazi in town: here you don't even need to worry about your
business, because even if you lost that customer, it wouldn't have but a very
minor impact on you.

My answer here is along a similar line as the previous one: yes, I would still
sell my bread to him. One reason is that this risks being another rabbit hole:
a nazi might hold the most disgusting opinions and views, but then — if you
think about it — many people do. Maybe they don't hate Jews, but Roms, Muslims,
[lawyers, or the French](https://youtu.be/2ETCM90yHiY?t=81); maybe they don't
want to colonize Africa, but Greece; maybe they don't physically torture
dissidents, but keep them rotting in a jail without charges (or with made-up
charges). Even without going to these extremes, there's simply the fact that we
are all imperfect: both in our opinions and in our actions. Would you refuse
selling bread to a guy holding nazi views, but otherwise honest and
well-behaving, while selling it to someone having well-balances opinions, but
who evades millions in taxes?

But the main reason why I'd sell my bread to a nazi is that, really, I'd like
to get to know him. I would like to learn why he holds those views, because I
think that understanding is the first step towards correction. This is probably
matter for a future post, but I'm convinced that all this censorship in the
social networks (yes, especially on the federated ones!) is detrimental to the
fight against fascism: if we won't even know where and who the fascists are,
how can we have any hopes of winning the fight?

So, let's build bridges, let's talk, let's try to understand each other's
points of view, and find exactly *why* we see things differently. And this is
much more effective when done at a personal level, one to one, rather than with
public big proclaims — which, more often than not, have the only effect of
polarizing the field even more.

In short — and I guess this is my answer to Jos — try to make a difference with
those who are closer to you. Accept the deal with the evil corporation, and let
everyone of your friends and colleagues know how much you are suffering because
of that. Let your peers in that company know you for what you are worth, but
don't hide your feelings. Maybe, who knows, the day you leave the deal, they
will also decide that it's time to make a big choice in their life?
