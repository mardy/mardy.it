<!--
.. title: MiTubo 0.3 brings basic RSS support
.. slug: mitubo-03-brings-basic-rss-support
.. date: 2021-09-27 23:44:53 UTC+03:00
.. tags: Linux,Qt,Ubuntu,english,informatica,kdeplanet,programmation,AppImage,MiTubo
.. category: 
.. link: 
.. description: 
.. type: text
-->

I just pushed MiTubo 0.3 to the [Ubuntu Touch app
store](https://open-store.io/), added a link to the AppImage package in [the
Releases page](https://gitlab.com/mardy/mitubo/-/releases/VERSION_0.3) and
later during the night the Launchpad builders should import it and build it for
[Ubuntu 18.04 and 20.04](https://launchpad.net/~mardy/+archive/ubuntu/mitubo).

This release adds basic support for RSS feeds. One just needs to type the
address of an RSS (or Atom) feed in the search bar and the "Search" button will
transform itself into a "Subscribe" one. Unfortunately no discovery mechanism
is implemented yet, so one cannot just enter the address of a webpage and
expect MiTubo to find the feed URL(s); I plan to bring that in a future
release, but for the time being you'll have to enter the exact address of the
RSS feed.

Also, there's no tracking of which videos are new and which ones have been
already watched. Again, that's material for a next release.

But as a start, feed are remembered in the configuration, and the next time you
open up MiTubo you'll see them in the main page, and the most recent videos
will be shown.

As usual, you're welcome to report issues [in
gitlab](https://gitlab.com/mardy/mitubo/-/issues).
