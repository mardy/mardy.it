<!--
.. title: La premiazione di Maria Shukshina al Cremlino
.. slug: la-premiazione-di-maria-shukshina-al-cremlino
.. date: 2021-09-07 11:42:03 UTC+03:00
.. tags: Russia,cinema,cultura,information,italiano,politica,reflexiones,scientia,societate,video
.. category: 
.. link: 
.. description: 
.. type: text
-->

Il 31 agosto di quest'anno all'attrice russa Maria Shukshina è stata conferita
una medaglia per “servizio alla Patria” nell'ambito della cultura. A
consegnarle la medaglia è stato il primo ministro russo Michail Mishustin, che
probabilmente però non si aspettava dall'attrice un discorso di questa portata:

<video id="video" controls preload="metadata">
   <source src="/archivos/videos/Premiazione Maria Shukshina.mp4" type="video/mp4">
   <track label="Italiano" kind="subtitles" srclang="it" src="/archivos/videos/Premiazione Maria Shukshina.vtt" default>
</video>

L'ho sottotitolato per voi con tanto amore, e spero con pochi errori. Cercate
di non farvi troppo distrarre dal volto impassibile del primo ministro, anche
se effettivamente questo da solo varrebbe la visione del video.

La domanda per voi, chiaramente, è questa: vi sono stati episodi analoghi in
Italia, in cui un personaggio dello spettacolo si sia rivolto così a Draghi,
davanti alle telecamere nazionali?
