.. title: Cammino quindi penso - 2021-06-07 - Il senso di una discussione
.. slug: cammino-quindi-penso-2021-06-07-il-senso-di-una-discussione
.. date: 2021-06-07 22:35:30 UTC+03:00
.. tags: italiano,video,Cammino quindi penso,cultura,reflexiones
.. category: 
.. link: 
.. description: 
.. type: text

Qualche consiglio su come impostare una discussione in modo che risulti un
dialogo civile e possibilmente pure fruttifero.

.. youtube:: fdWsgDAqFL0
   :align: center
