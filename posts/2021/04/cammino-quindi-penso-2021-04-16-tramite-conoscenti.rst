.. title: Cammino quindi penso - 2021-04-16 - Tramite conoscenti
.. slug: cammino-quindi-penso-2021-04-16-tramite-conoscenti
.. date: 2021-04-16 22:53:58 UTC+03:00
.. tags: italiano,video,Cammino quindi penso,informatica,societate
.. category: 
.. link: 
.. description: 
.. type: text

Sull'importanza di una rete di conoscenze, e di come la didattica a distanza
sia un ostacolo alla sua formazione.

.. youtube:: iU5EqPyY7ko
   :align: center

