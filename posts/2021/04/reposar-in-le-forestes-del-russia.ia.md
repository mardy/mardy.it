<!--
.. title: Relaxar se in le forestes del Russia
.. slug: reposar-in-le-forestes-del-russia
.. date: 2021-04-06 18:02:39 UTC+03:00
.. tags: interlingua,Russia,natura,turismo,video
.. category: 
.. link: 
.. description: 
.. type: text
-->

Al fin de martio, in compania de mi amate consorte, io passava un par de dies in un casetta disperse inter le forestes del Russia, a un centeno de chilometros a nord de Sancte Petroburgo. Illo non esseva vermente “disperse”, viste que illo esseva parte de un village de relaxation constituite de un decena de casettas simile, ma illo esseva comocunque lontan de botecas e de centros habitate. Con un breve promenada de un par de minutas on poteva attinger le ripas del laco Vuoksa, que nos jam habeva cognoscite le estate passate.

Le casetta esseva parve, non plus de cinque metros per latere, ma illo esseva equippate con aqua e electricitate. Io filmava un breve video pro conseniar lo al posteritate (vole ben excusar me pro mi enorme facie barbose al initio):

<center>
<iframe width="560" height="315" src="https://www.youtube.com/embed/j6O29pM8eZU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</center>
