.. title: New desktop application: MiTubo
.. slug: new-desktop-application-mitubo
.. date: 2021-04-14 23:11:16 UTC+03:00
.. tags: Linux,Qt,Ubuntu,english,informatica,kdeplanet,programmation,AppImage,MiTubo
.. category: 
.. link: 
.. description: 
.. type: text

I've recently started a new project, to enable me to playback videos from
online sources in a desktop application: Mitubo.

Here's a terrible video I quickly made to show what MiTubo can currently do:

.. youtube:: P2pdGGTOHEg 
   :align: center

MiTubo is currently available for Ubuntu 18.04 and later from `this PPA
<https://launchpad.net/~mardy/+archive/ubuntu/mitubo>`_ and for all other
distributions from `here as an AppImage package
<https://gitlab.com/mardy/mitubo/-/releases>`_.

You are very welcome to try it out and report issues at `its bug tracker
<https://gitlab.com/mardy/mitubo/-/issues>`_. I know that there are many of
them, but regardless of them the program is still useful to me, so I hope it
can be useful for other people too.
