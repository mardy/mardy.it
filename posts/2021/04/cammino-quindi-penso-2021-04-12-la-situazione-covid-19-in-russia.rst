.. title: Cammino quindi penso - 2021-04-12 - La situazione COVID-19 in Russia
.. slug: cammino-quindi-penso-2021-04-12-la-situazione-covid-19-in-russia
.. date: 2021-04-13 09:14:11 UTC+03:00
.. tags: italiano,video,Cammino quindi penso,Sancte Petroburgo
.. category: 
.. link: 
.. description: 
.. type: text

Dopo una pausa di due anni e due giorni, riprendo (non so per quanto!) a
dilettarvi con la serie “`Cammino quindi penso`_”, mostrandovi com'è la
situazione COVID-19 qui in Russia, a San Pietroburgo.

.. youtube:: dFeeLqYEm9A 
   :align: center

.. _Cammino quindi penso: link://tag/cammino-quindi-penso
