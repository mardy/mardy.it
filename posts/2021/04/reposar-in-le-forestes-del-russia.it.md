<!--
.. title: Rilassarsi tra i boschi, in Russia
.. slug: reposar-in-le-forestes-del-russia
.. date: 2021-04-06 18:02:39 UTC+03:00
.. tags: italiano,Russia,natura,turismo,video
.. category: 
.. link: 
.. description: 
.. type: text
-->

Alla fine di marzo, in compagnia della mia amata consorte, ho passato un paio
di giorni in una casetta sperduta tra i boschi della Russia, a un centinaio di
chilometri a nord di San Pietroburgo. “Sperduta” si fa per dire, visto che era
parte di un villaggio vacanze costituito da una decina di casette simili, ma
era comunque lontana da negozi e centri abitati. Con una breve passeggiatina di
un paio di minuti si raggiungevano le rive del lago Vuoksa, che già avevamo
conosciuto l'estate scorsa.

La casetta era piccina, non più di cinque metri per ogni lato, ma attrezzata
con acqua ed elettricità. Ho girato un piccolo video per consegnarla alla
posterità (mi scuso per il mio faccione barbuto iniziale):

<center>
<iframe width="560" height="315" src="https://www.youtube.com/embed/mTLN7i-8QAE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</center>
