.. title: Cammino quindi penso - 2021-05-18 - Palestina, conflitto mal raccontato
.. slug: cammino-quindi-penso-2021-05-18-palestina-conflitto-mal-raccontato
.. date: 2021-05-18 12:54:36 UTC+03:00
.. tags: italiano,video,Cammino quindi penso,information,actualitate
.. category: 
.. link: 
.. description: 
.. type: text

I nostri mezzi di informazione raccontano gli eventi in Palestina in maniera
distorta, mettendo sullo stesso piano l'aggressore e l'aggredito — talvolta,
addirittura, imputando a quest'ultimo le vittime di questo conflitto.

.. youtube:: -Hk2L4bThbw
   :align: center

