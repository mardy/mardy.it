<!--
.. title: Un libro per riflettere: Donbass
.. slug: un-libro-per-riflettere-donbass
.. date: 2021-08-07 17:41:44 UTC+03:00
.. tags: Russia,Ucraina,actualitate,cultura,italiano,politica,reflexiones
.. category: 
.. link: 
.. description: 
.. type: text
-->

Il giornalista Vittorio Nicola Rangeloni ha recentemente pubblicato un libro, “Donbass — Le mie cronache di guerra”, in cui racconta la sua esperienza in Ucraina, paese in cui ha trascorso buona parte degli anni tra il 2014 ed oggi. A differenza dei suoi rapporti dal fronte, per il cui rigore ed obiettività si è fatto sempre apprezzare da chi lo segue sulle reti sociali, questo libro è una sequenza di pagine vive, in cui i numeri delle statistiche sulle vittime della guerra lasciano il posto alle storie di queste persone, che l'autore raccoglie e ci fa rivivere tramite la sua penna.

È un libro che non può che produrre emozioni forti e lo fa con un linguaggio colto e fluido, in cui non mancano passaggi che non esiterei a definire poetici, in particolare per come viene dipinto il contrasto tra il futuro che i protagonisti del racconto si aspettano (o che noi lettori siamo indotti ad auspicare per loro) e la triste realtà che vive chi trascorre la sua vita a poche centinaia di metri da un fronte di guerra. Rangeloni non usa parole forti, non descrive apertamente le sue emozioni, eppure (o forse proprio per questo) tramite la viva descrizione delle sue giornate e dei suoi incontri riesce a farle rivivere benissimo nel lettore, che a tratti si trova spaesato, a tratti impaurito, a tratti angosciato.

Civili o combattenti, gli amici che l'autore ci fa conoscere ed amare vengono cancellati dalla pagina con la stessa rapidità dei proiettili che hanno posto fine alle loro vite. Il susseguirsi delle introduzioni alle nuove conoscenze ed amicizie del giornalista, le brevi descrizioni delle loro case danneggiate dai colpi dei mortai, le tazze di tè offerte al visitatore da chi ha poco o nulla da mangiare, e, talvolta, la notizia della loro prematura scomparsa, ci portano in una dimensione dove i freddi bollettini di guerra (pur spesso [taciuti dai mezzi di informazione occidentali](link://slug/what-you-dont-hear-about-ukraine)) non riescono ad entrare: è la dimensione umana della guerra, in cui non vi sono numeri ma volti, emozioni e speranze.

Il valore del libro sta principalmente in questo: è un'arma contro l'indifferenza, contro la convinzione di saperne abbastanza di un conflitto che, dimenticato dalle televisioni, sembra non aver più nulla da dirci. Mentre nel cuore dell'Europa si continua a morire.

Ho letto con inconsueta (per me) avidità, e spesso con occhi lucidi, le più di trecento pagine di questo libro, e apprezzato la piccola raccolta fotografica che ne costituisce le ultime pagine. Come avrete già capito, il fatto che abbia preso la briga di parlare di questo libro significa che ne consiglio caldamente la lettura.


#### Dove aquistarlo

“Donbass — Le mie cronache di guerra” è edito da [Idrovolante edizioni](http://www.idrovolanteedizioni.it/libri/donbass-le-mie-cronache-di-guerra/) ed è anche disponibile in molte altre librerie in linea.
