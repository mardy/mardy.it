<!--
.. title: Recepta del crostata de piras
.. slug: recepta-del-crostata-de-piras
.. date: 2021-08-19 19:37:59 UTC+03:00
.. tags: cocina,interlingua
.. category: 
.. link: 
.. description: 
.. type: text
-->

<video style="float: right;" width="204" controls>
  <source src="/archivos/videos/Crostata di pere.mkv" type="video/mp4">
  <source src="/archivos/videos/Crostata di pere.mkv" type="video/x-matroska">
</video>

Le arbores del *dacia* produceva multe piras iste anno, e infortunatemente
illos non se conserva ben pro un longe tempore. Alora, pro non lassar los
guastar, io decideva de utilisar los pro facer
[crostatas](https://it.wikipedia.org/wiki/Crostata), un dulce que me place
multo, ma que io nunquam ha cocinate. Ben, il ha un prime vice pro toto.

Le recepta del crostata de piras
================================

Io trovava un recepta pro le crostata de pomos in internet, e lo adaptava un
poco.

#### Ingredientes

- 300 grammas de farina
- 140 grammas de sucro
- 1 parve coclear de levatura (it: lievito, en: yeast)
- 2 ovos (on usa uno complete, e un [vitello](https://it.wikipedia.org/wiki/Tuorlo) del altere)
- 100 grammas de butiro
- 1 coclear de oleo de tornasol (it: girasole, en: sunflower)
- 500 grammas de piras a peciettas

#### Preparation

On pone le farina, le sucro e le levatura in un salatiera (o altere contenitor
bastante ample pro poter laborar le impasto con le manos), e on los misce con
un coclear. Postea on adde le oleo e le butiro, taliate a pecias (le dimension
non importa).
On labora con le digitos del mano, usque quando le butiro se ha reducite in
partes plus parve, e le impasto ha acquirite un consistentia granulose. Alora
on adde le ovo e le vitello (isto es le parte jalne central) del altere ovo (le
resto del secunde ovo on non va utilisar lo), e on misce e impasta con le
manos, fin a quando le massa non plus es collose (le manos resta munde quando
on lo labora).

A iste puncto on inveloppa le impasto in un pellicula transparente (o lo pone
in un sacchetto pro alimentos) e on lo pone in le refrigerator (*non in le
congelator!*), ubi illo debe reposar pro al minus 30 minutas. Intertanto, vos
non va reposar: vos debe taliar le piras in parve partes, usque le peso del
fructa arriva a circa 500 grammas.

<figure style="margin: 0 auto; width: 512px">
  <img src="/archivos/imagines/blog/Crostata di pere.jpg" class="shadow-sm" />
  <figcaption>Le prime crostata de mi production</figcaption>
</figure>

Quando vos ha finite (ma non prima que 30 minutas ha passate!), on recupera le
massa del refrigerator e on remove un tertio o un quarto de illo, que essera
utilisate postea. Le parte le plus grande on va poner sur un folio de carta pro
furno, e on lo extende in un forma approximatemente circular, del spissor de
circa 4 millimetros. Le diametro debera esser sufficiente pro coperir tote le
fundo del tortiera, plus 2 centimetros pro le bordos del torta. Post que le
impasto es in le tortiera, on face alicun foros con le puntas de un furchetta
(non demanda a me le ration — io non lo sape!), e on versa le fructa. Alora, on
prende le parte del impasto que on removeva precedentemente, e on lo extende e
talia in bandas de un centimetro o poco plus de largor, que on pone super le
strato de fructa pro formar le texitura a quadrettos.

In fin, on pone le tortiera in le furno, a 150 grados de calor, pro circa 40
minutas.

#### Variantes

In vice que piras, on pote obviemente utilisar un altere fructo de vostre
gradimento; si le fructo es dulce, alora il poterea esser un bon idea adder
succo de lemon — isto es particularmente consiliate con le pomos, anque pro
evitar que illos deveni nigre.

Bon appetito!
