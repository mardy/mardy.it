<!--
.. title: Imaginario 0.10
.. slug: imaginario-010
.. date: 2020-11-12 19:12:36 UTC+03:00
.. tags: english,kdeplanet,planetmaemo,imaginario
.. category: 
.. link: 
.. description: 
.. type: text
-->

Today I released [Imaginario
0.10](http://imaginario.mardy.it/news/imaginario-010-is-out/). No bigger
changes there, but two important bugfixes.
