<!--
.. title: Quando le scientia manca de argumentos
.. slug: quando-le-scientia-manca-de-argumentos
.. date: 2020-10-27 22:25:39 UTC+03:00
.. tags: actualitate, etica, information, interlingua, philosophia, politica, reflexiones, scientia
.. category: 
.. link: 
.. description: 
.. type: text
-->

E ben, io lo confessa: io es un de ille negationistas qui non crede in le
scientia. Al minus, si on associa le parola “scientia” con le Organisation
Mundial del Sanitate (OMS, o WHO in anglese).

Jam desde plure annos io ha perdite le _fide_ in iste organisation, ma un nova
que io legeva recentemente me ha stimulate a declarar publicamente mi adversion
pro le OMS: in iste articulo, le capite del OMS
[declarava](https://www.israelnationalnews.com/News/News.aspx/288950) que le
idea de attinger le _immunitate de grege_ per relaxar le limitationes al
diffusion del virus COVID-19 **non es ethic**.

Le referimento al ethica me surprendeva. Io non poteva recordar, que io unquam
audiva le OMS parlar de ethica, ante iste momento. An le OSM non es un
organisation scientific? Si illo lo es, le evocation del ethica como
justification de un argumentation sembla demonstrar que il non ha factos
scientific pro supportar le these.

Ma anque si nos concede que le personal del OMS pote — a titulo purmente
personal — facer considerationes de charactere ethic, como pote nos conciliar
iste declaration de Tedros Adhanom Ghebreyesus con le silentio super un
quantitate de practicas e experimentos scientific, que multe personas considera
esser contrari al ethica? Esque experimentar le vaccinos super populationes del
tertie mundo es ethic? O experimentar los super le stratos plus indigente del
population (_voluntarimente_, on dice, ma on les compensa con alicun beneficios) es
ethic? Reciper enorme contributiones financiari del companias que developpa
medicinas e vaccinos, e pronunciar se super themas medic e virologic, es isto
ethic?

> Contra potentes nemo est munitus satis;  
> si vero accessit consiliator maleficus,  
> vis et nequitia quicquid oppugnant, ruit.

_(Fedro, libro secunde, capitulo sexte)_
