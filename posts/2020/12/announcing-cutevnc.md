<!--
.. title: Announcing CuteVNC
.. slug: announcing-cutevnc
.. date: 2020-12-13 16:38:48 UTC+03:00
.. tags: QBS,QML,Qt,Ubports,Ubuntu,english,informatica,kdeplanet,programmation
.. category: 
.. link: 
.. description: 
.. type: text
-->

Having a single screen connected to my PC, I was wondering if I could use my
nice [BQ M10 tablet](https://en.wikipedia.org/wiki/BQ_Aquaris_M10) running
UBports as an additional screen. The idea was that if I could convince the X
server to create a new virtual display, and then run the x11vnc server on it, I
could then connect the tablet to it and enjoy my desktop on two screens. Yes,
the M10 has not such a big display, but it couls still be useful to host a
window with a mail or a chat client, so that I would not need to switch between
applications just to check if new emails or chat messages have arrived (and I
hate instant notifications, so I tend to disable them in every program that
allows me to).

Anyway, I digress. I found some articles, and specifically [this
question](https://unix.stackexchange.com/questions/156412/vnc-server-as-a-virtual-x11-monitor-to-expand-screen),
that show how one can add a new virtual screen and connect to it via VNC. I
tried it and it kind of worked, but there were some visual artifacts that
annoyed me (most likely due to the AMDGPU drivers), so I'll defer the creation
of a demo video till when I'll have figured out a better setup. But the main
point is that the idea, in principle, can work.

# CuteVNC

So I sat down and started writing a VNC client for UBports, since it appears
that there wasn't one yet in the store. I use the past tense, because after a
week or so of work, [CuteVNC is
there](https://open-store.io/app/it.mardy.lomiri-vnc). It's still a very alpha
version, and does seem to get confused if the VNC server is using caching, but
overall it's working. Its impressive feature list includes:

* Can connect to a VNC server, and show the remote display
* Can send mouse events
* Windowed and fullscreen mode
* Pinch to zoom

There's a lot more that could be done, but for a first release this can be OK.

# Development of a UBports Qt/QML app with QBS

If, like me, you are a fan of QBS, you might want to have a look at the [source
code of CuteVNC](https://gitlab.com/mardy/lomiri-vnc) (yes, the project was
born as "Lomiri VNC", but since it's a generic QtQuick.Controls 2 application,
there was no need to tie it to the Lomiri environment); there you can find a
[QBS module for UBports
applications](https://gitlab.com/mardy/lomiri-vnc/-/tree/master/qbs/modules/ubuntutouch),
which can help in filling in the manifest file and deploying other Ubuntu Touch
specific files into their proper install location.

You can also see the [configuration
file](https://gitlab.com/mardy/lomiri-vnc/-/blob/master/clickable.json) to make
[clickable](https://clickable-ut.dev/en/stable/) build a QBS-based project. In
my mental task backlog I still have the idea of making clickable support QBS
out of the box, but the integration of the two technologies is so easy even
now, that I gave this task a quite low priority.

# A class for computing viewport transformations

Last but not least, a developer might find useful the [generic Scaler
class](https://gitlab.com/mardy/lomiri-vnc/-/blob/master/src/scaler.h) I wrote
to compute the viewport transformations: it's a simple, QtGui-only C++ class
which takes an input structure with the viewport information, and returns a
similar structure containing a [QTransform
matrix](https://doc.qt.io/qt-5/qtransform.html) to map item coordinates to
coordinates in the source object coordinates (in my case, the source object is
the remote screen served by VNC) and some other useful parameters, such as the
scale, center offset, and the painted area rectangle.

The class has a 100% line and branch coverage in the [unit
tests](https://gitlab.com/mardy/lomiri-vnc/-/blob/master/tests/tst_scaler.cpp),
so I hope I can rely on it.
