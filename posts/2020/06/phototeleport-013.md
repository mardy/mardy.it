<!--
.. title: PhotoTeleport 0.13
.. slug: phototeleport-013
.. date: 2020-06-22 23:11:09 UTC+03:00
.. tags: english,kdeplanet,planetmaemo,PhotoTeleport
.. category: 
.. link: 
.. description: 
.. type: text
-->

Just a quick note to let the world know that [PhotoTeleport 0.13 has been
released](http://phototeleport.com/2020/06/20/version-0.13/).
