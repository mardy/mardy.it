<!--
.. title: A simple cross-compiler for the Raspberry Pi
.. slug: a-simple-cross-compiler-for-the-raspberry-pi
.. date: 2020-02-19 23:44:42 UTC+03:00
.. tags: english,raspberrypi,informatica,Linux,programmation,kdeplanet,Ubuntu,Ubports
.. category: 
.. link: 
.. description: 
.. type: text
-->

I've recently found some use for a first-generation Raspberry Pi (Pi 1 model
B) which had been lying in a drawer since many years. A few days ago I've
installed the Raspbian distribution in it, and was about to install
[motion](https://motion-project.github.io/) on it, but I stopped as soon as I
noticed that `apt` was suggesting to bring in 1 GB worth of dependencies. Adding
`--no-install-recommends` reduced the proposal a bit, but it was still around
700 MB -- a bit too much for my taste. I figured out that the `motion` package
for Debian (and Raspbian) depends on MySQL, PostgreSQL, FFmpeg and what not;
so, I decided that I could probably just recompile it and disable all the stuff
I didn't need at `configure` time.

But I didn't want to install all the build dependencies and the cross-compiler in
my machine; containers exist for a reason, after all. So I had a look at the
[crossbuilder](https://github.com/ubports/crossbuilder/) tool that we use in
UBports: this is a nice little shell program that uses an Ubuntu-based LXD
image to cross-compile a package after automatically fetching all its
dependencies, and installs it into an UBports device. It does some magic with
`adb` and stuff, but I thought that the basic functionality should work with
minor modifications on any Debian-based distribution.

And indeed, some hours later, I got [a
branch](https://github.com/mardy/crossbuilder/tree/raspbian) where I can use
`crossbuilder` to build packages for the Raspberry Pi. Assuming that you have
LXD properly setup, the command

    crossbuilder --raspbian source motion

will cause crossbuilder to create a container and download the Debian source
package for `motion`; at this point you can modify the source code as you see
fit, and rebuild it. I only changed the `debian/rules` file to add a few flags
such as `--without-mysql`, `--without-ffmpeg`, etc.  And

    crossbuilder --raspbian

is the command to run in order to perform the build. This will first download
all the dependencies (according to the `debian/control` file), build the
package, and create a `tar` archive containing all the generated `.deb` files.
This archive can then be copied into the target device and unpacked there.

Now, there's a small problem in that Raspbian claims to be `armhf`, while in
fact its floating-point processor is somehow not compliant with the armhf
architecture. So, you generally cannot use an `armhf` package from Debian or
Ubuntu on the Raspberry Pi. Given that I didn't have the time to prepare a
proper Raspbian image for LXD, I used the Debian distribution as a base
instead, and I chose to target the `armel` architecture: this might impose some
penalties on the performance (at least for floating-point code), but it seems
to work fine on the Raspberry Pi. Unfortunately, this means that you cannot
just install the generated packages o the Pi, as `dpkg` will complain about the
architecture mismatch (your package is `armel`, while the distro claims to be
`armhf`). But unpacking the debian package with

    dpkg -x motion_<...>.deb tmp

will indeed give you package that you can use.

I will eventually get back to this and make the script work better with
Raspbian, but this is a quick start.
