<!--
.. title: Need a fast way to tag faces in many images? Try Imaginario!
.. slug: need-a-fast-way-to-tag-faces-in-many-images-try-imaginario
.. date: 2020-03-09 10:13:13 UTC+03:00
.. tags: english,imaginario,kdeplanet,planetmaemo,informatica
.. category: 
.. link: 
.. description: 
.. type: text
-->

Today I've released Imaginario 0.9. The big feature coming with this new
release is a face tagging flow which I believe will be the fastest and simplest
you've ever used, despite it being all manual. I even sat down and spent some
quality time with Blender to prepare a video to show it off:

<center>
<iframe width="560" height="315" src="https://www.youtube.com/embed/kfEuEPaOYUY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</center>

While some people might actually think that I spent more time for making the
video than for implementing the face tagging feature itself, this couldn't be
farther from the truth: the face tagging branch has been being worked on for at
least three months (of course, that's my spare time — so it's actually less
than one hour per day) and consisted of more than 40 commits (after squashing
all the fixups), whereas for the video I spent no more than a couple of hours.

I would appreciate if the curious could go and [try it
out](http://imaginario.mardy.it/releases.html), and let me know about any
issues you should find: there are built packages for Linux (AppImage), macOS
and Windows. I do also have an [Ubuntu
PPA](https://launchpad.net/~mardy/+archive/ubuntu/imaginario) where nightly
images are built, but I'm not sure if I can recommend that one, since I've not
been using it myself and have no idea whether those packages actually even
start. But you are welcome to try :-)

Your feedback will help me do better, so please don't be shy!
