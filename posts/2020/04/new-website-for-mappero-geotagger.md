<!--
.. title: New website for Mappero Geotagger, and cross-compiling stuff
.. slug: new-website-for-mappero-geotagger
.. date: 2020-04-12 12:20:55 UTC+03:00
.. tags: QBS,Qt,Ubuntu,english,gps,informatica,kdeplanet,mappero-geotagger,photographia,planetmaemo,programmation
.. category: 
.. link: 
.. description: 
.. type: text
-->

Mappero Geotagger has now moved from [its previous page from this
site](http://www.mardy.it/mappero-geotagger/) to a [new, separate
website](http://mappero.mardy.it) built with the awesome [Nikola static website
generator](http://getnikola.com).

The main reason for this change is that I didn't have an online space where to
host the application binaries, and I wanted to experiment with a different
selling method. Now, downloads are (poorly) hidden behind a payment page,
whereas in multiple places of the website I also mention that I can provide
the application for free to whomever asks for it. While it might seem weird at
first, I do honestly believe that this will not stop people from buying it:
first of all, many people just think it's fair to pay for a software
applications, and secondly, for some people writing an e-mail and establishing
a personal contact with a stranger is actually harder than paying a small
amount of money. And in all sincerity, the majority of the income I've had so
far for Mappero Geotagger came from donations, rather than purchases; so, not
much to lose here.

## QBS and MXE

Anyway, since this is primarily a technical blog, I want to share my
experiences with cross-building from Linux to Windows. As you might remember,
[some time ago I switched the build system of Mappero from qmake to
QBS](link://slug/qbs-and-code-coverage-reports), and I haven't regretted it at
all. I've managed to build the application in Linux (of course), macOS, as a
Debian package on the [Ubuntu PPA
builders](https://launchpad.net/~mardy/+archive/ubuntu/mappero), on [Windows
with AppVeyor](https://ci.appveyor.com/project/mardy/mappero) and, last but not
least, on Linux for Windows using the mingw setup provided by the [MXE
project](http://mxe.cc).

QBS worked surprisingly well also in this case, though I had to fight with a
small bug on the toolchain detection, which is hopefully going to be [fixed
soon](https://codereview.qt-project.org/c/qbs/qbs/+/296870). For the few of you
who are interested in achieving something similar, here's the steps I ran to
configure QBS for mingw:

```bash
    MXE_BASE=<path-to-mxe>
    MXE_TARGET=x86_64-w64-mingw32.shared # 32 bit or static targets are also available

    MXE_PROFILE="mxe"
    QT_PROFILE="${MXE_PROFILE}-qt"
    qbs setup-toolchains "${MXE_BASE}/usr/bin/${MXE_TARGET}-g++" $MXE_PROFILE
    qbs config profiles.$MXE_PROFILE.cpp.toolchainPrefix "${MXE_TARGET}-" # temporary workaround
    qbs setup-qt "$MXE_BASE/usr/$MXE_TARGET/qt5/bin/qmake" ${QT_PROFILE}
    qbs config profiles.${QT_PROFILE}.baseProfile $MXE_PROFILE
```

Sorry for using that many environment variables ☺. After qbs is configured,
it's just a matter of running 

```bash
    qbs profile:$QT_PROFILE
```

to build the application. You will get a nice window binary and, once you
collect all the needed library dependencies, you'll be able to run it on
Windows. Or WINE ☺.

As part of this effort, I also had to build [libraw](http://libraw.org), so I
didn't miss the occasion to [contribute its recipe to
MXE](https://github.com/mxe/mxe/pull/2481). I'm also trying to get a change
accepted, that would make MXE [support the dynamic OpenGL
selection](https://github.com/mxe/mxe/pull/2480) available since Qt 5.4.
