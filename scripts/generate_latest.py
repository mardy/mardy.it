#! /usr/bin/env python3

import argparse
import json
import os.path


def fill_latest(releases):
    global platforms
    latest = {}
    for release in releases:
        platform_versions = release["packages"]
        for platform in platform_versions:
            if platform not in latest and platform in platform_versions:
                latest[platform] = platform_versions[platform]
                if len(latest) == len(platforms):
                    return latest
    return latest


parser = argparse.ArgumentParser(description=
        'Generate JSON file with latest releases for update purposes')
parser.add_argument('output', help='Output directory')
args = parser.parse_args()

with open('data/mitubo/releases.json', 'r') as r:
    releases = json.load(r)

platforms = [ 'linux', 'macos', 'windows' ]
latest = fill_latest(releases)

with open(os.path.join(args.output, 'latestversion.json'), 'w') as f:
    json.dump(latest, f, indent=2)
